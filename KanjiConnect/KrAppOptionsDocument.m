//
//  AppOptionsDocument.m
//  KanjiConnect
//
//  Created by Ray Price on 10/12/12.
//
//

#import "KrAppOptionsDocument.h"

@implementation KrAppOptionsDocument

@synthesize appOptions;

-(id)initWithFileURL:(NSURL *)url {
    self = [super initWithFileURL:url];
    if (self) {
        // initialize
        appOptions = [[NSMutableDictionary alloc] initWithCapacity:10];
        
        /*
		// load from plist
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *path = [documentsDirectory stringByAppendingPathComponent:@"AppOptions.plist"];
		if ([[NSFileManager defaultManager] fileExistsAtPath:path])
			appOptions = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
		else
			appOptions = [[NSMutableDictionary alloc] init];
        */
        
    }
    
    return self;
}

-(void)handleError:(NSError *)error
userInteractionPermitted:(BOOL)userInteractionPermitted
{
    // log the error
    NSLog(@"Error '%@' during handling of KrAppOptions document.", error.localizedDescription);
    
    // pass to super
    [super handleError:error userInteractionPermitted:userInteractionPermitted];
}

// Called whenever the application reads data from the file system
- (BOOL)loadFromContents:(id)contents ofType:(NSString *)typeName
                   error:(NSError **)outError
{
    // load data, or initialize
    if ([contents length] > 0) {
        @try {
            // get app options from contents
            NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:contents];
            appOptions = [[unarchiver decodeObjectForKey:@"AppOptions"] retain];
            
            // if for some reason app options failed to load, initialize
            if (appOptions == nil)
                appOptions = [[NSMutableDictionary alloc] init];
            
            [unarchiver finishDecoding];
            [unarchiver release];
        }
        @catch (NSException *exception) {
            // display warning to user
            // print confirmation message
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iCloud" message:@"Failed loading contents from iCloud document.  The document has become corrupted.  Please try again.  If the error persists, please contact support@rakudasoft.com." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
            
            // throw exception to exit application so user doesn't overwrite their
            // progress
            [NSException raise:@"iCloud document became corrupted" format:@"iCloud document became corrupted"];
        }
        @
        finally {
        }
        /*
		// load from plist
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *path = [documentsDirectory stringByAppendingPathComponent:@"OldAppOptions.plist"];
		if ([[NSFileManager defaultManager] fileExistsAtPath:path])
			appOptions = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
		else
			appOptions = [[NSMutableDictionary alloc] init];*/

    } else {
        // empty options to start
        appOptions = [[NSMutableDictionary alloc] init];
    }
    
    return YES;
}

// Called whenever the application (auto)saves the content of a note
- (id)contentsForType:(NSString *)typeName error:(NSError **)outError
{
    // return appOptions as data
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:appOptions forKey:@"AppOptions"];
    [archiver finishEncoding];
    [archiver release];
    
    return [data autorelease];
    
    /*    return [NSData dataWithBytes:[self.noteContent UTF8String]
     length:[self.noteContent length]];*/
}

-(void)dealloc {
    // super
    [super dealloc];
    
    // release options
    [appOptions release];
}

@end
