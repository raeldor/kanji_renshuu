//
//  RadialMenuViewControllerDelegate.h
//  KanjiConnect
//
//  Created by Ray Price on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef KanjiConnect_RadicalMenuViewControllerDelegate_h
#define KanjiConnect_RadicalMenuViewControllerDelegate_h

@protocol RadialMenuViewControllerDelegate

@optional

-(void)didMakeRadialSelection:(int)inSelection;
-(void)didCancelRadialSelection;

@end

#endif
