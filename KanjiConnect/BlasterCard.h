//
//  BlasterCard.h
//  KanjiConnect
//
//  Created by Ray Price on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BlasterCard : NSObject {
    
}

@property (nonatomic, assign) int dictionaryId;
@property (nonatomic, retain) NSString *kanji;
@property (nonatomic, retain) NSString *kana;
@property (nonatomic, assign) int senseIndex;

-(id)initWithDictionaryId:(int)inDictionaryId kanji:(NSString*)inKanji andKana:(NSString*)inKana andSense:(int)inSenseIndex;

@end
