//
//  EditKcListViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KanjiList.h"
#import "KanjiPickerDelegate.h"
#import "EditKanjiListViewControllerDelegate.h"

@interface EditKanjiListViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, KanjiPickerDelegate> {
    KanjiList *editList;
    KanjiList *originalList;
    BOOL isNew;
    
}

-(void)setEditableObject:(KanjiList*)inEditableObject isNew:(BOOL)inIsNew;
-(IBAction)saveButtonClick:(id)sender;
-(IBAction)cancelButtonClick:(id)sender;
-(IBAction)didChangeName:(id)sender;
-(BOOL)isOkToSave;

@property (nonatomic, assign) id<EditKanjiListViewControllerDelegate> delegate;

@end
