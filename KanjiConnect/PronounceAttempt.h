//
//  PronounceAttempt.h
//  KanjiConnect
//
//  Created by Ray Price on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PronounceAttempt : NSObject {
    NSString *wordPronun;
    NSMutableArray *kanjiPronun;
}

-(id)initWithPronunication:(NSString*)inPron andKanjiPronunciation:(NSMutableArray*)inKanjiPronun;

@property (nonatomic, assign) NSMutableArray *kanjiPronun;

@end
