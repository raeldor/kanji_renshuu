//
//  BlasterSentence.h
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JapaneseWord.h"
#import "SampleEntry.h"
#import "BlasterSentenceDelegate.h"
#import "BlasterWordDelegate.h"

@class BlasterGame;

@interface BlasterSentence : NSObject <BlasterWordDelegate> {
    SampleEntry *sentenceEntry;
    BlasterGame *containerGame;
    NSMutableArray *words; // of type blastersentenceword
    NSString *meaning;
    NSString *kanji;
    BOOL completed;
    
    id<BlasterSentenceDelegate> delegate;
}

-(id)initWithSentenceEntry:(SampleEntry*)inEntry forGame:(BlasterGame*)inGame;

@property (nonatomic, retain) BlasterGame *containerGame;
@property (nonatomic, retain) NSMutableArray *words;
@property (nonatomic, assign) NSString *meaning;
@property (nonatomic, assign) NSString *kanji;
@property (nonatomic, assign) BOOL completed;
@property (nonatomic, assign) id<BlasterSentenceDelegate> delegate;

@end
