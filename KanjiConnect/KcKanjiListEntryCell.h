//
//  KcKanjiListEntryCell.h
//  KanjiConnect
//
//  Created by Ray Price on 7/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KcKanjiListEntryCell : UITableViewCell {
    IBOutlet UILabel *kanjiLabel;
    IBOutlet UILabel *meaningLabel;
}

@property (nonatomic, assign) UILabel *kanjiLabel;
@property (nonatomic, assign) UILabel *meaningLabel;

@end
