//
//  BlasterWord.m
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BlasterWord.h"
#import "BlasterGame.h"
#import "KanjiDictionary.h"
#import "JapaneseDictionary.h"
#import "TextFunctions.h"
#import "JmwDatabase.h"
#import "NSMutableArrayShuffle.h"
#import "KrAppOptions.h"
#import "KrLocalOptions.h"
#import "LeitnerLogic.h"

@implementation BlasterWord

@synthesize toBeCompleted;
@synthesize completed;
@synthesize wordEntry;
@synthesize kanjis;
@synthesize containerGame;
@synthesize sampleSentence;
@synthesize kanji;
@synthesize kana;
@synthesize senseIndex;
@synthesize wrongFurigana;
@synthesize meaning;
@synthesize furigana;
@synthesize userFurigana;
@synthesize conjugation;
@synthesize delegate;
@synthesize isInJlptLevel;
@synthesize isWellKnown;
@synthesize isWellKnownForReading;
@synthesize isContainsKanjiInStudyList;
@synthesize hadToLook;

-(id)initWithSampleEntryWord:(SampleEntryWord*)inEntry forGame:(BlasterGame*)inGame {
    self = [super init];
    if (self) {
        // save word entry and game
        containerGame = [inGame retain];
        
        // save kanji and kana and meaning
        kanji = [inEntry.kanji retain];
        kana = [inEntry.kana retain];
        senseIndex = inEntry.senseIndex;
        
        // try and find meaning
        if (inEntry.dictionaryId != -1) {
            // set meaning
            JapaneseWord *dictEntry = [[JapaneseDictionary getSingleton] getWordFromDictionaryId:inEntry.dictionaryId];
            
            // pick the correct meaning, not just the first one!
            meaning = @"ERROR:SENSE NOT FOUND";
            for (int i=0; i < dictEntry.englishMeanings.count; i++) {
                // is this the correct sense index
                NSString *thisSenseString = [dictEntry.meaningSense objectAtIndex:i];
                int thisSenseIndex = [thisSenseString intValue];
                if (thisSenseIndex == senseIndex || (senseIndex == 0 && thisSenseIndex == 1)) {
                    meaning = [[dictEntry.englishMeanings objectAtIndex:i] retain];
                    break;
                }
            }
            
            // save entry
            wordEntry = [dictEntry retain];
        }
        else
            meaning = @"";
        
        // work out flags that govern how this word looks and whether it needs completing
        // try and find dictionary entry for this word
        if (inEntry.dictionaryId != -1) {
            // find japanese word
            JapaneseWord *dictEntry = [[JapaneseDictionary getSingleton] getWordFromDictionaryId:inEntry.dictionaryId];
            
            NSLog(@"Sentence word is %@,%@", dictEntry.kana, dictEntry.kanji)
            ;
            
            // if outside of our JLPT level
            toBeCompleted = YES;
            if ([[[JapaneseDictionary getSingleton] getJlptStringListFromLevelNumber:(jlptLevelEnum)[KrLocalOptions getSingleton].showWordsFor] rangeOfString:dictEntry.customCategory].location == NSNotFound) {
                isInJlptLevel = NO;
                
                // we still have to complete if this word has been SPECIFICALLY chosen
                if ([containerGame isWordKnownForKanji:dictEntry.kanji andKana:dictEntry.kana andSenseIndex:inEntry.senseIndex])
                    toBeCompleted = YES;
                else
                    toBeCompleted = NO;
                
                NSLog(@"Not in JLPT level");
            }
            else {
                // is in our level
                isInJlptLevel = YES;
                
                // or contains only kana
                if (![TextFunctions containsKanji:inEntry.kanji]) {
                    toBeCompleted = NO;
                    NSLog(@"Has only kana");
                }
                else {
                    // missing stroke data?  don't complete for writing then
                    if (dictEntry.missingStrokeData && containerGame.gameType&blGameType_Writing) {
                        toBeCompleted = NO;
                        NSLog(@"Missing stroke data");
                    }
                    else {
                        // must contain at least ONE kanji we know
                        isContainsKanjiInStudyList = NO;
                        if ([KrLocalOptions getSingleton].pickWordsUsing == 1) {
                            // or contains only kanji we don't know
                            for (int k=0; k < inEntry.kanji.length; k++) {
                                NSString *thisKanji = [inEntry.kanji substringWithRange:NSMakeRange(k, 1)];
                                if ([containerGame isKanjiKnown:thisKanji]) {
                                    isContainsKanjiInStudyList = YES;
                                    break;
                                }
                            }
                            if (!isContainsKanjiInStudyList) {
                                NSLog(@"Doesn't contain kanji in study list");
                                toBeCompleted = NO;
                            }
                        }
                        else {
                            if ([KrLocalOptions getSingleton].pickWordsUsing == 0) {
                                // must contain ONLY kanji we know
                                isContainsKanjiInStudyList = YES;
                                for (int k=0; k < inEntry.kanji.length; k++) {
                                    NSString *thisKanji = [inEntry.kanji substringWithRange:NSMakeRange(k, 1)];
                                    if (![TextFunctions isKana:thisKanji] && ![containerGame isKanjiKnown:thisKanji]) {
                                        NSLog(@"Doesn't only contain kanji we know");
                                        toBeCompleted = NO;
                                        isContainsKanjiInStudyList = NO;
                                        break;
                                    }
                                }
                            }
                            else {
                                // must contain only kanji we know and only ONE kanji
                                // or contains only kanji we don't know
                                int kanjiCount = 0;
                                for (int k=0; k < inEntry.kanji.length; k++) {
                                    NSString *thisKanji = [inEntry.kanji substringWithRange:NSMakeRange(k, 1)];
                                    if (![TextFunctions isKana:thisKanji])
                                        kanjiCount++;
                                    if ([containerGame isKanjiKnown:thisKanji]) {
                                        isContainsKanjiInStudyList = YES;
                                    }
                                }
                                if (!isContainsKanjiInStudyList) {
                                    NSLog(@"Doesn't contain kanji in study list");
                                    toBeCompleted = NO;
                                }
                                if (kanjiCount > 1)
                                    toBeCompleted = NO;
                            }
                        }
                        
                        // override if word has been specifically chosen
                        NSLog(@"Check is word known for kanji %@, kana %@, sense %d", dictEntry.kanji, dictEntry.kana, inEntry.senseIndex);
                        if ([containerGame isWordKnownForKanji:dictEntry.kanji andKana:dictEntry.kana andSenseIndex:inEntry.senseIndex]) {
                            NSLog(@"Override because it's in word list");
                            toBeCompleted = YES;
                            isContainsKanjiInStudyList = NO;
                        }
                        
                        // if this word is well known, don't need to keep completing
                        isWellKnown = NO;
                        
                        // if this word is not the word we are studying for this sentence
                        BlasterCard *thisCard = [inGame getCurrentCard];
                        NSLog(@"Sentence study word is %@,%@", thisCard.kana, thisCard.kanji)
                        ;
                        if ([dictEntry.kana isEqualToString:thisCard.kana ] &&
                            [dictEntry.kanji isEqualToString:thisCard.kanji]) {
                            // always complete if it's the study word for the sentence
                            NSLog(@"It's the main sentence word");
                        }
                        else {
                            // if it's not the study word
                            // if it's in leitner deck 5, give the user a break
                            // create temp card
                            int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:dictEntry.kanji andKana:dictEntry.kana andSenseIndex:senseIndex withBlasterGameType:inGame.gameType];
                            /*
                            NSString *key = [NSString stringWithFormat:@"%@\r\n%@\r\n%d", dictEntry.kanji, dictEntry.kana, senseIndex];
                            int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKey:key isWritingGame:inGame.gameType&blGameType_Writing];*/
                            NSLog(@"Leitner deck is %d", leitnerDeck);
                            if (leitnerDeck >= 5) {
                                // don't complete because we know
                                toBeCompleted = NO;
                                isWellKnown = YES;
                            }
                            leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:dictEntry.kanji andKana:dictEntry.kana andSenseIndex:senseIndex withBlasterGameType:blGameType_Reading];
//                            leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKey:key isWritingGame:NO];
                            if (leitnerDeck == 5)
                                isWellKnownForReading = YES;
                        }
                    }
                }
            }
        }
        else {
            // not found in dictionary, so mark completed
            toBeCompleted = NO;
        }
        completed = !toBeCompleted;
        
        // create list of blaster kanjis for each kanji
        kanjis = [[NSMutableArray alloc] initWithCapacity:10];
        for (int i=0; i < kanji.length; i++) {
            // if this is a writing game
            BOOL kanjiToBeCompleted = NO;
            // if the word is to be completed
            NSString *thisKanjiString = [kanji substringWithRange:NSMakeRange(i, 1)];
            if (toBeCompleted) {
                if (containerGame.gameType&blGameType_Writing) {
                    // shouldn't this ALWAYS be a kanji we know if it's marked as to be completed?  as long as it's not a kana
                    if (![TextFunctions isKana:thisKanjiString])
                        kanjiToBeCompleted = YES;
                    /*
                    // and this is a kanji we know
                    if ([containerGame isKanjiKnown:thisKanjiString] || [containerGame isWordKnownForKanji:dictEntry.kanji andKana:dictEntry.kana andSenseIndex:inEntry.senseIndex]) {
                        // it needs to be completed
                        kanjiToBeCompleted = YES;
                    }*/
                }
            }
            // find kanji entry and create blaster kanji
            KanjiEntry *kanjiEntry = [[KanjiDictionary getSingleton] getEntryUsingKanji:thisKanjiString];
            BlasterKanji *newKanji;
            if (kanjiEntry != nil)
                newKanji = [[BlasterKanji alloc] initWithKanjiEntry:kanjiEntry toBeCompleted:kanjiToBeCompleted forGame:containerGame];
            else
                newKanji = [[BlasterKanji alloc] initWithKanjiString:thisKanjiString forGame:containerGame];
            [kanjis addObject:newKanji];
            [newKanji release];
        }
        
        // first get the conjugation string to add to the kanji pronunciation
        conjugation = @"";
        for (int i=(int)kanji.length-1; i >= 0; i--) {
            if ([TextFunctions isKana:[kanji substringWithRange:NSMakeRange(i, 1)]])
                conjugation = [NSString stringWithFormat:@"%@%@", [kanji substringWithRange:NSMakeRange(i, 1)], conjugation];
            else
                break;
        }
        conjugation = [conjugation retain];
        
        // calculate furigana part
        furigana = [[kana substringToIndex:kana.length-conjugation.length] retain];
        userFurigana = @"?";
        
        // sometimes furigana is in katakana, so convert to hiragana
        self.furigana = [TextFunctions katakanaToHiragana:furigana];
//        NSString *hiraganaFurigana = [TextFunctions katakanaToHiragana:furigana];
//        [furigana release];
//        furigana = [hiraganaFurigana retain];
        
        // trim leading kana
        if ([TextFunctions containsKanji:kanji]) {
            for (int i=0; i < kanji.length-1; i++) {
                if ([TextFunctions isKana:[kanji substringWithRange:NSMakeRange(i, 1)]]) {
                    self.furigana = [furigana substringFromIndex:1];
                }
                else
                    break;
            }
        }
    }
    return self;
}

-(int)getLeitnerDeckNumber {
    // if this is in the dictionary
    if (wordEntry != nil) {
        // return deck number
//        NSString *key = [NSString stringWithFormat:@"%@\r\n%@\r\n%d", wordEntry.kanji, wordEntry.kana, senseIndex];
        return [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:wordEntry.kanji andKana:wordEntry.kana andSenseIndex:senseIndex withBlasterGameType:containerGame.gameType];
//        return [[KrAppOptions getSingleton] getLeitnerDeckNumberForKey:key isWritingGame:containerGame.gameType&blGameType_Writing];
    }
    return 1;
}

-(void)incrementLeitnerDeckNumber {
    // if this is in the dictionary
    if (wordEntry != nil) {
        // get current deck, and minutes/days since shown
        int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType];
        NSUInteger daysSinceShown = [[KrAppOptions getSingleton] getDaysSinceLastShownForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType];
        NSUInteger minutesSinceShown = [[KrAppOptions getSingleton] getMinutesSinceLastShownForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType];
        
        // save for debug printout
        int oldDeck = leitnerDeck;
        
        // use leitner logic now to centralize the logic
        int newDeck = [LeitnerLogic incrementToDeckForCurrentDeck:leitnerDeck withDaysSinceShown:daysSinceShown andMinutesSinceShown:minutesSinceShown];
        
        // update deck if it's different
        if (newDeck != leitnerDeck) {
            // set new deck number
            [[KrAppOptions getSingleton] setLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType toValue:newDeck autoSave:NO];

            // mark word as seen - have to do this AFTER leitner deck change, as
            // it needs to know when last seen
            [[KrAppOptions getSingleton] updateLastShownTimestampForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType autoSave:YES];

            NSLog(@"Word %@ went from deck %d to deck %d", self.kanji, oldDeck, newDeck);
        }
        
        /*
        [[KrAppOptions getSingleton] setLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType toValue:leitnerDeck autoSave:NO];
//        [[KrAppOptions getSingleton] setLeitnerDeckNumberForKey:wordKey toValue:leitnerDeck isWritingGame:containerGame.gameType&blGameType_Writing];
        
        // NOW increment leitner deck
        [[KrAppOptions getSingleton] incrementLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType autoSave:NO];
//        [[KrAppOptions getSingleton] incrementLeitnerDeckNumberForKey:wordKey isWritingGame:containerGame.gameType&blGameType_Writing];
        
        int newDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType];
//        int newDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKey:wordKey isWritingGame:containerGame.gameType&blGameType_Writing];
        
        NSLog(@"Word %@ went from deck %d to deck %d", self.kanji, oldDeck, newDeck);
        
        
        // mark word as seen - have to do this AFTER leitner deck change, as
        // it needs to know when last seen
        [[KrAppOptions getSingleton] updateLastShownTimestampForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType autoSave:YES];
//        [[KrAppOptions getSingleton] updateDaysSinceLastShownForKey:wordKey isWritingGame:containerGame.gameType&blGameType_Writing];
        
        */
    }
}

-(void)decrementLeitnerDeckNumber {
    // if this is in the dictionary
    if (wordEntry != nil) {
        int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType];
        NSUInteger daysSinceShown = [[KrAppOptions getSingleton] getDaysSinceLastShownForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType];
        NSUInteger minutesSinceShown = [[KrAppOptions getSingleton] getMinutesSinceLastShownForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType];

        // save for debug printout
        int oldDeck = leitnerDeck;
        
        // use leitner logic now to centralize the logic
        int newDeck = [LeitnerLogic decrementToDeckForCurrentDeck:leitnerDeck withDaysSinceShown:daysSinceShown andMinutesSinceShown:minutesSinceShown];
        
        // update deck if it's different
        if (newDeck != leitnerDeck) {
            // set new deck number
            [[KrAppOptions getSingleton] setLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType toValue:newDeck autoSave:NO];
            
            // mark word as seen - have to do this AFTER leitner deck change, as
            // it needs to know when last seen
            [[KrAppOptions getSingleton] updateLastShownTimestampForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType autoSave:YES];
            
            NSLog(@"Word %@ went from deck %d to deck %d", self.kanji, oldDeck, newDeck);
        }

        // if you're seeing it before you're due to, then decrement more, ie if you're supposed to
        // be seeing it after a week, but are seeing it after a day and you STILL get it wrong,
        /*
         don't do this!  if you were not supposed to see it for 6 months, but it shows up
         after a week, this would reset it back to a week, when really all you needed was
         a friendly reminder and not need to see it for another 3 months.
         
        // put it in the day deck before you decrement
        if (minutesSinceShown <= 5 && leitnerDeck > -2)
            leitnerDeck = -2;
        if (minutesSinceShown <= 15 && leitnerDeck > -1)
            leitnerDeck = -1;
        if (minutesSinceShown <= 30 && leitnerDeck > 0)
            leitnerDeck = 0;
        if (minutesSinceShown <= 60 && leitnerDeck > 1)
            leitnerDeck = 1;
        if (daysSinceShown <= 1 && leitnerDeck > 2)
            leitnerDeck = 2;
        if (daysSinceShown <= 3 && leitnerDeck > 3)
            leitnerDeck = 3;
        if (daysSinceShown <= 7 && leitnerDeck > 4)
            leitnerDeck = 4;
        if (daysSinceShown <= 30 && leitnerDeck > 5)
            leitnerDeck = 5;
        if (daysSinceShown <= 30*4 && leitnerDeck > 6)
            leitnerDeck = 6;
        if (daysSinceShown <= 365 && leitnerDeck > 7)
            leitnerDeck = 7;
        */
        
        /*
        // set deck number for special processing before decrementing
        [[KrAppOptions getSingleton] setLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType toValue:leitnerDeck autoSave:NO];
//        [[KrAppOptions getSingleton] setLeitnerDeckNumberForKey:wordKey toValue:leitnerDeck isWritingGame:containerGame.gameType&blGameType_Writing];
        
        // NOW decrement leitner deck
        [[KrAppOptions getSingleton] decrementLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType autoSave:NO];
//        [[KrAppOptions getSingleton] decrementLeitnerDeckNumberForKey:wordKey isWritingGame:containerGame.gameType&blGameType_Writing];
        
        int newDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType];
//        int newDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKey:wordKey isWritingGame:containerGame.gameType&blGameType_Writing];
        
        NSLog(@"Word %@ went from deck %d to deck %d", self.kanji, oldDeck, newDeck);
        
        
        // mark word as seen - have to do this AFTER leitner deck change, as
        // it needs to know when last seen
        [[KrAppOptions getSingleton] updateLastShownTimestampForKanji:self.wordEntry.kanji andKana:self.wordEntry.kana andSenseIndex:self.senseIndex withBlasterGameType:containerGame.gameType autoSave:YES];
//        [[KrAppOptions getSingleton] updateDaysSinceLastShownForKey:wordKey isWritingGame:containerGame.gameType&blGameType_Writing];
        */
        
        
        /*
        // return deck number
        NSString *key = [NSString stringWithFormat:@"%@\r\n%@\r\n%d", wordEntry.kanji, wordEntry.kana, senseIndex];
        [[KrAppOptions getSingleton] decrementLeitnerDeckNumberForKey:key isWritingGame:containerGame.gameType&blGameType_Writing];*/
    }
}

-(void)setCompleted:(BOOL)inCompleted {
    // set flag
    completed = inCompleted;
    
    // call delegate to tell them it's completed
    if (completed) {
        // call delegate
        [delegate didCompleteWord:self];
    }
}

-(void)dealloc {
    // free our retains
//    [wordEntry release];
    [containerGame release];
    [kanji release];
    [kana release];
    [wordEntry release];
    [kanjis release];
    [meaning release];
    [conjugation release];
    [furigana release];
    [userFurigana release];
    
    // super
    [super dealloc];
}

@end
