//
//  BlasterWord.h
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JapaneseWord.h"
#import "BlasterSentence.h"
#import "SampleEntryWord.h"
#import "BlasterWordDelegate.h"

@class BlasterGame;

@interface BlasterWord : NSObject {
    JapaneseWord *wordEntry;
    BlasterGame *containerGame;
    NSMutableArray *kanjis; // of type blasterkanji
    NSString *kanji;
    NSString *kana;
    int senseIndex;
    NSString *meaning;
    NSString *furigana; // furigana part
    NSString *userFurigana; // user's guess
    NSString *conjugation; // non-furigana part
    int furiganaSize; // # of kanji it covers
    BlasterSentence *sampleSentence;
    
    BOOL toBeCompleted;
    BOOL completed;
    
    // determines color coding
    BOOL isInJlptLevel;
    BOOL isWellKnown;
    BOOL isWellKnownForReading;
    BOOL isContainsKanjiInStudyList;
    
    BOOL hadToLook; // did ask for help
    
    id<BlasterWordDelegate> delegate;
}

//-(id)initWithWordEntry:(JapaneseWord*)inEntry forGame:(BlasterGame*)inGame;
-(id)initWithSampleEntryWord:(SampleEntryWord*)inEntry forGame:(BlasterGame*)inGame;
-(int)getLeitnerDeckNumber;
-(void)incrementLeitnerDeckNumber;
-(void)decrementLeitnerDeckNumber;

@property (nonatomic, retain) JapaneseWord *wordEntry;
@property (nonatomic, retain) BlasterGame *containerGame;
@property (nonatomic, retain) NSMutableArray *kanjis;
@property (nonatomic, assign) BOOL toBeCompleted;
@property (nonatomic, assign) BOOL completed;
@property (nonatomic, retain) BlasterSentence *sampleSentence;
@property (nonatomic, retain) NSString *kanji;
@property (nonatomic, retain) NSString *kana;
@property (nonatomic, assign) int senseIndex;
@property (nonatomic, retain) NSString *meaning;
@property (nonatomic, retain) NSString *furigana;
@property (nonatomic, retain) NSString *userFurigana;
@property (nonatomic, retain) NSString *conjugation;
@property (nonatomic, retain) NSMutableArray *wrongFurigana;
@property (nonatomic, assign) id<BlasterWordDelegate> delegate;
@property (nonatomic, assign) BOOL isInJlptLevel;
@property (nonatomic, assign) BOOL isWellKnown;
@property (nonatomic, assign) BOOL isWellKnownForReading;
@property (nonatomic, assign) BOOL isContainsKanjiInStudyList;
@property (nonatomic, assign) BOOL hadToLook;

@end
