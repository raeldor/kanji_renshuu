//
//  AppDelegate.m
//  KanjiConnect
//
//  Created by Ray Price on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "KrAppOptions.h"
#import "ViewController.h"
#import "KrLocalOptions.h"
#import <AVFoundation/AVFoundation.h>

@implementation AppDelegate

@synthesize window = _window;
@synthesize appOptions;
@synthesize localOptions;
@synthesize isOptionsLoaded;
@synthesize isLocalOptionsLoaded;
@synthesize dictionaryListDefinitionFont;
@synthesize dictionaryListReadingFont;
@synthesize dictionaryListMeaningFont;
@synthesize dictionaryViewDefinitionFont;
@synthesize dictionaryViewGradeFont;
@synthesize dictionaryViewReadingFont;
@synthesize dictionaryViewSenseNumberFont;
@synthesize dictionaryViewMeaningFont;
@synthesize dictionaryViewNotesFont;
@synthesize dictionaryViewConjugationTypeFont;
@synthesize dictionaryViewConjugationFont;
@synthesize deckEditDefinitionFont;
@synthesize deckEditReadingFontFont;
@synthesize deckEditMeaningFont;

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // set notification callback for options
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localOptionsLoaded:) name:@"Local Options Loaded" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appOptionsLoaded:) name:@"AppOptions.plist Options Loaded" object:nil];
    
    // first load local options
    localOptions = [[KrLocalOptions alloc] init];
    
    // set audio to ambient so we don't block music!
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    
    //
    // do any initialization that doesn't rely on app or local options
    //
    
    return YES;
}

-(void)localOptionsLoaded:(NSNotification *) notification {
    // check for cloud access
    NSURL *ubiq = [[NSFileManager defaultManager]
                   URLForUbiquityContainerIdentifier:nil];
    
    // test font zoom
    float fontZoom = 1.0f;
//    if ([localOptions getEnlargedFontsFlag] == 1)
//        fontZoom = 1.6f;
    
    // set default fonts here
// change font back to system because helvetica was causing wrong sizing in furigana control
//    dictionaryListDefinitionFont = [[UIFont fontWithName:@"Helvetica Neue" size:24.0f*fontZoom] retain];
    dictionaryListDefinitionFont = [[UIFont boldSystemFontOfSize:24.0f*fontZoom] retain];

    
    dictionaryListReadingFont = [[UIFont systemFontOfSize:14.0f*fontZoom] retain];
    dictionaryListMeaningFont = [[UIFont systemFontOfSize:14.0f*fontZoom] retain];
    dictionaryViewDefinitionFont = [[UIFont boldSystemFontOfSize:36.0f*fontZoom] retain];
    dictionaryViewGradeFont = [[UIFont systemFontOfSize:12.0f*fontZoom] retain];
    dictionaryViewReadingFont = [[UIFont boldSystemFontOfSize:14.0f*fontZoom] retain];
    dictionaryViewSenseNumberFont = [[UIFont systemFontOfSize:10.0f*fontZoom] retain];
    dictionaryViewMeaningFont = [[UIFont systemFontOfSize:14.0f*fontZoom] retain];
    dictionaryViewNotesFont = [[UIFont systemFontOfSize:12.0f*fontZoom] retain];
    dictionaryViewConjugationTypeFont = [[UIFont boldSystemFontOfSize:12.0f*fontZoom] retain];
    dictionaryViewConjugationFont = [[UIFont systemFontOfSize:14.0f*fontZoom] retain];
    deckEditDefinitionFont = [[UIFont boldSystemFontOfSize:16.0f*fontZoom] retain];
    deckEditReadingFontFont = [[UIFont boldSystemFontOfSize:16.0f*fontZoom] retain];
    deckEditMeaningFont = [[UIFont boldSystemFontOfSize:16.0f*fontZoom] retain];
    
    // if we have cloud access
    if (ubiq) {
        NSLog(@"iCloud access at %@", ubiq);
        
        // if cloud is now found, show message to user
        if (!localOptions.isUsingCloud) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iCloud Enabled" message:@"iCloud has become available.  Switching over to using iCloud.  If you would prefer to work locally, please go to control panel and disable iCloud for this device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        
        // soon as cloud is found, consider we are using
        localOptions.isUsingCloud = YES;
        [localOptions save];
        
        // load app options and check for changes
        appOptions = [[KrAppOptions alloc] initWithFilename:@"AppOptions.plist" andLocalOptions:localOptions];
        [appOptions checkForCloudChanges];
    } else {
        NSLog(@"No iCloud access");
        
        // if we were previously using cloud, error and exit
        if ([KrLocalOptions getSingleton].isUsingCloud) {
            // show error and give option to restart locally or exit
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No iCloud" message:@"iCloud is not available.  Please try again later or, if you turned it off on purpose, you can choose to work local.  If problem persists, please contact support@rakudasoft.com." delegate:nil cancelButtonTitle:@"Try Later" otherButtonTitles:@"Work Local", nil];
            alert.delegate = self;
            [alert show];
            [alert release];
        }
        else {
            // otherwise, just show warning that cloud is good :S
            if (![KrLocalOptions getSingleton].cloudWarningShown) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No iCloud" message:@"iCloud is not available.  Consider enabling iCloud if you want to synchronize your progress between devices." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [alert release];
                
                // set error shown in options
                [KrLocalOptions getSingleton].cloudWarningShown = YES;
                [[KrLocalOptions getSingleton] save];
            }
            
            // load app options locally
            appOptions = [[KrAppOptions alloc] initWithFilename:@"AppOptions.plist" andLocalOptions:localOptions];
        }
    }
    
    // show welcome message
    if (![KrLocalOptions getSingleton].welcomeMessageShown) {
        // show welcome message
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Welcome" message:@"Welcome to Kanji Renshuu.  It is recommended you go first go into options and set your JLPT level.  Press the 'i' button in the lower right for more help on how to play.  Enjoy the application, and please feel free to send us your comments and suggestions." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        // set message shown flag
        [KrLocalOptions getSingleton].welcomeMessageShown = YES;
        [[KrLocalOptions getSingleton] save];
    }
    
    // options now loaded
    isLocalOptionsLoaded = YES;

    NSLog(@"Finished loading local options");
    
    // remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Local Options Loaded" object:nil];
}


-(void)appOptionsLoaded:(NSNotification *) notification {
    NSLog(@"Finished loading app options");
    
    // did they load sucessfully?
    AppOptions *theseOptions = (AppOptions*)notification.object;
    if (!theseOptions.loadedOk) {
        // give user option to create new progress
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iCloud Error" message:@"iCloud progress failed to load.  If this is your first time using the cloud, select 'Create New', otherwise select 'Try Again'.  If error persists, please contact support@rakudasoft.com." delegate:nil cancelButtonTitle:@"Try Later" otherButtonTitles:@"Create New", @"Try Again", nil];
        alert.delegate = self;
        [alert show];
        [alert release];
        
        return;
    }
    
    // check for cloud access
    NSURL *ubiq = [[NSFileManager defaultManager]
                   URLForUbiquityContainerIdentifier:nil];
    
    /*
    // do one-off update to version 2
    if ([appOptions updateToVersion2Options])
        [appOptions save];
    */
    
   	// if database version has changed
	if (![localOptions.lastVersionNumber isEqualToString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]]) {
        // unpack decks
        [self copyBundledDocumentsOfType:@"flashDeck"];
        
        // update version number
        [localOptions setLastVersionNumber:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
        [localOptions save];
	}
    
    // if cloud is available and it's not kana practice, copy up local decks to the cloud
    NSString *productName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"] ;
    if (ubiq != nil && ![productName isEqualToString:@"KanaPractice"]) {
        // enumerate files with extension
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSArray *files = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
        BOOL firstDeck = YES;
        for (int i=0; i < files.count; i++)
            if ([[[files objectAtIndex:i] pathExtension] isEqualToString:@"flashDeck"]) {
                // if first deck, warn user
                if (firstDeck) {
                    // show alert
                    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Found Local Flash Decks" message:@"Copying local flash decks up to the cloud so they can be shared between your devices and Rakudasoft apps." delegate:self cancelButtonTitle:@"Understood" otherButtonTitles:nil];
                    [myAlert show];
                    [myAlert release];
                    firstDeck = NO;
                }
                
                // copy to cloud
                NSString *thisFile = [files objectAtIndex:i];
                NSURL *sourceURL = [NSURL fileURLWithPathComponents:[NSArray arrayWithObjects:documentsDirectory, thisFile, nil]];
                NSURL *destURL = [ubiq URLByAppendingPathComponent:[@"Documents" stringByAppendingPathComponent:thisFile]];
                
                // if this is a bundled document, delete the cloud version first so
                // we don't get error that it already exists
                if ([self isBundledDocumentWithName:thisFile andType:nil])
                    [[NSFileManager defaultManager] removeItemAtURL:destURL error:nil];
                
                NSError *thisError;
                if (![[NSFileManager defaultManager] setUbiquitous:YES itemAtURL:sourceURL destinationURL:destURL error:&thisError]) {
                    // show warning
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iCloud Conflict" message:[NSString stringWithFormat:@"Error moving flash deck %@ to the cloud.  Please delete the local document using iTunes or if you want to replace the cloud document with this one, delete the cloud document using iCloud in your phone's settings.", thisFile] delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
                    [alert show];
                    [alert release];
                }
                
            }
    }
    
    // mark as loaded
    isOptionsLoaded = YES;
    
    // remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"App Options Loaded" object:nil];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // if we cancelled, pop controler
    if ([alertView.title isEqualToString:@"Fatal Error"]) {
        // exit with alert message
        [NSException raise:alertView.title format:@"%@", alertView.message];
    }
    
    // work local?
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Work Local"]) {
        // now working local
        [KrLocalOptions getSingleton].isUsingCloud = NO;
        [KrLocalOptions getSingleton].cloudWarningShown = YES;
        [[KrLocalOptions getSingleton] save];
        
        // load app options locally
        appOptions = [[KrAppOptions alloc] initWithFilename:@"AppOptions.plist" andLocalOptions:localOptions];
    }
    
    // create new icloud document?
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Create New"]) {
        // create new progress
        // don't check for changes straight away, as save operates in background
        [appOptions createNewCloudDocument];
    }
    
    // try again
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Try Again"]) {
        // load app options and check for changes
        appOptions = [[KrAppOptions alloc] initWithFilename:@"AppOptions.plist" andLocalOptions:localOptions];
        [appOptions checkForCloudChanges];
    }
}

-(BOOL)isBundledDocumentWithName:(NSString*)inName andType:(NSString*)inType {
    // see if this file is in the bundle
    if ([[NSBundle mainBundle] pathForResource:inName ofType:inType] != nil)
        return YES;
    else
        return NO;
}

-(void)copyBundledDocumentsOfType:(NSString*)inType {
    // copy files from bundle to documents
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *actorPaths = [[NSBundle mainBundle] pathsForResourcesOfType:inType inDirectory:nil];
    for (int i=0; i < actorPaths.count; i++) {
        NSString *bundlePath = [actorPaths objectAtIndex:i];
        NSString *docsPath = [documentsDirectory stringByAppendingPathComponent:[bundlePath lastPathComponent]];
        [fileManager removeItemAtPath:docsPath error:nil];
        [fileManager copyItemAtPath:bundlePath toPath:docsPath error:nil];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    // don't do this if local options are not loaded
    if (!isLocalOptionsLoaded)
        NSLog(@"Local options not loaded in applicationDidBecomeActive");
    else {
        // check for cloud progress changes
        NSLog(@"Ask options to check for cloud changes in applicationDidBecomeActive");
        [[KrAppOptions getSingleton] checkForCloudChanges];
    }
    
    /*
    // check see if options have changed (from another device)
    UINavigationController *navController = (UINavigationController*)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ([navController.topViewController isKindOfClass:[ViewController class]]) {
        ViewController *menuController = (ViewController*)navController.topViewController;
        NSLog(@"Ask menu to wait for updated options in applicationDidBecomeActive");
        if ([menuController waitForUpdatedOptions]) {
            // check for cloud progress changes
            NSLog(@"Ask options to check for cloud changes in applicationDidBecomeActive");
            [[AppOptions getSingleton] checkForCloudChanges];
        }
    }
    else {
        // check for cloud progress changes
        NSLog(@"Ask options to check for cloud changes in applicationDidBecomeActive");
        [[AppOptions getSingleton] checkForCloudChanges];
    }*/
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
