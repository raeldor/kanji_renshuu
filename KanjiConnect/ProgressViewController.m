//
//  ProgressViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 9/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProgressViewController.h"
#import "BlasterGame.h"
#import "LeitnerLogic.h"

@interface ProgressViewController ()

@end

@implementation ProgressViewController

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGB2(r,g,b) [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1.0f]

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // repaint if size changed
    [statsView addObserver:self forKeyPath:@"bounds" options:0 context:nil];
    
    // build an array of connect colors
    int barCount = 6;
    connectColors = [[NSMutableArray alloc] initWithCapacity:barCount];
    [connectColors addObject:UIColorFromRGB2(255.0f, 65.0f, 65.0f)];
//    [connectColors addObject:UIColorFromRGB2(255.0f, 65.0f, 65.0f)];
    [connectColors addObject:UIColorFromRGB2(255.0f, 96.0f, 32.0f)];
//    [connectColors addObject:UIColorFromRGB2(255.0f, 96.0f, 32.0f)];
    [connectColors addObject:UIColorFromRGB2(255.0f, 128.0f, 0.0f)]; // orangeColor
    [connectColors addObject:UIColorFromRGB2(128.0f, 128.0f, 0.0f)]; // orangeColor
    [connectColors addObject:UIColorFromRGB2(128.0f, 164.0f, 0.0f)];
//    [connectColors addObject:UIColorFromRGB2(128.0f, 164.0f, 0.0f)];
    [connectColors addObject:UIColorFromRGB2(0.0f, 200.f, 0.0f)];
//    [connectColors addObject:UIColorFromRGB2(0.0f, 200.f, 0.0f)];
    
    // udpate stats
    [self updateStats];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if (object == statsView && [keyPath isEqualToString:@"bounds"]) {
        // repaint
        [self updateStats];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)setGame:(BlasterGame*)inGame {
    // save game
    game = [inGame retain];
}

-(void)updateGraphView:(UIView*)inGraphView usingStatsArray:(float[])inStatsArray {
    // how many bars?
    int barCount = [LeitnerLogic getDeckCount];
    
    // remove all sub views
    while (inGraphView.subviews.count > 0)
        [[inGraphView.subviews objectAtIndex:0] removeFromSuperview];
    
    // get total word count
    int wordCount = 0;
    for (int i=0; i < barCount; i++)
        wordCount += inStatsArray[i];
    
    // find largest count
    float largestCount = 0.0f;
    for (int i=0; i < barCount; i++) {
        if (inStatsArray[i] > largestCount)
            largestCount = inStatsArray[i];
    }
    
    // now add new super views as graph bars
    float width = inGraphView.frame.size.width/(float)barCount;
    float offset = 0.0f;
    for (int i=0; i < barCount; i++) {
        int bucketCount = inStatsArray[i];
        float heightPercent = (float)bucketCount/(float)largestCount;
        float height = inGraphView.frame.size.height*heightPercent;
        // create sub view
        UIView *barView = [[UIView alloc] initWithFrame:CGRectMake(offset, inGraphView.frame.size.height-height, width, height)];
        barView.backgroundColor = [connectColors objectAtIndex:i];
        [inGraphView addSubview:barView];
        [barView release];
        offset += width;
    }
}

-(IBAction)viewTapped:(id)sender {
    // pop view controller
    [self dismissViewControllerAnimated:YES completion:^{
    }];
// deprecated    [self dismissModalViewControllerAnimated:YES];
}

-(void)updateStats {
    // show how many word we have found
    foundLabel.text = [NSString stringWithFormat:@"Found %lu words using %lu selected kanji.", (unsigned long)[game getWordCount], (unsigned long)[game getKanjiCount]];
    
    // update stats graph
    float leitnerStats[11];
    [game getPlayStatsIntoIntArray:leitnerStats];
    [self updateGraphView:statsView usingStatsArray:leitnerStats];
    
    // format numbers
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    formatter.maximumFractionDigits = 1;
    formatter.minimumFractionDigits = 1;
    NSString *formatted = [formatter stringFromNumber:[NSNumber numberWithFloat:leitnerStats[6]]];
    
    // update % complete label
//    percentCompleteLabel.text = [NSString stringWithFormat:@"You are %@%% of the way to knowing these entries very well for the selected game type.", formatted];
    percentCompleteLabel.text = [NSString stringWithFormat:@"You are %@%% of the way to %@ these kanji well at your JLPT level.", formatted, game.gameType&blGameType_Writing?@"writing":@"reading"];
    
    /*
    // update jouyuu stats graph
    [game getJouyuuStatsIntoIntArray:leitnerStats];
    [self updateGraphView:jouyuuStatsView usingStatsArray:leitnerStats];
    jouyuuPercentCompleteLabel.text = [NSString stringWithFormat:@"You are %d%% of the way to %@ the jouyuu kanji well at your JLPT level.", leitnerStats[5], game.gameType&blGameType_Writing?@"writing":@"reading"];*/
}

-(void)dealloc {
    // release our retains
    [game release];
    
    // super
    [super dealloc];
}

@end
