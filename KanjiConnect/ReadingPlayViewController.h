//
//  PlayController.h
//  KanjiConnect
//
//  Created by Ray Price on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#import "KanjiDictionary.h"
#import "JapaneseDictionary.h"
#import "BuildKanjiView.h"
#import "BlasterGame.h"
#import "BlasterWordSgo.h"
#import "BlasterKanjiSgo.h"
#import "BlasterSentenceSgo.h"
#import "WriteKanjiViewController.h"
#import "WriteKanjiViewControllerDelegate.h"

@interface ReadingPlayViewController : UIViewController <UIGestureRecognizerDelegate, UIAlertViewDelegate, AVAudioPlayerDelegate, BlasterGameDelegate, UITextFieldDelegate, WriteKanjiViewControllerDelegate, KanjiDrawingViewDelegate> {
    IBOutlet UIToolbar *bottomToolbar;
    IBOutlet UIView *playView;
    IBOutlet UIView *writingPopupView;
	IBOutlet UILabel *strokesLabel;
	IBOutlet UIButton *clearButton;
	IBOutlet UIButton *hintButton;
    IBOutlet UIBarButtonItem *menuButton;
    IBOutlet UIBarButtonItem *checkButton;
    IBOutlet UIBarButtonItem *nextButton;
    IBOutlet UIScrollView *sentenceScrollView;
    IBOutlet UIScrollView *meaningScrollView;
    IBOutlet UILabel *sentenceMeaningLabel;
	IBOutlet KanjiDrawingView *drawingView;
	IBOutlet BuildKanjiView *buildView;
	IBOutlet KanjiDrawingView *outlineView;
	IBOutlet KanjiDrawingView *animatedView;
	IBOutlet KanjiDrawingView *finishedView;
    IBOutlet UISlider *drawingAnimationSlider;
    IBOutlet UIButton *readingHelpButton;
    IBOutlet UIButton *writingHelpButton;
    IBOutlet UIView *myInputAccessoryView;
//    IBOutlet NSLayoutConstraint *sentenceFullScreen;
//    IBOutlet NSLayoutConstraint *sentencePartScreen;
    IBOutlet NSLayoutConstraint *animationSliderHeightConstraint;
    IBOutlet NSLayoutConstraint *strokeCountHeightConstraint;
    IBOutlet NSLayoutConstraint *writingControlsHeightConstraint;
    IBOutlet NSLayoutConstraint *writingButtonsHeightConstraint;
    IBOutlet NSLayoutConstraint *meaningFixedHeightConstraint;
    IBOutlet NSLayoutConstraint *meaningHeightConstraint;

    AVAudioPlayer *musicPlayer;
    
//    NSMutableDictionary *systemSounds; // of type SystemSoundID, to cache
    
    // game
    BlasterGame *game;
    
    // sub controller to isolate drawing stuff for re-use
    WriteKanjiViewController *writeKanjiController;
    
    JapaneseDictionary *dictionary;
    KanjiDictionary *kanjiDictionary;
    
    UIPinchGestureRecognizer *pinchRecognizer;
    UITapGestureRecognizer *tapRecognizer;
    UITapGestureRecognizer *doubleTapRecognizer;
    UILongPressGestureRecognizer *longRecognizer;
    UILongPressGestureRecognizer *clearButtonLongRecognizer;
    UISwipeGestureRecognizer *swipeLeftRecognizer;
    UISwipeGestureRecognizer *swipeRightRecognizer;
    
    // default zoom out and stroke width
    float defaultZoom;
    float defaultStrokeWidth;
    
    // states
    BOOL zoomedIn;
    BOOL isZooming;
    
    // index of sentence subview we are checking
    int checkWordIndex;
    int checkKanjiIndex;
    
    // current user focus in view
    BlasterSentenceSgo *newFocusSentence;
    BlasterWordSgo *newFocusWord;
    BlasterKanjiSgo *newFocusKanji;
//    SceneGraphObject *focusSgo;
    
    // make hidden text field for keyboard
    UITextField *hiddenTextField;
    
    // current user focus in scroller
    int scrollFocusWordIndex;
    int scrollFocusKanjiIndex;
    
    // save frames for resize
    CGRect oldScrollViewFrame;
    CGRect oldCheckButtonFrame;
    
    // magnifier
    UIImageView *magView;
    UIImageView *magOverlayView;
    CGImageRef magMask;
    BOOL isMagnifying;
    
    // route change message queue
    NSMutableArray *routeChangeQueue; // of nsstring
}

@property (nonatomic, retain) BlasterGame *game;

-(void)checkNextWord;
-(void)hiddenTextFieldChange:(id)sender;
-(CGAffineTransform)getZoomOutTransform;
-(CGAffineTransform)getZoomTransformForWord:(BlasterWordSgo*)inWord;
-(CGAffineTransform)getZoomTransformForKanji:(BlasterKanjiSgo*)inKanji;
-(IBAction)menuButtonPressed:(id)sender;
-(IBAction)clearButtonPressed:(id)sender;
-(void)clearButtonLongPressed:(id)sender;
-(IBAction)hintButtonPressed:(id)sender;
-(IBAction)checkAnswersButtonPressed:(id)sender;
-(IBAction)nextSentenceButtonPressed:(id)sender;
-(void)viewPinched:(id)sender;
-(void)viewTapped:(id)sender;
-(BOOL)viewTappedDuringReadingGame:(id)sender;
-(BOOL)viewTappedDuringWritingGame:(id)sender;
-(IBAction)previousKanjiButtonPressed:(id)sender;
-(IBAction)nextKanjiButtonPressed:(id)sender;
-(BOOL)moveToNextKanji;
-(BOOL)moveToPreviousKanji;
-(void)viewDoubleTapped:(id)sender;
-(void)viewLongPressed:(id)sender;
-(void)showCurrentSentence;
-(void)playSoundEffectNamed:(NSString*)inEffectName withVolume:(float)inVolume;
-(void)hideDrawingInterface:(BOOL)hideComparison;
-(void)showDrawingInterface:(BOOL)showComparison;
-(void)hideSentenceMeaning;
-(void)showSentenceMeaning;
-(BlasterWordSgo*)getWordTappedForRecognizer:(UIGestureRecognizer*)inRecognizer;
-(BlasterKanjiSgo*)getKanjiTappedForRecognizer:(UIGestureRecognizer*)inRecognizer;
-(void)onKeyboardShow:(NSNotification *)notification;
-(void)onKeyboardHide:(NSNotification *)notification;
-(void)zoomToKanji:(BlasterKanjiSgo*)thisKanji;
-(void)zoomToWord:(BlasterWordSgo*)thisWord;
-(BlasterKanjiSgo*)getNextFocusKanji;
-(BlasterKanjiSgo*)getPreviousFocusKanji;

@end
