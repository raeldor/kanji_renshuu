//
//  PronounceAttempt.m
//  KanjiConnect
//
//  Created by Ray Price on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PronounceAttempt.h"

@implementation PronounceAttempt

@synthesize kanjiPronun;

-(id)initWithPronunication:(NSString*)inPron andKanjiPronunciation:(NSMutableArray*)inKanjiPronun {
    self = [super init];
    if (self) {
        wordPronun = [inPron retain];
        kanjiPronun = [inKanjiPronun retain];
    }
    return self;
}

-(void)dealloc {
    // release our variables
    [wordPronun release];
    [kanjiPronun release];
    
    // call super
    [super dealloc];
}

@end
