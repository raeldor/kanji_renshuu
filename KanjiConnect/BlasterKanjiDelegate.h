//
//  BlasterKanjiDelegate.h
//  KanjiConnect
//
//  Created by Ray Price on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef KanjiConnect_BlasterKanjiDelegate_h
#define KanjiConnect_BlasterKanjiDelegate_h

@class BlasterKanji;

@protocol BlasterKanjiDelegate

-(void)didCompleteKanji:(BlasterKanji*)inKanji;

@end


#endif
