//
//  KanjiPickerCell.h
//  KanjiConnect
//
//  Created by Ray Price on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KanjiPickerCell : UITableViewCell {
    
}

@property (nonatomic, assign) IBOutlet UILabel *kanjiLabel;
@property (nonatomic, assign) IBOutlet UILabel *meaningLabel;
@property (nonatomic, assign) IBOutlet UILabel *readingLabel;

@end
