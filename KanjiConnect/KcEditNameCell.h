//
//  KcEditNameCell.h
//  KanjiConnect
//
//  Created by Ray Price on 7/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KcEditNameCell : UITableViewCell {
    IBOutlet UITextField *nameField;
}

@property (nonatomic, assign) UITextField *nameField;

@end
