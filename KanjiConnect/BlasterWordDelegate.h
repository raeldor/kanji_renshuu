//
//  BlasterWordDelegate.h
//  KanjiConnect
//
//  Created by Ray Price on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef KanjiConnect_BlasterWordDelegate_h
#define KanjiConnect_BlasterWordDelegate_h

@class BlasterWord;

@protocol BlasterWordDelegate

-(void)didCompleteWord:(BlasterWord*)inWord;

@end


#endif
