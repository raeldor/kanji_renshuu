//
//  SentenceWord.m
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BlasterSentenceWord.h"

@implementation BlasterSentenceWord

@synthesize completed;
@synthesize wordEntry;

-(id)initWithSentenceWordEntry:(SampleEntryWord*)inEntry forGame:(BlasterGame*)inGame {
    self = [super init];
    if (self) {
        // save entry and game
        wordEntry = [inEntry retain];
        containerGame = [inGame retain];
        
        // save kanji and kana
        kanji = [wordEntry.kanji retain];
        kana = [wordEntry.kana retain];
        
        // work out furigana and length for this word
        
        // get some wrong furigana examples for radial menu
    }
    return self;
}

-(void)dealloc {
    // release our retains
    [wordEntry release];
    [containerGame release];
    [kanji release];
    [kana release];
    
    // super
    [super dealloc];
}

@end
