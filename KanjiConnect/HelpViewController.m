//
//  HelpViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 10/14/12.
//
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

@synthesize contentView;
@synthesize delegate;
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
}

-(void)viewDidLayoutSubviews {
    // set content size for scroll view
    scrollView.contentSize = contentView.frame.size;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    if (@available(iOS 11, *)) {
        scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
}
    
-(IBAction)closeButtonPressed:(id)sender {
    // call delegate
    [delegate didTapToClose];
    
    // pop
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    
// deprecated    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
