//
//  KcKanjiListCell.h
//  KanjiConnect
//
//  Created by Ray Price on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KcKanjiListCell : UITableViewCell {
}

@property (nonatomic, assign) IBOutlet UILabel *listNameLabel;
@property (nonatomic, assign) IBOutlet UISegmentedControl *readingSegment;
@property (nonatomic, assign) IBOutlet UISegmentedControl *writingSegment;


@end
