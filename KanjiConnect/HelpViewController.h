//
//  HelpViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 10/14/12.
//
//

#import <UIKit/UIKit.h>
#import "TapToCloseDelegate.h"

@interface HelpViewController : UIViewController

-(IBAction)closeButtonPressed:(id)sender;

@property (nonatomic, assign) IBOutlet UIView *contentView;
@property (nonatomic, assign) IBOutlet UIScrollView *scrollView;
@property (nonatomic, assign) id<TapToCloseDelegate> delegate;

@end
