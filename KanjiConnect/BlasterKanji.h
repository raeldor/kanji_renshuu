//
//  BlasterKanji.h
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KanjiEntry.h"
#import "BlasterKanjiDelegate.h"

@class BlasterGame;

@interface BlasterKanji : NSObject {
    KanjiEntry *kanjiEntry;
    BlasterGame *containerGame;
    NSString *meaning;
    NSString *kanji;
    BOOL toBeCompleted;
    BOOL completed;
    
    BOOL hadToLook; // user had to get help
    float finalScore;
    
    id<BlasterKanjiDelegate> delegate;
}

-(id)initWithKanjiEntry:(KanjiEntry*)inEntry toBeCompleted:(BOOL)inToBeCompleted forGame:(BlasterGame*)inGame;
-(id)initWithKanjiString:(NSString*)inString forGame:(BlasterGame*)inGame;

@property (nonatomic, retain) KanjiEntry *kanjiEntry;
@property (nonatomic, retain) BlasterGame *containerGame;
@property (nonatomic, assign) BOOL toBeCompleted;
@property (nonatomic, assign) BOOL completed;
@property (nonatomic, assign) BOOL hadToLook;
@property (nonatomic, retain) NSString *meaning;
@property (nonatomic, retain) NSString *kanji;
@property (nonatomic, retain) NSMutableArray *wrongMeanings;
@property (nonatomic, retain) NSMutableArray *words;
@property (nonatomic, assign) id<BlasterKanjiDelegate> delegate;
@property (nonatomic, assign) float finalScore;

@end
