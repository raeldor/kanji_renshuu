//
//  KrAppOptions.m
//  KanjiConnect
//
//  Created by Ray Price on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KrAppOptions.h"
//#import "KrAppOptionsDocument.h"
#import "KrLocalOptions.h"
#import "AppDelegate.h"

//static KrAppOptions *appOptionsSingleton;

@implementation KrAppOptions

+(KrAppOptions*)getSingleton {
    // get application delegate
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // return local options
    return appDelegate.appOptions;
}

-(id)initWithFilename:(NSString*)inFilename andLocalOptions:(LocalOptions*)inLocalOptions {
	if ((self = [super initWithFilename:inFilename andLocalOptions:inLocalOptions])) {
    }
    return self;
}


// translate between blaster card and flash card type
-(int)getLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType {
    // create temporary flash card to bridge gap
    // SENSE IS ALWAYS 1 FOR KANJI RENSHUU, OTHERWISE IF YOU PICK A WORD WITH SENSE '3'
    // BUT THE SENTENCE USAGE IS '0' IT WILL NEVER GET IT'S DECK INCREMENTED OR LAST SHOWN
    // DATE UPDATED
    FlashCard *tempCard = [[[FlashCard alloc] initWithKanji:inKanji kana:inKana senseIndex:inSenseIndex==0?1:1] autorelease];
    return [self getLeitnerDeckNumberForCard:tempCard ofType:@"Dictionary" withGameType:inBlasterGameType&blGameType_Writing?gameType_WriteInSentence:gameType_ReadInSentence];
}

-(void)setToMaxLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType autoSave:(BOOL)inAutoSave {
    // create temporary flash card to bridge gap
    FlashCard *tempCard = [[[FlashCard alloc] initWithKanji:inKanji kana:inKana senseIndex:inSenseIndex==0?1:1] autorelease];
    return [self setToMaxLeitnerDeckNumberForCard:tempCard ofType:@"Dictionary" withGameType:inBlasterGameType&blGameType_Writing?gameType_WriteInSentence:gameType_ReadInSentence autoSave:inAutoSave];
}

-(void)setLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType toValue:(int)inValue autoSave:(BOOL)inAutoSave {
    // create temporary flash card to bridge gap
    FlashCard *tempCard = [[[FlashCard alloc] initWithKanji:inKanji kana:inKana senseIndex:inSenseIndex==0?1:1] autorelease];
    return [self setLeitnerDeckNumberForCard:tempCard ofType:@"Dictionary" withGameType:inBlasterGameType&blGameType_Writing?gameType_WriteInSentence:gameType_ReadInSentence toValue:inValue autoSave:inAutoSave];
}

-(void)incrementLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType autoSave:(BOOL)inAutoSave {
    // create temporary flash card to bridge gap
    FlashCard *tempCard = [[[FlashCard alloc] initWithKanji:inKanji kana:inKana senseIndex:inSenseIndex==0?1:1] autorelease];
    return [self incrementLeitnerDeckNumberForCard:tempCard ofType:@"Dictionary" withGameType:inBlasterGameType&blGameType_Writing?gameType_WriteInSentence:gameType_ReadInSentence autoSave:inAutoSave];
}

-(void)decrementLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType autoSave:(BOOL)inAutoSave {
    FlashCard *tempCard = [[[FlashCard alloc] initWithKanji:inKanji kana:inKana senseIndex:inSenseIndex==0?1:1] autorelease];
    return [self decrementLeitnerDeckNumberForCard:tempCard ofType:@"Dictionary" withGameType:inBlasterGameType&blGameType_Writing?gameType_WriteInSentence:gameType_ReadInSentence autoSave:inAutoSave];
}

-(NSUInteger)getDaysSinceLastShownForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType {
    FlashCard *tempCard = [[[FlashCard alloc] initWithKanji:inKanji kana:inKana senseIndex:inSenseIndex==0?1:1] autorelease];
    return [self getDaysSinceLastShownForCard:tempCard ofType:@"Dictionary" withGameType:inBlasterGameType&blGameType_Writing?gameType_WriteInSentence:gameType_ReadInSentence];
}

-(NSUInteger)getMinutesSinceLastShownForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType {
    FlashCard *tempCard = [[[FlashCard alloc] initWithKanji:inKanji kana:inKana senseIndex:inSenseIndex==0?1:1] autorelease];
    return [self getMinutesSinceLastShownForCard:tempCard ofType:@"Dictionary" withGameType:inBlasterGameType&blGameType_Writing?gameType_WriteInSentence:gameType_ReadInSentence];
}

-(void)updateLastShownTimestampForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType autoSave:(BOOL)inAutoSave {
    FlashCard *tempCard = [[[FlashCard alloc] initWithKanji:inKanji kana:inKana senseIndex:inSenseIndex==0?1:1] autorelease];
    return [self updateLastShownTimestampForCard:tempCard ofType:@"Dictionary" withGameType:inBlasterGameType&blGameType_Writing?gameType_WriteInSentence:gameType_ReadInSentence autoSave:inAutoSave];
}

-(NSDate*)getDateLastShownForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType {
    FlashCard *tempCard = [[[FlashCard alloc] initWithKanji:inKanji kana:inKana senseIndex:inSenseIndex==0?1:1] autorelease];
    return [self getDateLastShownForCard:tempCard ofType:@"Dictionary" withGameType:inBlasterGameType&blGameType_Writing?gameType_WriteInSentence:gameType_ReadInSentence];
}



/*
-(int)getLeitnerDeckNumberForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting {
    // for now, ignore sense index, always set to '1' - TO DO!
    NSArray *components = [inKey componentsSeparatedByString:@"\r\n"];
    inKey = [NSString stringWithFormat:@"%@\r\n%@\r\n1", components[0], components[1]];
    
    // get dictionary entry
    NSString *leitnerKey = inIsWriting?@"LeitnerWriting":@"LeitnerReading";
    NSMutableDictionary *leitner = [appOptions objectForKey:leitnerKey];
    if (leitner == nil)
        return -3; // first deck by default
    NSMutableDictionary *deckNumbers = [leitner objectForKey:@"DeckNumbers"];
    if (deckNumbers == nil)
        return -3; // first deck by default
    NSNumber *thisNumber = [deckNumbers objectForKey:inKey];
    if (thisNumber == nil)
        return -3; // first deck by default
    
    // return deck number
    return [thisNumber intValue];
}

-(void)setLeitnerDeckNumberForKey:(NSString*)inKey toValue:(int)inValue isWritingGame:(BOOL)inIsWriting{
    // for now, ignore sense index, always set to '1' - TO DO!
    NSArray *components = [inKey componentsSeparatedByString:@"\r\n"];
    inKey = [NSString stringWithFormat:@"%@\r\n%@\r\n1", components[0], components[1]];
    
    // get dictionary entry
    NSString *leitnerKey = inIsWriting?@"LeitnerWriting":@"LeitnerReading";
    NSMutableDictionary *leitner = [appOptions objectForKey:leitnerKey];
    if (leitner == nil) {
        leitner = [NSMutableDictionary dictionaryWithCapacity:1];
        [appOptions setObject:leitner forKey:leitnerKey];
    }
    NSMutableDictionary *deckNumbers = [leitner objectForKey:@"DeckNumbers"];
    if (deckNumbers == nil) {
        deckNumbers = [NSMutableDictionary dictionaryWithCapacity:1];
        [leitner setObject:deckNumbers forKey:@"DeckNumbers"];
    }
    
    // increment leitner deck number
    if (inValue >=-3 && inValue <= 7) {
        [deckNumbers setObject:[NSNumber numberWithInt:inValue] forKey:inKey];
    }
}

-(void)incrementLeitnerDeckNumberForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting {
    // for now, ignore sense index, always set to '1' - TO DO!
    NSArray *components = [inKey componentsSeparatedByString:@"\r\n"];
    inKey = [NSString stringWithFormat:@"%@\r\n%@\r\n1", components[0], components[1]];
    
    // get dictionary entry
    NSString *leitnerKey = inIsWriting?@"LeitnerWriting":@"LeitnerReading";
    NSMutableDictionary *leitner = [appOptions objectForKey:leitnerKey];
    if (leitner == nil) {
        leitner = [NSMutableDictionary dictionaryWithCapacity:1];
        [appOptions setObject:leitner forKey:leitnerKey];
    }
    NSMutableDictionary *deckNumbers = [leitner objectForKey:@"DeckNumbers"];
    if (deckNumbers == nil) {
        deckNumbers = [NSMutableDictionary dictionaryWithCapacity:1];
        [leitner setObject:deckNumbers forKey:@"DeckNumbers"];
    }
    
    // get existing deck number
    NSNumber *thisNumber = [deckNumbers objectForKey:inKey];
    if (thisNumber == nil) {
        NSLog(@"NO PREVIOUS LEITNER DECK FOUND FOR KEY %@, USED 1", inKey);
        thisNumber = [NSNumber numberWithInt:1]; // first deck by default
    }

    // increment leitner deck number
    int deckNumber = [thisNumber intValue];
    if (deckNumber < 7) {
        [deckNumbers setObject:[NSNumber numberWithInt:++deckNumber] forKey:inKey];
    }
}

-(void)decrementLeitnerDeckNumberForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting {
    // for now, ignore sense index, always set to '1' - TO DO!
    NSArray *components = [inKey componentsSeparatedByString:@"\r\n"];
    inKey = [NSString stringWithFormat:@"%@\r\n%@\r\n1", components[0], components[1]];
    
    // get dictionary entry
    NSString *leitnerKey = inIsWriting?@"LeitnerWriting":@"LeitnerReading";
    NSMutableDictionary *leitner = [appOptions objectForKey:leitnerKey];
    if (leitner == nil) {
        leitner = [NSMutableDictionary dictionaryWithCapacity:1];
        [appOptions setObject:leitner forKey:leitnerKey];
    }
    NSMutableDictionary *deckNumbers = [leitner objectForKey:@"DeckNumbers"];
    if (deckNumbers == nil) {
        deckNumbers = [NSMutableDictionary dictionaryWithCapacity:1];
        [leitner setObject:deckNumbers forKey:@"DeckNumbers"];
    }
    
    // get existing deck number
    NSNumber *thisNumber = [deckNumbers objectForKey:inKey];
    if (thisNumber == nil)
        thisNumber = [NSNumber numberWithInt:1]; // first deck by default
    
    // increment leitner deck number
    int deckNumber = [thisNumber intValue];
    if (deckNumber > -3) {
        [deckNumbers setObject:[NSNumber numberWithInt:--deckNumber] forKey:inKey];
    }
}

-(int)getDaysSinceLastShownForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting {
    // for now, ignore sense index, always set to '1' - TO DO!
    NSArray *components = [inKey componentsSeparatedByString:@"\r\n"];
    inKey = [NSString stringWithFormat:@"%@\r\n%@\r\n1", components[0], components[1]];
    
    // get dictionary entry
    NSString *leitnerKey = inIsWriting?@"LeitnerWriting":@"LeitnerReading";
    NSMutableDictionary *leitner = [appOptions objectForKey:leitnerKey];
    if (leitner == nil)
        return 0; // must be 1 minute now, otherwise it gets put into 365 deck!
//        return 365; // 365 days by default
    NSMutableDictionary *dates = [leitner objectForKey:@"LastShownDates"];
    if (dates == nil)
        return 0; // must be 1 minute now, otherwise it gets put into 365 deck!
//        return 365; // 365 days by default
    NSDate *lastShownDate = [dates objectForKey:inKey];
    if (lastShownDate == nil)
        return 0; // must be 1 minute now, otherwise it gets put into 365 deck!
//        return 365; // 365 days by default
    
    // return days since shown
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    unsigned int unitFlags = NSDayCalendarUnit;
    NSDateComponents *conversionInfo = [sysCalendar components:unitFlags fromDate:lastShownDate toDate:[NSDate date] options:0];
//    NSLog(@"Days since shown %d for last shown date %@ and now date %@", [conversionInfo day], lastShownDate, [NSDate date]);
    return [conversionInfo day];
}

-(int)getMinutesSinceLastShownForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting {
    // for now, ignore sense index, always set to '1' - TO DO!
    NSArray *components = [inKey componentsSeparatedByString:@"\r\n"];
    inKey = [NSString stringWithFormat:@"%@\r\n%@\r\n1", components[0], components[1]];
    
    // get dictionary entry
    NSString *leitnerKey = inIsWriting?@"LeitnerWriting":@"LeitnerReading";
    NSMutableDictionary *leitner = [appOptions objectForKey:leitnerKey];
    if (leitner == nil)
        return 1; // must be 1 minute now, otherwise it gets put into 365 deck!
//        return 365*24*60; // 365 days by default
    NSMutableDictionary *dates = [leitner objectForKey:@"LastShownDates"];
    if (dates == nil)
        return 1; // must be 1 minute now, otherwise it gets put into 365 deck!
//        return 365*24*60; // 365 days by default
    NSDate *lastShownDate = [dates objectForKey:inKey];
    if (lastShownDate == nil)
        return 1; // must be 1 minute now, otherwise it gets put into 365 deck!
//        return 365*24*60; // 365 days by default
    
    // return days since shown
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    unsigned int unitFlags = NSMinuteCalendarUnit;
    NSDateComponents *conversionInfo = [sysCalendar components:unitFlags fromDate:lastShownDate toDate:[NSDate date] options:0];
//    NSLog(@"Minutes since shown %d for last shown date %@ and now date %@", [conversionInfo minute], lastShownDate, [NSDate date]);
    return [conversionInfo minute];
}

-(void)updateDaysSinceLastShownForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting {
    // for now, ignore sense index, always set to '1' - TO DO!
    NSArray *components = [inKey componentsSeparatedByString:@"\r\n"];
    inKey = [NSString stringWithFormat:@"%@\r\n%@\r\n1", components[0], components[1]];
    
    // get dictionary entry
    NSString *leitnerKey = inIsWriting?@"LeitnerWriting":@"LeitnerReading";
    NSMutableDictionary *leitner = [appOptions objectForKey:leitnerKey];
    if (leitner == nil) {
        leitner = [NSMutableDictionary dictionaryWithCapacity:1];
        [appOptions setObject:leitner forKey:leitnerKey];
    }
    NSMutableDictionary *dates = [leitner objectForKey:@"LastShownDates"];
    if (dates == nil) {
        dates = [NSMutableDictionary dictionaryWithCapacity:1];
        [leitner setObject:dates forKey:@"LastShownDates"];
    }
    
    // update last shown date
    [dates setObject:[NSDate date] forKey:inKey];
}


// merge flash changes
-(void)mergeChangesFromOptions:(NSMutableDictionary*)fromOptions toOptions:(NSMutableDictionary*)toOptions {
    // call super first
    [super mergeChangesFromOptions:fromOptions toOptions:toOptions];
    
    NSLog(@"Found and merging changes");
    
    // loop through last shown dates
    for (int isWriting=0; isWriting < 2; isWriting++) {
        NSString *leitnerKey = isWriting?@"LeitnerWriting":@"LeitnerReading";
        NSMutableDictionary *thisLeitner = [fromOptions objectForKey:leitnerKey];
        NSMutableDictionary *thisDates = [thisLeitner objectForKey:@"LastShownDates"];
        NSMutableDictionary *thisDecks = [thisLeitner objectForKey:@"DeckNumbers"];
        NSMutableDictionary *currentLeitner = [toOptions objectForKey:leitnerKey];
        NSMutableDictionary *currentDates = [currentLeitner objectForKey:@"LastShownDates"];
        NSMutableDictionary *currentDecks = [currentLeitner objectForKey:@"DeckNumbers"];
        if (thisDates != nil) {
            for(NSString *cardKey in thisDates) {
                // see if this exists in current dates
                NSDate *thisDate = [thisDates objectForKey:cardKey];
                NSNumber *thisDeck = [thisDecks objectForKey:cardKey];
                if ([currentDates objectForKey:cardKey] == nil) {
                    // copy last seen and leitner info
                    [currentDates setObject:thisDate forKey:cardKey];
                    if (thisDeck != nil)
                        [currentDecks setObject:thisDeck forKey:cardKey];
                }
                else {
                    // compare the dates
                    NSDate *currentDate = [currentDates objectForKey:cardKey];
                    if ([thisDate compare:currentDate] == NSOrderedDescending) {
                        // thisDate is later, so copy info
                        // copy last seen and leitner info
                        [currentDates setObject:thisDate forKey:cardKey];
                        if (thisDeck != nil)
                            [currentDecks setObject:thisDeck forKey:cardKey];
                    }
                }
            }
        }
    }
}
*/

-(void)dealloc {
//	[appOptions release];
	[super dealloc];
}

@end
