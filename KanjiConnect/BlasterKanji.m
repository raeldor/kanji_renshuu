//
//  BlasterKanji.m
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BlasterKanji.h"
#import "BlasterGame.h"
#import "BlasterWord.h"
#import "JmwDatabase.h"
#import "JapaneseDictionary.h"

@implementation BlasterKanji

@synthesize kanjiEntry;
@synthesize containerGame;
@synthesize toBeCompleted;
@synthesize completed;
@synthesize hadToLook;
@synthesize meaning;
@synthesize kanji;
@synthesize wrongMeanings;
@synthesize words;
@synthesize delegate;
@synthesize finalScore;

-(id)initWithKanjiEntry:(KanjiEntry*)inEntry toBeCompleted:(BOOL)inToBeCompleted forGame:(BlasterGame*)inGame {
    self = [super init];
    if (self) {
        // save entry and game
        kanjiEntry = [inEntry retain];
        containerGame = [inGame retain];
        toBeCompleted = inToBeCompleted;
        completed = !toBeCompleted;
        
        // save meaning (first by default)
        meaning = [[inEntry.meanings objectAtIndex:0] retain];
        kanji = [inEntry.kanji retain];
    }
    return self;
}

-(id)initWithKanjiString:(NSString*)inString forGame:(BlasterGame*)inGame {
    self = [super init];
    if (self) {
        // save entry and game
        containerGame = [inGame retain];
        toBeCompleted = NO;
        completed = YES;
        
        // save meaning (first by default)
        meaning = @"None";
        kanji = [inString retain];
    }
    return self;
}

-(void)setCompleted:(BOOL)inCompleted {
    // set flag
    completed = inCompleted;
    
    // call delegate to tell them it's completed
    if (completed) {
        [delegate didCompleteKanji:self];
    }
}

-(void)dealloc {
    // free our retains
    [kanjiEntry release];
    [containerGame release];
    [words release];
    [wrongMeanings release];
    [meaning release];
    [kanji release];
    
    // super
    [super dealloc];
}

@end
