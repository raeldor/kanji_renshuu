//
//  KanjiPickerViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KanjiDictionary.h"
#import "KanjiPickerDelegate.h"

@interface KanjiPickerViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UILongPressGestureRecognizer *longPressRecognizer;
    IBOutlet UISearchDisplayController *searchController;
    
    KanjiDictionary *kanjiDictionary;
    
    id<KanjiPickerDelegate> delegate;
    
	// selected kanji
	NSMutableArray *selectedKanjis; // array of kanji strings
	NSMutableArray *searchResults; // array of indexes into the actual list
}

@property (nonatomic, assign) id<KanjiPickerDelegate> delegate;

-(int)numberOfEntriesInGroup:(int)groupIndex;
-(KanjiEntry*)entryAtGroupIndex:(int)groupIndex entryIndex:(int)entryIndex;
-(IBAction)selectButtonPressed:(id)sender;
-(IBAction)cancelButtonPressed:(id)sender;
-(void)updateSearchResultsUsingSearchController:(UISearchDisplayController*)controller;
-(IBAction)handleLongPressGesture:(UIGestureRecognizer*)gestureRecognizer;

@end
