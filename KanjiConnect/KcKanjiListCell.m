//
//  KcKanjiListCell.m
//  KanjiConnect
//
//  Created by Ray Price on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KcKanjiListCell.h"

@implementation KcKanjiListCell

@synthesize listNameLabel;
@synthesize readingSegment;
@synthesize writingSegment;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
