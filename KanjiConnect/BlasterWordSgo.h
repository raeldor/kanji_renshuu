//
//  BlasterWordSgo.h
//  KanjiConnect
//
//  Created by Ray Price on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SceneGraphObject.h"
#import "BlasterWord.h"

@interface BlasterWordSgo : SceneGraphObject {
    IBOutlet UILabel *kanaLabel;
    IBOutlet UILabel *meaningLabel;
    
    BlasterWord *word;

    int helpStage; // current help stage
}

+(BlasterWordSgo*)BlasterWordSgoWithWord:(BlasterWord*)inWord;
-(void)setWord:(BlasterWord*)inWord;
-(void)setAsCompleted;
-(void)helpButtonPressed:(id)sender;
-(BOOL)clearButtonPressed:(id)sender;
-(void)setKanjiHiddenStateTo:(BOOL)inState;

@property (nonatomic, retain) BlasterWord *word;
@property (nonatomic, retain) UILabel *meaningLabel;
@property (nonatomic, retain) UILabel *kanaLabel;

@end
