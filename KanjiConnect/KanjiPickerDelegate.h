//
//  KanjiPickerDelegate.h
//  KanjiConnect
//
//  Created by Ray Price on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef KanjiConnect_KanjiPickerDelegate_h
#define KanjiConnect_KanjiPickerDelegate_h

@protocol KanjiPickerDelegate

-(void)didMakeSelection:(id)sender selection:(NSMutableArray*)inSelection;

@end

#endif
