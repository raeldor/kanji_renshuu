//
//  SentenceWord.h
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SampleEntryWord.h"
#import "BlasterGame.h"

@interface BlasterSentenceWord : NSObject {
    SampleEntryWord *wordEntry;
    BlasterGame *containerGame;
    NSString *kanji;
    NSString *kana;
    NSString *furigana;
    NSString *furiganaSize;
    NSMutableArray *wrongFurigana; // of type nsstring for radial menu
    BOOL completed;
}

-(id)initWithSentenceWordEntry:(SampleEntryWord*)inEntry forGame:(BlasterGame*)inGame;

@property (nonatomic, assign) BOOL completed;
@property (nonatomic, assign) SampleEntryWord *wordEntry;

@end
