//
//  AboutViewController.h
//  JapaneseMyWay
//
//  Created by Ray on 8/9/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "AboutViewController.h"


@implementation AboutViewController

@synthesize ackWebView;
@synthesize versionLabel;
@synthesize productLabel;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set html text
	ackWebView.delegate = self;
	[ackWebView loadHTMLString:@"<HTML><P>For support and more information, please visit the official <a href=""http://www.rakudasoft.com"">Rakudasoft</a> website.</P><P>This package uses the <a href=""http://www.csse.monash.edu.au/~jwb/edict.html"">EDICT</a> and <a href=""http://www.csse.monash.edu.au/~jwb/kanjidic.html"">KANJIDIC</a> dictionary files.  These files are the property of the <a href=""http://www.edrdg.org/""> Electronic Dictionary Research and Development Group</a>, and are used in conformance with the Group's <a href=""http://www.edrdg.org/edrdg/licence.html"">licence</a>.</P><P>This package also uses the <a href=""http://kanji.japan-diary.com/"">Kanji SVG</a> stroke data courtesy of special licence by Dr. Ulrich Apel.</P><P>Sample sentences are courtesy of the <a href=""http://tatoeba.org"">Tatoeba Project</a> and used under the Creative Commons licence.</P></HTML>" baseURL:nil];
	
	// set version text
    versionLabel.text = [NSString stringWithFormat:@"v%@ build %@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],  [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
	productLabel.text = [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return false;
    }
    return true;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // set alpha back, this avoids the white flash when it first comes up
    webView.alpha = 1.0f;
    
    // remove padding around webview
    NSString *padding = @"document.body.style.margin='0';document.body.style.padding = '0'";
    [webView stringByEvaluatingJavaScriptFromString:padding];
    
    // set css
//    NSString *cssString = @":root {color-scheme:light dark;} @media (prefers-color-scheme:dark) {background-color:black;color:white;} body {background-color:white;color:black;}"; // 1
    NSString *cssString = @":root {color-scheme:light dark;}"; // 1
    NSString *javascriptString = @"var style = document.createElement('style'); style.innerHTML = '%@'; document.head.appendChild(style)"; // 2
    NSString *javascriptWithCSSString = [NSString stringWithFormat:javascriptString, cssString]; // 3
    [webView stringByEvaluatingJavaScriptFromString:javascriptWithCSSString]; // 4
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
	[ackWebView release];
	[versionLabel release];
    [super dealloc];
}


@end
