//
//  KrLocalOptions.m
//  KanjiConnect
//
//  Created by Ray Price on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KrLocalOptions.h"
#import "AppDelegate.h"

//static KrLocalOptions *localOptionsSingleton;

@implementation KrLocalOptions

+(KrLocalOptions*)getSingleton {
    // get application delegate
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // return local options
    return appDelegate.localOptions;
}

/*
+(KrLocalOptions*)getSingleton {
    // create singleton if it's not already created
    if (localOptionsSingleton == nil) {
        // create app options
        localOptionsSingleton = [[KrLocalOptions alloc] init];
    }
    return localOptionsSingleton;
}*/

/*
-(id)init {
     // initialize
     if (self = [super init]) {
         // load from plist
         NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
         NSString *documentsDirectory = [paths objectAtIndex:0];
         NSString *path = [documentsDirectory stringByAppendingPathComponent:@"LocalOptions.plist"];
         if ([[NSFileManager defaultManager] fileExistsAtPath:path])
             localOptions = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
         else
             localOptions = [[NSMutableDictionary alloc] init];
     }
    
     return self;
}*/

// implement methods to add to dictionary instead of synthesizing
-(NSString*)lastVersionNumber {
    // get dictionary entry
	NSString *lastVersionNumber = [localOptions objectForKey:@"LastVersionNumber"];
	if (lastVersionNumber == nil)
        return @"x.x.x";
    return lastVersionNumber;
}

-(void)setLastVersionNumber:(NSString*)versionNumber {
    // set dictionary entry
    [localOptions setObject:versionNumber forKey:@"LastVersionNumber"];
}

/*
// implement methods to add to dictionary instead of synthesizing
-(BOOL)cloudWarningShown {
    // get dictionary entry
	NSNumber *shownFlagNumber = [localOptions objectForKey:@"CloudWarningShown"];
    if (shownFlagNumber == nil)
        return NO;
    return [shownFlagNumber boolValue];
}

-(void)setCloudWarningShown:(BOOL)shownFlag {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithBool:shownFlag] forKey:@"CloudWarningShown"];
}

// implement methods to add to dictionary instead of synthesizing
-(BOOL)isUsingCloud {
    // get dictionary entry
	NSNumber *usingFlagNumber = [localOptions objectForKey:@"IsUsingCloud"];
    if (usingFlagNumber == nil)
        return NO;
    return [usingFlagNumber boolValue];
}

-(void)setIsUsingCloud:(BOOL)usingFlag {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithBool:usingFlag] forKey:@"IsUsingCloud"];
}
*/

// implement methods to add to dictionary instead of synthesizing
-(BOOL)welcomeMessageShown {
    // get dictionary entry
	NSNumber *welcomeMessageShownNumber = [localOptions objectForKey:@"WelcomeMessageShown"];
    if (welcomeMessageShownNumber == nil)
        return NO;
    return [welcomeMessageShownNumber boolValue];
}

-(void)setWelcomeMessageShown:(BOOL)welcomeFlag {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithBool:welcomeFlag] forKey:@"WelcomeMessageShown"];
}

// implement methods to add to dictionary instead of synthesizing
-(BOOL)hideAllFurigana {
    // get dictionary entry
    NSNumber *hideAllFuriganaNumber = [localOptions objectForKey:@"HideAllFurigana"];
    if (hideAllFuriganaNumber == nil)
        return NO;
    return [hideAllFuriganaNumber boolValue];
}

-(void)setHideAllFurigana:(BOOL)hideFlag {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithBool:hideFlag] forKey:@"HideAllFurigana"];
}

// implement methods to add to dictionary instead of synthesizing
-(BOOL)hideTranslation {
    // get dictionary entry
    NSNumber *hideTranslationNumber = [localOptions objectForKey:@"HideTranslation"];
    if (hideTranslationNumber == nil)
        return NO;
    return [hideTranslationNumber boolValue];
}

-(void)setHideTranslation:(BOOL)hideFlag {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithBool:hideFlag] forKey:@"HideTranslation"];
}


// implement methods to add to dictionary instead of synthesizing
-(kcJlptLevel)showWordsFor {
//    -(kcJlptLevel)showWordsFor {
    // get dictionary entry
	NSNumber *showWordsForNumber = [localOptions objectForKey:@"ShowWordsFor"];
	if (showWordsForNumber == nil)
        // jlpt 2 by default
        return 3;
    return [showWordsForNumber intValue];
}

-(void)setShowWordsFor:(kcJlptLevel)inShowLevel {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInt:inShowLevel] forKey:@"ShowWordsFor"];
}

// implement methods to add to dictionary instead of synthesizing
-(float)passAccuracy {
    // get dictionary entry
	NSNumber *passAccuracyNumber = [localOptions objectForKey:@"PassAccuracy"];
	if (passAccuracyNumber == nil)
        // 70% by default
        return 0.7f;
    return [passAccuracyNumber floatValue];
}

-(void)setPassAccuracy:(float)passAccuracy {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithFloat:passAccuracy] forKey:@"PassAccuracy"];
}

// implement methods to add to dictionary instead of synthesizing
-(NSUInteger)readingStudyFreeSwitchValue {
    // get dictionary entry
	NSNumber *readingStudyFreeSwitchValueNumber = [localOptions objectForKey:@"ReadingStudyFreeSwitchValue"];
	if (readingStudyFreeSwitchValueNumber == nil)
        // free play by default
        return 1;
    return [readingStudyFreeSwitchValueNumber integerValue];
}

-(void)setReadingStudyFreeSwitchValue:(NSUInteger)inValue {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInteger:inValue] forKey:@"ReadingStudyFreeSwitchValue"];
}

// implement methods to add to dictionary instead of synthesizing
-(NSUInteger)writingStudyFreeSwitchValue {
    // get dictionary entry
	NSNumber *writingStudyFreeSwitchValueNumber = [localOptions objectForKey:@"WritingStudyFreeSwitchValue"];
	if (writingStudyFreeSwitchValueNumber == nil)
        // free play by default
        return 1;
    return [writingStudyFreeSwitchValueNumber integerValue];
}

-(void)setWritingStudyFreeSwitchValue:(NSUInteger)inValue {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInteger:inValue] forKey:@"WritingStudyFreeSwitchValue"];
}


// implement methods to add to dictionary instead of synthesizing
-(NSUInteger)maxBatchSize {
    // get dictionary entry
	NSNumber *maxBatchSizeNumber = [localOptions objectForKey:@"MaxBatchSize"];
	if (maxBatchSizeNumber == nil)
        // 10 by default
        return 10;
    return [maxBatchSizeNumber integerValue];
}

-(void)setMaxBatchSize:(NSUInteger)maxBatchSize {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInteger:maxBatchSize] forKey:@"MaxBatchSize"];
}

// implement methods to add to dictionary instead of synthesizing
-(kcPickWordsUsing)pickWordsUsing {
    // get dictionary entry
	NSNumber *pickWordsUsingNumber = [localOptions objectForKey:@"PickWordsUsing"];
	if (pickWordsUsingNumber == nil)
        // only by default
        return 0;
    return [pickWordsUsingNumber intValue];
}

-(void)setPickWordsUsing:(kcPickWordsUsing)inPickWordsUsing {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInt:inPickWordsUsing] forKey:@"PickWordsUsing"];
}

-(int)getReadingStudyStatusForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName {
    // get dictionary entry
    NSMutableDictionary *studyStatus = [localOptions objectForKey:@"ReadingStudyStatus"];
    if (studyStatus == nil)
        return 2; // not studying by default
    
    // return known/unknown
    NSString *key = [NSString stringWithFormat:@"%@\n%@", inDeckName, inSectionName];
    NSNumber *studyStatusNumber = [studyStatus objectForKey:key];
    if (studyStatusNumber == nil)
        return 2; // unknown by default
    else
        return [studyStatusNumber intValue];
}

-(void)setReadingStudyStatusForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName toValue:(NSUInteger)inValue {
    // get dictionary entry
    NSMutableDictionary *studyStatus = [localOptions objectForKey:@"ReadingStudyStatus"];
    if (studyStatus == nil) {
        studyStatus = [NSMutableDictionary dictionaryWithCapacity:1];
        [localOptions setObject:studyStatus forKey:@"ReadingStudyStatus"];
    }
    
    // update known/unknown
    NSString *key = [NSString stringWithFormat:@"%@\n%@", inDeckName, inSectionName];
    [studyStatus setObject:[NSNumber numberWithInteger:inValue] forKey:key];
}

-(int)getWritingStudyStatusForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName {
    // get dictionary entry
    NSMutableDictionary *studyStatus = [localOptions objectForKey:@"WritingStudyStatus"];
    if (studyStatus == nil)
        return 2; // not studying by default
    
    // return known/unknown
    NSString *key = [NSString stringWithFormat:@"%@\n%@", inDeckName, inSectionName];
    NSNumber *studyStatusNumber = [studyStatus objectForKey:key];
    if (studyStatusNumber == nil)
        return 2; // unknown by default
    else
        return [studyStatusNumber intValue];
}

-(void)setWritingStudyStatusForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName toValue:(NSUInteger)inValue {
    // get dictionary entry
    NSMutableDictionary *studyStatus = [localOptions objectForKey:@"WritingStudyStatus"];
    if (studyStatus == nil) {
        studyStatus = [NSMutableDictionary dictionaryWithCapacity:1];
        [localOptions setObject:studyStatus forKey:@"WritingStudyStatus"];
    }
    
    // update known/unknown
    NSString *key = [NSString stringWithFormat:@"%@\n%@", inDeckName, inSectionName];
    [studyStatus setObject:[NSNumber numberWithInteger:inValue] forKey:key];
}

/*
-(void)save {
	// save options back out
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"LocalOptions.plist"];
    
    // when on a seperate thread, we have to clone the dictionary before we save.  I think
    // this is because we get access error if it changes on other thread during save
    [localOptions writeToFile:path atomically:NO];
}*/

-(void)dealloc {
	[localOptions release];
	[super dealloc];
}
@end
