//
//  OptionsTableViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KrAppOptions.h"
#import "KrLocalOptions.h"

@interface OptionsTableViewController : UITableViewController {
    IBOutlet UISegmentedControl *showWordsForSegment;
    IBOutlet UISegmentedControl *pickWordsSegment;
    IBOutlet UISlider *passAccuracySlider;
    IBOutlet UILabel *passAccuracyPercentLabel;
    IBOutlet UISlider *batchSizeSlider;
    IBOutlet UILabel *batchSizeLabel;
    IBOutlet UISegmentedControl *hideAllFuriganaSegment;
    IBOutlet UISegmentedControl *hideTranslationSegment;

    // don't update options until saved
    kcJlptLevel newShowWordsFor;
    kcPickWordsUsing newPickWordsUsing;
}

-(void)cancelButtonPressed:(id)sender;
-(void)saveButtonPressed:(id)sender;
-(IBAction)batchSizeChanged:(id)sender;
-(IBAction)passAccuracySliderValueChanged:(UISlider*)sender;

@end
