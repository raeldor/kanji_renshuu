//
//  KrLocalOptions.h
//  KanjiConnect
//
//  Created by Ray Price on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "LocalOptions.h"

typedef enum {
    kcJlptLevel_5 = 0,
    kcJlptLevel_4 = 1,
    kcJlptLevel_3 = 2,
    kcJlptLevel_2 = 3,
    kcJlptLevel_1 = 4,
    kcJlptLevel_0 = 5
} kcJlptLevel;

typedef enum {
    kcLearningType_Read = 0,
    kcLearningType_Write = 1
} kcLearningType;

typedef enum {
    kcPickWordsUsing_All = 0,
    kcPickWordsUsing_OnePlus = 1,
    kcPickWordsUsing_OnlyOne = 2
} kcPickWordsUsing;

@interface KrLocalOptions : LocalOptions {
}

+(KrLocalOptions*)getSingleton;
-(int)getReadingStudyStatusForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName;
-(void)setReadingStudyStatusForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName toValue:(NSUInteger)inValue;
-(int)getWritingStudyStatusForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName;
-(void)setWritingStudyStatusForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName toValue:(NSUInteger)inValue;

@property (nonatomic, assign) NSString *lastVersionNumber;
//@property (nonatomic, assign) BOOL cloudWarningShown;
//@property (nonatomic, assign) BOOL isUsingCloud;
@property (nonatomic, assign) BOOL welcomeMessageShown;
@property (nonatomic, assign) kcJlptLevel showWordsFor;
@property (nonatomic, assign) float passAccuracy;
@property (nonatomic, assign) kcPickWordsUsing pickWordsUsing;
@property (nonatomic, assign) NSUInteger maxBatchSize;
@property (nonatomic, assign) NSUInteger readingStudyFreeSwitchValue;
@property (nonatomic, assign) NSUInteger writingStudyFreeSwitchValue;
@property (nonatomic, assign) BOOL hideAllFurigana;
@property (nonatomic, assign) BOOL hideTranslation;

@end
