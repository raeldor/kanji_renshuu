//
//  KrAppOptions.h
//  KanjiConnect
//
//  Created by Ray Price on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppOptions.h"
#import "FlashAppOptions.h"
#import "BlasterGame.h"

@interface KrAppOptions : FlashAppOptions {
}

+(KrAppOptions*)getSingleton;

// translate between blaster card and flash card type
-(int)getLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType;
-(void)setToMaxLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType autoSave:(BOOL)inAutoSave;
-(void)setLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType toValue:(int)inValue autoSave:(BOOL)inAutoSave;
-(void)incrementLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType autoSave:(BOOL)inAutoSave;
-(void)decrementLeitnerDeckNumberForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType autoSave:(BOOL)inAutoSave;
-(NSUInteger)getDaysSinceLastShownForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType;
-(NSUInteger)getMinutesSinceLastShownForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType;
-(void)updateLastShownTimestampForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType autoSave:(BOOL)inAutoSave;
-(NSDate*)getDateLastShownForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex withBlasterGameType:(blGameTypeEnum)inBlasterGameType;


/*
@interface KrAppOptions : AppOptions {
}

+(KrAppOptions*)getSingleton;
-(int)getLeitnerDeckNumberForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting;
-(void)setLeitnerDeckNumberForKey:(NSString*)inKey toValue:(int)inValue isWritingGame:(BOOL)inIsWriting;
-(void)incrementLeitnerDeckNumberForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting;
-(void)decrementLeitnerDeckNumberForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting;
-(int)getDaysSinceLastShownForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting;
-(int)getMinutesSinceLastShownForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting;
-(void)updateDaysSinceLastShownForKey:(NSString*)inKey isWritingGame:(BOOL)inIsWriting;

@property (strong) NSMetadataQuery *query;
*/

@end
