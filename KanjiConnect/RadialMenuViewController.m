//
//  RadialMenuViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RadialMenuViewController.h"
#import "ResourceManager.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface RadialMenuViewController ()

@end

@implementation RadialMenuViewController

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // no selection by default
    currentSelection = -1;
    
    // Do any additional setup after loading the view from its nib.
    
    // set menu strings again
    if (menuStrings.count > 0)
        menuLabel1.text = [menuStrings objectAtIndex:0];
    if (menuStrings.count > 1)
        menuLabel2.text = [menuStrings objectAtIndex:1];
    if (menuStrings.count > 2)
        menuLabel3.text = [menuStrings objectAtIndex:2];
    if (menuStrings.count > 3)
        menuLabel4.text = [menuStrings objectAtIndex:3];
}

-(void)setMenuItemStrings:(NSArray*)inMenuStrings {
    // save menu strings too
    menuStrings = [inMenuStrings retain];
    
    // set label strings
    if (inMenuStrings.count > 0)
        menuLabel1.text = [menuStrings objectAtIndex:0];
    if (inMenuStrings.count > 1)
        menuLabel2.text = [menuStrings objectAtIndex:1];
    if (inMenuStrings.count > 2)
        menuLabel3.text = [menuStrings objectAtIndex:2];
    if (inMenuStrings.count > 3)
        menuLabel4.text = [menuStrings objectAtIndex:3];
}

-(void)processLongPress:(id)sender {
    // get recognizer
    UILongPressGestureRecognizer *recognizer = sender;
    
    // get position of touch
    CGPoint point = [recognizer locationOfTouch:0 inView:self.view];
    
    // state?
    switch (recognizer.state) {
        case UIGestureRecognizerStateChanged:
            // see if any of our views were touched
            currentSelection = -1;
            if ([self checkTouchOnView:menuOption1 withPoint:point])
                currentSelection = 0;
            if ([self checkTouchOnView:menuOption2 withPoint:point])
                currentSelection = 1;
            if ([self checkTouchOnView:menuOption3 withPoint:point])
                currentSelection = 2;
            if ([self checkTouchOnView:menuOption4 withPoint:point])
                currentSelection = 3;
            
            // play sound effect if it changes
            if (currentSelection != previousSelection) {
                // play move noise if on a menu item
                if (currentSelection != -1)
                    [self playSoundEffectNamed:@"NFF-select.wav" withVolume:0.25f];
                
                // save previous
                previousSelection = currentSelection;
            }
            
            break;
        case UIGestureRecognizerStateEnded:
            // call delegate
            if (currentSelection == -1)
                [delegate didCancelRadialSelection];
            else {
                // play select sound
                [self playSoundEffectNamed:@"blip1.wav" withVolume:0.25f];
                
                // call delegate
                [delegate didMakeRadialSelection:currentSelection];
            }
            break;
        default:
            break;
    }
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event {
    printf("radial touches began\r\n");
}

-(void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event {
    // get touch point
	UITouch *touch = [touches anyObject];
	CGPoint point = [touch locationInView:self.view];
    
    printf("point is %f,%f\r\n", point.x, point.y);
    printf("rect is %f,%f,%f,%f\r\n", menuOption1.frame.origin.x, menuOption1.frame.origin.y, menuOption1.frame.size.width, menuOption1.frame.size.height);
    
    // see if any of our views were touched
    currentSelection = -1;
    if ([self checkTouchOnView:menuOption1 withPoint:point])
        currentSelection = 0;
    if ([self checkTouchOnView:menuOption2 withPoint:point])
        currentSelection = 1;
    if ([self checkTouchOnView:menuOption3 withPoint:point])
        currentSelection = 2;
    if ([self checkTouchOnView:menuOption4 withPoint:point])
        currentSelection = 3;
    
    // play sound effect if it changes
    if (currentSelection != previousSelection) {
        // play move noise if on a menu item
        if (currentSelection != -1)
            [self playSoundEffectNamed:@"NFF-select.wav" withVolume:0.25f];
        
        // save previous
        previousSelection = currentSelection;
    }
    
    //
    // if we are over a sub-view
    printf("radial touches moved\r\n");
}

-(BOOL)checkTouchOnView:(UIView*)inView withPoint:(CGPoint)inPoint {
    if (CGRectContainsPoint(inView.frame, inPoint)) {
        if (inView == menuOption1)
            menuImage1.image = [UIImage imageNamed:@"radial_selected2.png"];
        if (inView == menuOption2)
            menuImage2.image = [UIImage imageNamed:@"radial_selected3.png"];
        if (inView == menuOption3)
            menuImage3.image = [UIImage imageNamed:@"radial_selected4.png"];
        if (inView == menuOption4)
            menuImage4.image = [UIImage imageNamed:@"radial_selected1.png"];
        return YES;
    }
    else {
        if (inView == menuOption1)
            menuImage1.image = [UIImage imageNamed:@"radial2.png"];
        if (inView == menuOption2)
            menuImage2.image = [UIImage imageNamed:@"radial3.png"];
        if (inView == menuOption3)
            menuImage3.image = [UIImage imageNamed:@"radial4.png"];
        if (inView == menuOption4)
            menuImage4.image = [UIImage imageNamed:@"radial1.png"];
        return NO;
    }
}

-(void)playSoundEffectNamed:(NSString*)inEffectName withVolume:(float)inVolume {
    // play sound effect
    AVAudioPlayer *newPlayer = [[AVAudioPlayer alloc] initWithData:[ResourceManager getBundleDataNamed:inEffectName] error:nil];
    newPlayer.delegate = self;
    newPlayer.volume = inVolume;
    [newPlayer play];
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    // release player
    [player release];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    printf("radial touches ended\r\n");
    // call delegate
    if (currentSelection == -1)
        [delegate didCancelRadialSelection];
    else {
        // play select sound
        [self playSoundEffectNamed:@"blip1.wav" withVolume:0.25f];
        
        // call delegate
        [delegate didMakeRadialSelection:currentSelection];
    }
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    printf("radial touches cancelled\r\n");
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(NSString*)getMenuItemStringForIndex:(int)inIndex {
    return [menuStrings objectAtIndex:inIndex];
}

-(void)dealloc {
    // release our vars
    [menuStrings release];
    
    // release super
    [super dealloc];
}

@end
