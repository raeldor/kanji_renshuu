//
//  ManageListsViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ManageListsViewController.h"
#import "KcKanjiListCell.h"
#import "EditKanjiListViewController.h"
#import "KrAppOptions.h"
#import "KrLocalOptions.h"

@interface ManageListsViewController ()

@end

@implementation ManageListsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // add edit button
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // populate list
    [self populateListList];
    
    // for testing, remove this later...
//    [listNameList addObject:@"Genki II Chapter 20"];
//    [listNameList addObject:@"Genki II Chapter 21"];
//    [listNameList addObject:@"Genki II Chapter 22"];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)populateListList {
    listNameList = [[KanjiList getListOfFilesWithExtension:@"kanjilist"] retain];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // always only one section
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	if (self.editing)
		return listNameList.count+1;
	else
	{
		if (listNameList.count == 0)
			return 1;
		else
			return listNameList.count;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *listCellIdentifier = @"KcKanjiListCell";
    static NSString *cellIdentifier = @"Cell";
    
    // get and configure cell
	// if no cells, display 'press to add' message
	if (listNameList.count == 0 && !self.tableView.editing) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		cell.textLabel.text = @"Press Edit Button To Create New Deck";
		cell.textLabel.textColor = [UIColor lightGrayColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES;
        return cell;
	}
    else {
        if ([indexPath row] < listNameList.count) {
            // setup list cell
            KcKanjiListCell *cell = [tableView dequeueReusableCellWithIdentifier:listCellIdentifier];
            cell.listNameLabel.text = (NSString*)[listNameList objectAtIndex:[indexPath row]];
            cell.readingSegment.selectedSegmentIndex = [[KrLocalOptions getSingleton] getKnownUnknownForListName:[listNameList objectAtIndex:[indexPath row]] isWritingGame:NO];
            cell.readingSegment.tag = [indexPath row]; // tag with row
            cell.writingSegment.selectedSegmentIndex = [[KrLocalOptions getSingleton] getKnownUnknownForListName:[listNameList objectAtIndex:[indexPath row]] isWritingGame:YES];
            cell.writingSegment.tag = [indexPath row]; // tag with row
            return cell;
        }
        else {
            // setup add new cell
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell.textLabel.text = @"Add New";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            return cell;
        }
    }
    
    return nil;
}

-(IBAction)readingSegmentChanged:(id)sender {
    // which list does this belong to?
    UISegmentedControl *thisSegment = (UISegmentedControl*)sender;
    NSString *listName = [listNameList objectAtIndex:thisSegment.tag];
    
    // set and save options
    [[KrLocalOptions getSingleton] setKnownUnknownTo:thisSegment.selectedSegmentIndex ForListName:listName isWritingGame:NO];
    [[KrLocalOptions getSingleton] save];
}

-(IBAction)writingSegmentChanged:(id)sender {
    // which list does this belong to?
    UISegmentedControl *thisSegment = (UISegmentedControl*)sender;
    NSString *listName = [listNameList objectAtIndex:thisSegment.tag];
    
    // set and save options
    [[KrLocalOptions getSingleton] setKnownUnknownTo:thisSegment.selectedSegmentIndex ForListName:listName isWritingGame:YES];
    [[KrLocalOptions getSingleton] save];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// which row are we getting
	NSUInteger row = [indexPath row];
	
	// if we are in edit mode
	if (self.tableView.editing) {
		// show delete style and insert for 'add new'
		if (row != listNameList.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	// add/remove placeholder items
	NSMutableArray *indexPaths = [NSMutableArray array];
	[indexPaths addObject:[NSIndexPath indexPathForRow:listNameList.count inSection:0]];
	[self.tableView beginUpdates];
	[super setEditing:editing animated:animated];
	[self.tableView setEditing:editing animated:YES];
	if (editing) {
		// delete 'press edit' message
		if (listNameList.count == 0)
			[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
		// Show the placeholder rows and hide back button
		[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		// Hide the placeholder rows and show back button again
		[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = NO;
		
		// show 'press edit' message
		if (listNameList.count == 0)
			[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.tableView endUpdates];
}


-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// find row
	NSUInteger row = [indexPath row];
	
	// editing mode?
	if (self.editing) {
		// add new?
		if (row >= [listNameList count]) {
			[self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
			// remember old name, because we may need to rename file
			oldListName = [[listNameList objectAtIndex:row] retain];
            
            // call edit using segue
            [self performSegueWithIdentifier:@"EditKcListSegue" sender:[listNameList objectAtIndex:row]];
		}
	}
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // call super first
    [super prepareForSegue:segue sender:sender];
    
    // only one segue
    EditKanjiListViewController *listController = [segue destinationViewController];
    
    // are we editing, or inserting new?
    if (sender == nil) {
        // create new kanji list and call editor
        KanjiList *newList = [[KanjiList alloc] init];
        [listController setEditableObject:newList isNew:YES];
        listController.delegate = self;
        [newList release];
    }
    else {
        // edit existing
        KanjiList *myList = [[KanjiList alloc] initFromArchive:sender];
        [listController setEditableObject:myList isNew:NO];
        listController.delegate = self;
        [myList release];
    }
    
}

-(void)didAddKanjiList:(KanjiList *)inKanjiList {
    // save kanji list
    [inKanjiList saveAs:inKanjiList.listName];
    
    // rebuild list of kanji lists
    [self populateListList];
    
    // reload table
    [self.tableView reloadData];
}

-(void)didUpdateKanjiList:(KanjiList *)inKanjiList {
    // do we need to rename?
    if (![inKanjiList.listName isEqualToString:oldListName])
        // rename file
        [inKanjiList renameFrom:oldListName to:inKanjiList.listName];

    // save changes
    [inKanjiList saveAs:inKanjiList.listName];
    
    // rebuild list of kanji lists
    [self populateListList];
    
    // reload table
    [self.tableView reloadData];
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // delete file
        [KanjiList deleteKanjiList:[listNameList objectAtIndex:[indexPath row]]];
        [listNameList removeObjectAtIndex:[indexPath row]];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // call edit using segue
        [self performSegueWithIdentifier:@"EditKcListSegue" sender:nil];
	}
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(void)dealloc {
    // release retains
    [listNameList release];
    
    // call super
    [super dealloc];
}

#pragma mark - Table view delegate


@end
