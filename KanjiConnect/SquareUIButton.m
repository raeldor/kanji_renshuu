//
//  SquareUIButton.m
//  KanjiConnect
//
//  Created by Ray Price on 11/11/12.
//
//

#import "SquareUIButton.h"
#import <QuartzCore/QuartzCore.h>


@implementation SquareUIButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        // support dark mode in ios13
        if (@available(iOS 13.0, *)) {
            [self.layer setBorderColor: [[UIColor labelColor] CGColor]];
        } else {
            // Fallback on earlier versions
            [self.layer setBorderColor: [[UIColor blackColor] CGColor]];
        }
        [self.layer setBorderWidth: 2.0];
        self.backgroundColor = [UIColor lightGrayColor];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        // set border
        // support dark mode in ios13
        if (@available(iOS 13.0, *)) {
            [self.layer setBorderColor: [[UIColor labelColor] CGColor]];
        } else {
            // Fallback on earlier versions
            [self.layer setBorderColor: [[UIColor blackColor] CGColor]];
        }
        [self.layer setBorderWidth: 1.0];
        self.backgroundColor = [UIColor lightGrayColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
