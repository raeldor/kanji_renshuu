//
//  OptionsTableViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OptionsTableViewController.h"
#import "KrAppOptions.h"

@interface OptionsTableViewController ()

@end

@implementation OptionsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // base height on autolayout content
    self.tableView.estimatedRowHeight = 128;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // set buttons on navigation controller to save and cancel
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonPressed:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    [cancelButton release];
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveButtonPressed:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    [saveButton release];
    
    // update controls with current options
    [showWordsForSegment setSelectedSegmentIndex:[KrLocalOptions getSingleton].showWordsFor];
    [pickWordsSegment setSelectedSegmentIndex:[KrLocalOptions getSingleton].pickWordsUsing];
    passAccuracySlider.value = [KrLocalOptions getSingleton].passAccuracy;
    batchSizeSlider.value = [KrLocalOptions getSingleton].maxBatchSize;
    if ([KrLocalOptions getSingleton].hideAllFurigana)
        [hideAllFuriganaSegment setSelectedSegmentIndex:0];
    else
        [hideAllFuriganaSegment setSelectedSegmentIndex:1];
    if ([KrLocalOptions getSingleton].hideTranslation)
        [hideTranslationSegment setSelectedSegmentIndex:0];
    else
        [hideTranslationSegment setSelectedSegmentIndex:1];

    // update slider labels
    [self batchSizeChanged:batchSizeSlider];
    [self passAccuracySliderValueChanged:passAccuracySlider];
    
    // update % label
//    passAccuracyPercentLabel.text = [NSString stringWithFormat:@"%1.0f%%", passAccuracySlider.value*100.0f];
    
    // register for slider events to show the %
//    [passAccuracySlider addTarget:self action:@selector(passAccuracySliderValueChanged:) forControlEvents:UIControlEventValueChanged];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(IBAction)batchSizeChanged:(id)sender {
    // set to closest value
    [batchSizeSlider setValue:((int)((batchSizeSlider.value + 2.5) / 5) * 5) animated:NO];
    
    // update label
    batchSizeLabel.text = [NSString stringWithFormat:@"%2.0f", batchSizeSlider.value];
}

-(IBAction)passAccuracySliderValueChanged:(UISlider *)sender {
    // update label
    passAccuracyPercentLabel.text = [NSString stringWithFormat:@"%1.0f%%", passAccuracySlider.value*100.0f];
}

-(void)cancelButtonPressed:(id)sender {
    // pop controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)saveButtonPressed:(id)sender {
    // save back to options
    [KrLocalOptions getSingleton].showWordsFor = (kcJlptLevel)showWordsForSegment.selectedSegmentIndex;
    [KrLocalOptions getSingleton].pickWordsUsing = (kcPickWordsUsing)pickWordsSegment.selectedSegmentIndex;
    [KrLocalOptions getSingleton].passAccuracy = passAccuracySlider.value;
    [KrLocalOptions getSingleton].maxBatchSize = batchSizeSlider.value;
    if (hideAllFuriganaSegment.selectedSegmentIndex == 0)
        [KrLocalOptions getSingleton].hideAllFurigana = YES;
    else
        [KrLocalOptions getSingleton].hideAllFurigana = NO;
    if (hideTranslationSegment.selectedSegmentIndex == 0)
        [KrLocalOptions getSingleton].hideTranslation = YES;
    else
        [KrLocalOptions getSingleton].hideTranslation = NO;
    [[KrLocalOptions getSingleton] save];
    
    // pop controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [super numberOfSectionsInTableView:tableView];
    
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [super tableView:tableView numberOfRowsInSection:section];
    
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
