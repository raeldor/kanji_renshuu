//
//  BlasterGame.m
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BlasterGame.h"
#import "JmwDatabase.h"
#import "KrAppOptions.h"
#import "KrLocalOptions.h"
#import "JapaneseDictionary.h"
#import "KanjiDictionary.h"
#import "NSMutableArrayShuffle.h"
#import "BlasterKanji.h"
#import "SampleSentences.h"
#import "BlasterCard.h"
#import "KanjiList.h"
#import "TextFunctions.h"
#import "FlashDeck.h"
#import "DeckSection.h"
#import "LeitnerLogic.h"

@implementation BlasterGame

@synthesize gameType;
@synthesize delegate;

-(id)init {
    self = [super init];
    if (self) {
        // create list of kanji that we are learning and known
        knownKanji = [[NSMutableArray alloc] init];
        learningKanji = [[NSMutableArray alloc] init];
        knownWords = [[NSMutableArray alloc] init];
        learningWords = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)startGameWithGameType:(blGameTypeEnum)inGameType completion:(void (^)(BOOL))block {
    // save game type
    gameType = inGameType;
    
    // wait for all open decks to finish
    dispatch_group_t group = dispatch_group_create();
    
    // loop through decks and get lists of studied and studying marked kanji
    // do the same for words now
    cloudQuery = [[FlashDeck getListOfArchivesWithExtension:@"flashDeck" completion:^(NSArray *archiveList) {
        // loop through all decks and section and find studied and studying kanji
        for (int i=0; i < archiveList.count; i++) {
            // loop through sections
            NSString *deckName = [archiveList objectAtIndex:i];
            FlashDeck * thisDeck = [[FlashDeck alloc] initWithName:deckName];
            dispatch_group_enter(group);
            [thisDeck openWithCompletionHandler:^(BOOL success) {
                for (int s=0; s < thisDeck.deckSections.count; s++) {
                    // if this section is a 'kanji' section
                    DeckSection *thisSection = [thisDeck.deckSections objectAtIndex:s];
                    int studyStatus;
                    if (gameType&blGameType_Writing)
                        studyStatus = [[KrLocalOptions getSingleton] getWritingStudyStatusForSectionNamed:thisSection.sectionName inDeckNamed:thisDeck.deckName];
                    else
                        studyStatus = [[KrLocalOptions getSingleton] getReadingStudyStatusForSectionNamed:thisSection.sectionName inDeckNamed:thisDeck.deckName];
                    // known
                    if (studyStatus == 0) {
                        // add to known kanji list
                        for (int c=0; c < thisSection.cards.count; c++) {
                            FlashCard *thisCard = [thisSection.cards objectAtIndex:c];
                            if ([thisSection.sectionType isEqualToString:@"Kanji"]) {
                                [knownKanji addObject:thisCard.kanji];
                            }
                            else {
                                // add to known words list
                                [knownWords addObject:thisCard];
                            }
                        }
                    }
                    else {
                        // if learning
                        if (studyStatus == 1) {
                            // add to learning kanji list
                            for (int c=0; c < thisSection.cards.count; c++) {
                                FlashCard *thisCard = [thisSection.cards objectAtIndex:c];
                                if ([thisSection.sectionType isEqualToString:@"Kanji"]) {
                                    [learningKanji addObject:thisCard.kanji];
                                }
                                else {
                                    // add to learning words list
                                    [learningWords addObject:thisCard];
                                }
                            }
                        }
                    }
                }
                dispatch_group_leave(group);
                
                // close the deck again, don't keep it open
                [thisDeck closeWithCompletionHandler:^(BOOL success) {
                    // do nothing
                }];
            }];
            [thisDeck release];
        }
        
        // wait for completion
        while (dispatch_group_wait(group, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.2f]];
        }
        dispatch_release(group);
        
        // make lists unique
        [self removeDuplicatesFromStringArray:knownKanji];
        [self removeDuplicatesFromStringArray:learningKanji];
        
        // start game
        [self startNewGame];
        
        // call completion block
        block(YES);
        
        // release query
        [cloudQuery release];
    }] retain];
}

-(void)removeDuplicatesFromStringArray:(NSMutableArray*)inArray {
    // create new array with strings
    NSCountedSet *countedSet = [NSCountedSet setWithArray:inArray];
    NSMutableArray *finalArray = [NSMutableArray arrayWithCapacity:[inArray count]];
    for(id obj in countedSet) {
        [finalArray addObject:obj];
    }
    
    // remove to our array
    [inArray removeAllObjects];
    [inArray addObjectsFromArray:finalArray];
}

-(NSString*)getKanjiIdStringPlayList:(NSArray*)inList {
    // put into a comma delimited list of kanji
    NSMutableString *kanjiList = [NSMutableString stringWithFormat:@""];
    for (int i=0; i < inList.count; i++) {
        NSString *thisKanji = [inList objectAtIndex:i];
        [kanjiList appendFormat:@"'%@',", thisKanji];
    }
    [kanjiList deleteCharactersInRange:NSMakeRange(kanjiList.length-1, 1)];
    
    NSLog(@"Kanji list to practice is %@\r\n", kanjiList);
    
    // make a comma delimited list of kanji ids because this is quicker to query database
    NSString *kanjiQuery = [NSString stringWithFormat:@"select kanji_id from kanji where kanji_text in (%@)", kanjiList];
    NSMutableArray *kanjiIdList = [JmwDatabase getIdListFromQuery:kanjiQuery];
    NSMutableString *kanjiIdString = [NSMutableString stringWithFormat:@""];
    for (int i=0; i < kanjiIdList.count; i++) {
        int thisId = [[kanjiIdList objectAtIndex:i] intValue];
        [kanjiIdString appendFormat:@"%d,", thisId];
    }
    [kanjiIdString deleteCharactersInRange:NSMakeRange(kanjiIdString.length-1, 1)];
    
    return kanjiIdString;
}

-(void)startNewGame {
    // start game at first level
    playIndex = 0;
    
    // build a list of kanji marked as known or studying (combined)
    NSMutableArray *kanjiCombinedList = [[NSMutableArray alloc] initWithCapacity:10];
    [kanjiCombinedList addObjectsFromArray:knownKanji];
    [kanjiCombinedList addObjectsFromArray:learningKanji];
    
    // build list of kanji we are playing
    kanjiPlayList = [[NSMutableArray alloc] initWithCapacity:10];
    if (gameType&blGameType_Freeplay)
        [kanjiPlayList addObjectsFromArray:knownKanji];
    [kanjiPlayList addObjectsFromArray:learningKanji];
    
    // make unique
    [self removeDuplicatesFromStringArray:kanjiCombinedList];
    [self removeDuplicatesFromStringArray:kanjiPlayList];
    
    // build a list of words marked as known or studying (combined)
    NSMutableArray *wordCombinedList = [[NSMutableArray alloc] initWithCapacity:10];
    [wordCombinedList addObjectsFromArray:knownWords];
    [wordCombinedList addObjectsFromArray:learningWords];
    
    // build list of words we are playing
    NSMutableArray *wordPlayList = [[NSMutableArray alloc] initWithCapacity:10];
    if (gameType&blGameType_Freeplay)
        [wordPlayList addObjectsFromArray:knownWords];
    [wordPlayList addObjectsFromArray:learningWords];
    
    // make unique
    [self removeDuplicatesFromStringArray:wordCombinedList];
    [self removeDuplicatesFromStringArray:wordPlayList];
    
    // get as list of ids because it's quicker to query db
//    NSString *playListIdString = [self getKanjiIdStringPlayList:kanjiPlayList];
//    NSString *combinedListIdString = [self getKanjiIdStringPlayList:kanjiCombinedList];
    
    // get id list of words to study using leitner
    playCards = [[self getPlayCardsFromKanjiPlayList:kanjiPlayList andKanjiCombinedList:kanjiCombinedList andWordPlayList:wordPlayList andWordCombinedList:wordCombinedList] retain];
    
//    playCards = [[self getPlaylistFromPlayIdList:playListIdString andCombinedIdList:combinedListIdString] retain];
    playIndex = 0;
    
    NSLog(@"Got playing cards, releasing combined lists");
    
    // don't need combined list anymore
    [kanjiCombinedList release];
    [wordCombinedList release];
    [wordPlayList release];
    
    NSLog(@"Get sentence for current play card");
    
    // get current sentence for current play card
    JapaneseDictionary *dictionary = [JapaneseDictionary getSingleton];
    NSMutableArray *sentences = [NSMutableArray arrayWithCapacity:10];
    while (sentences.count <= 0 && playIndex < playCards.count) {
        // get next word
        BlasterCard *thisCard = [playCards objectAtIndex:playIndex];
        NSLog(@"First word to play %@, %@\r\n", thisCard.kanji, thisCard.kana);
        JapaneseWord *myWord = [dictionary getWordFromDictionaryId:thisCard.dictionaryId];
        sentences = [self getSentencesWithWord:myWord andSense:thisCard.senseIndex giveMe:1];
        if (sentences.count == 0) {
            // go to next word
            playIndex++;
        }
    }
    
    NSLog(@"Sentence count is %lu", (unsigned long)sentences.count);
    
    if (sentences.count > 0)
        currentSentence = [[BlasterSentence alloc] initWithSentenceEntry:[sentences objectAtIndex:0] forGame:self];
    else {
        // no sentences found for ANY of the words
        // start a new game to try again
        // could get stuck in a loop here!
        [self startNewGame];
    }

    return;
}

-(NSUInteger)getWordCount {
    return allCards.count;
}

-(NSUInteger)getKanjiCount {
    return kanjiPlayList.count;
}

-(void)getLeitnerStatsIntoIntArray:(float[])inIntArray forCards:(NSMutableArray*)inCards {
    // reset array
    for (int i=0; i < [LeitnerLogic getDeckCount]; i++)
        inIntArray[i] = 0;
    int totalScore = 0; // 0-5 points based on deck position
    
    // loop through all cards
    for (int i=0; i < inCards.count; i++) {
        // get leitner deck for this card
        BlasterCard *thisCard = [inCards objectAtIndex:i];
        int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:thisCard.kanji andKana:thisCard.kana andSenseIndex:thisCard.senseIndex withBlasterGameType:gameType];
        /*
        NSString *key = [NSString stringWithFormat:@"%@\r\n%@\r\n%d", thisCard.kanji, thisCard.kana, thisCard.senseIndex];
        int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKey:key isWritingGame:gameType&blGameType_Writing];*/
        
        // max leitner deck out at 6, even though we can go to 7
        if (leitnerDeck > [LeitnerLogic getMaxDeckIndex])
            leitnerDeck = [LeitnerLogic getMaxDeckIndex];
        if (leitnerDeck < 0)
            leitnerDeck = 0;
        
        // increment respective bucket
        inIntArray[leitnerDeck]++;
        
        // increment score
        totalScore += leitnerDeck;
    }
    
    // put percent complete in position 6
    inIntArray[[LeitnerLogic getMaxDeckIndex]+1] = ((float)totalScore / ((float)inCards.count*(float)[LeitnerLogic getMaxDeckIndex]))*100.0f;
}

-(void)getPlayStatsIntoIntArray:(float[])inIntArray {
    return [self getLeitnerStatsIntoIntArray:inIntArray forCards:allCards];
}

//-(NSMutableArray*)getAllCardsFromPlayIdList:(NSString*)inIdList andCombinedIdList:(NSString*)inCombinedIdList {
-(NSMutableArray*)getAllCardsFromKanjiPlayList:(NSArray*)inKanjiPlayList andKanjiCombinedList:(NSArray*)inKanjiCombinedList andWordPlayList:(NSArray*)inWordPlayList andWordCombinedList:(NSArray*)inWordCombinedList {
    // we have to find a way here to pull a list of words QUICKLY for both play styles
    // and NOT pull the words that have no sample sentences
    
    NSLog(@"inKanjiPlayList count is %lu, inKanjiCombinedList count is %lu, inWordPlayListCount is %lu, inWordCombinedListCount is %lu", (unsigned long)inKanjiPlayList.count, (unsigned long)inKanjiCombinedList.count, (unsigned long)inWordPlayList.count, (unsigned long)inWordCombinedList.count);
    
    // create temporary tables to hold selections (for indexing and speed)
    sqlite3 *dictionaryDb = [JmwDatabase getSingleton];
    if (sqlite3_exec(dictionaryDb, [@"create temp table kanji_list (kanji_id int, order_number int);create index ix_kanji_id_temp on kanji_list (kanji_id);create index ix_order_temp on kanji_list (order_number);create temp table combined_list (kanji_id int, order_number int);create index ix_kanji_id_temp_combined on combined_list (kanji_id);create index ix_order_temp_combined on combined_list (order_number);create temp table word_list (dictionary_id int, sense_index int, kanji_word varchar(20), kana_word varchar(20), order_number);create index ix_dictionary_id_temp on word_list (dictionary_id, sense_index);create index ix_word_order_temp on word_list (order_number);create temp table combined_word_list (dictionary_id int, sense_index int, order_number int);create index ix_dictionary_id_temp_combined on combined_word_list (dictionary_id, sense_index);create index ix_word_order_temp_combined on combined_word_list (order_number);" UTF8String], nil, nil, nil) != SQLITE_OK) {
        char *errMessage;
        errMessage = (char*)sqlite3_errmsg(dictionaryDb);
        NSAssert(0, @"Sqlite Error %@", [NSString stringWithUTF8String:errMessage]);
		return nil;
    }
    
    // copy play kanji to temporary table
    for (int i=0; i < inKanjiPlayList.count; i++) {
        // get id as integer
        NSString *thisKanji = [inKanjiPlayList objectAtIndex:i];
        if (sqlite3_exec(dictionaryDb, [[NSString stringWithFormat:@"insert into kanji_list (kanji_id, order_number) select kanji.kanji_id, order_number from kanji inner join custom_category on custom_category.custom_category_id=kanji.custom_category_id where kanji_text='%@'", thisKanji] UTF8String], nil, nil, nil) != SQLITE_OK) {
            char *errMessage;
            errMessage = (char*)sqlite3_errmsg(dictionaryDb);
            NSAssert(0, @"Sqlite Error %@", [NSString stringWithUTF8String:errMessage]);
            return nil;
        }
    }
    
    // copy combined kanji to temporary table
    for (int i=0; i < inKanjiCombinedList.count; i++) {
        // get id as integer
        NSString *thisKanji = [inKanjiCombinedList objectAtIndex:i];
        if (sqlite3_exec(dictionaryDb, [[NSString stringWithFormat:@"insert into combined_list (kanji_id, order_number ) select kanji.kanji_id, order_number from kanji inner join custom_category on custom_category.custom_category_id=kanji.custom_category_id where kanji_text='%@'", thisKanji] UTF8String], nil, nil, nil) != SQLITE_OK) {
            char *errMessage;
            errMessage = (char*)sqlite3_errmsg(dictionaryDb);
            NSAssert(0, @"Sqlite Error %@", [NSString stringWithUTF8String:errMessage]);
            return nil;
        }
    }
     
    // copy play words to temporary table
    for (int i=0; i < inWordPlayList.count; i++) {
        // get id as integer
        FlashCard *thisCard = [inWordPlayList objectAtIndex:i];
        NSString *firstMeaning = [thisCard.meanings objectAtIndex:0];
        if (sqlite3_exec(dictionaryDb, [[NSString stringWithFormat:@"insert into word_list (dictionary_id, sense_index, kanji_word, kana_word, order_number) select dm.dictionary_id, dm.sense_index, d.kanji_word, d.kana_word, order_number from dictionary_meaning dm inner join dictionary_meaning_fts dmfts on dmfts.rowid=dm.rowid inner join dictionary d on d.dictionary_id=dm.dictionary_id inner join custom_category on custom_category.custom_category_id=d.custom_category_id where d.kanji_word='%@' and d.kana_word='%@' and dmfts.meaning='%@'", thisCard.kanji, thisCard.kana, [firstMeaning stringByReplacingOccurrencesOfString:@"\'" withString:@"''"]] UTF8String], nil, nil, nil) != SQLITE_OK) {
            char *errMessage;
            errMessage = (char*)sqlite3_errmsg(dictionaryDb);
            NSAssert(0, @"Sqlite Error %@", [NSString stringWithUTF8String:errMessage]);
            return nil;
        }
    }
    
    /*
    // copy combined words to temporary table
    NSArray *ids = [inIdList componentsSeparatedByString:@","];
    for (int i=0; i < ids.count; i++) {
        // get id as integer
        int thisId = [[ids objectAtIndex:i] intValue];
        if (sqlite3_exec(dictionaryDb, [[NSString stringWithFormat:@"insert into kanji_list (kanji_id) values (%d)", thisId] UTF8String], nil, nil, nil) != SQLITE_OK) {
            char *errMessage;
            errMessage = (char*)sqlite3_errmsg(dictionaryDb);
            NSAssert(0, @"Sqlite Error %@", [NSString stringWithUTF8String:errMessage]);
            return nil;
        }
    }*/
    
    // get jlpt id list based on users selected JLPT level
    NSString *jlptStringList = [[JapaneseDictionary getSingleton] getJlptStringListFromLevelNumber:(jlptLevelEnum)[KrLocalOptions getSingleton].showWordsFor];
    NSString *jlptIdList = [[JapaneseDictionary getSingleton] getCategoryIdListFromStringList:jlptStringList];
    
    // must use SENSE_INDEX!
    // find all words that use this kanji and are in our study list
    // don't include words written in kana alone!
    NSString *query;
    if ([KrLocalOptions getSingleton].pickWordsUsing == 0) {
        // words that must contain our kanji and also any known kanji
        query = [NSString stringWithFormat:@"select distinct dictionary_id, kanji_word, kana_word, sense_index from (select distinct dict.dictionary_id, dict.kanji_word, dict.kana_word, sense_index, order_number from dictionary_sense ds inner join dictionary dict on dict.dictionary_id=ds.dictionary_id inner join kanji_list kl inner join dictionary_kanji dk on dk.dictionary_id=ds.dictionary_id and dk.kanji_id=kl.kanji_id where dict.custom_category_id in %@ and ds.custom_category_id in %@ and (select sw.dictionary_id from sentence_words sw where sw.dictionary_id=dict.dictionary_id and (sw.sense_index=ds.sense_index or sw.sense_index=0) and uses_kanji=1 and missing_stroke_data=0 limit 1) not null and (select count(*) from dictionary_kanji where dictionary_id=dict.dictionary_id)=(select count(*) from dictionary_kanji inner join combined_list cl on cl.kanji_id=dictionary_kanji.kanji_id where dictionary_id=dict.dictionary_id) union select dictionary_id, kanji_word, kana_word, sense_index, order_number from word_list order by order_number) test", jlptIdList, jlptIdList];
    }
    else {
        if ([KrLocalOptions getSingleton].pickWordsUsing == 1) {
            // words that contain any of our kanji
            query = [NSString stringWithFormat:@"select distinct dictionary_id, kanji_word, kana_word, sense_index from (select distinct dict.dictionary_id, kanji_word, kana_word, sense_index, order_number from dictionary_sense ds inner join dictionary dict on dict.dictionary_id=ds.dictionary_id inner join kanji_list kl inner join dictionary_kanji dk on dk.dictionary_id=ds.dictionary_id and dk.kanji_id=kl.kanji_id where dict.custom_category_id in %@ and ds.custom_category_id in %@ and (select sw.dictionary_id from sentence_words sw where sw.dictionary_id=dict.dictionary_id and (sw.sense_index=ds.sense_index or sw.sense_index=0) and uses_kanji=1 and missing_stroke_data=0 limit 1) not null union select dictionary_id, kanji_word, kana_word, sense_index, order_number from word_list order by order_number) test", jlptIdList, jlptIdList];
        }
        else {
            // words that contain any of our kanji, but contain only ONE kanji
            query = [NSString stringWithFormat:@"select distinct dictionary_id, kanji_word, kana_word, sense_index from (select distinct dict.dictionary_id, kanji_word, kana_word, sense_index, order_number from dictionary_sense ds inner join dictionary dict on dict.dictionary_id=ds.dictionary_id inner join kanji_list kl inner join dictionary_kanji dk on dk.dictionary_id=ds.dictionary_id and dk.kanji_id=kl.kanji_id where dict.custom_category_id in %@ and ds.custom_category_id in %@ and (select sw.dictionary_id from sentence_words sw where sw.dictionary_id=dict.dictionary_id and (sw.sense_index=ds.sense_index or sw.sense_index=0) and uses_kanji=1 and missing_stroke_data=0 limit 1) not null  and (select count(*) from dictionary_kanji where dictionary_id=dict.dictionary_id)=1 union select dictionary_id, kanji_word, kana_word, sense_index, order_number from word_list where (select count(*) from dictionary_kanji where dictionary_id=word_list.dictionary_id)=1 order by order_number) test", jlptIdList, jlptIdList];
        }
    }
    
    NSLog(@"Prepare sql statement for get all cards");
    
    // save db for readbility
	sqlite3_stmt *statement;
	if (sqlite3_prepare_v2(dictionaryDb, [query UTF8String], -1, &statement, nil) != SQLITE_OK) {
        char *errMessage;
        errMessage = (char*)sqlite3_errmsg(dictionaryDb);
		sqlite3_finalize(statement);
        NSAssert(0, @"Sqlite Error %@", [NSString stringWithUTF8String:errMessage]);
		return nil;
	}

    NSLog(@"Loop though cards");

    // dictionary id may change, so use kanji and kana
    // first, build a list of all the words
    NSMutableArray *myCards = [NSMutableArray arrayWithCapacity:10];
	while (sqlite3_step(statement) == SQLITE_ROW) {
        // create card for this entry
		int thisId = sqlite3_column_int(statement, 0);
        NSString *thisKanji = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
        NSString *thisKana = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
        int thisSense = sqlite3_column_int(statement, 3);
        BlasterCard *newCard = [[BlasterCard alloc] initWithDictionaryId:thisId kanji:thisKanji andKana:thisKana andSense:thisSense];
        [myCards addObject:newCard];
        [newCard release];
    }
	sqlite3_finalize(statement);
    
    // add manually those specifically selected words
    
    // delete temporary tables
    if (sqlite3_exec(dictionaryDb, [@"drop table kanji_list;drop table combined_list;drop table word_list;drop table combined_word_list;" UTF8String], nil, nil, nil) != SQLITE_OK) {
        char *errMessage;
        errMessage = (char*)sqlite3_errmsg(dictionaryDb);
        NSAssert(0, @"Sqlite Error %@", [NSString stringWithUTF8String:errMessage]);
		return nil;
    }
    
    NSLog(@"Finished getting all cards (%lu)", (unsigned long)myCards.count);
    
    return myCards;
}

/*
-(BOOL)isShowCard:(BlasterCard*)inCard forPass:(int)pass percentageOfBatch:(float)percentageOfBatch {
//    NSString *key = [NSString stringWithFormat:@"%@\r\n%@\r\n%d", inCard.kanji, inCard.kana, inCard.senseIndex];
    int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:inCard.kanji andKana:inCard.kana andSenseIndex:inCard.senseIndex withBlasterGameType:gameType];
//    int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKey:key isWritingGame:gameType&blGameType_Writing];
    BOOL showCard = NO;
    NSUInteger daysSinceShown = [[KrAppOptions getSingleton] getDaysSinceLastShownForKanji:inCard.kanji andKana:inCard.kana andSenseIndex:inCard.senseIndex withBlasterGameType:gameType];
//    int daysSinceShown = [[KrAppOptions getSingleton] getDaysSinceLastShownForKey:key isWritingGame:gameType&blGameType_Writing];
    
    // which leitner deck is it in?
    switch (leitnerDeck) {
        case 0:
            break;
        case 1:
            showCard = YES; // always show
            NSLog(@"show %@ card from leitner 1, pass %d, days %lu, pob %f", inCard.kanji, pass, (unsigned long)daysSinceShown, percentageOfBatch);
            break;
        case 2:
            if (daysSinceShown > 1 || pass == 1) {
                NSLog(@"show %@ card from leitner 2, pass %d, days %lu, pob %f", inCard.kanji, pass, (unsigned long)daysSinceShown, percentageOfBatch);
                showCard = YES;
            }
            break;
        case 3:
            if (daysSinceShown > 3 || pass == 2) {
                NSLog(@"show %@ card from leitner 3, pass %d, days %lu, pob %f", inCard.kanji, pass, (unsigned long)daysSinceShown, percentageOfBatch);
                showCard = YES;
            }
            break;
        case 4:
            if (daysSinceShown > 7 || pass == 3) {
                NSLog(@"show %@ card from leitner 4, pass %d, days %lu, pob %f", inCard.kanji, pass, (unsigned long)daysSinceShown, percentageOfBatch);
                showCard = YES;
            }
            break;
        case 5:
            if (daysSinceShown > 30 || pass == 4) {
                NSLog(@"show %@ card from leitner 5, pass %d, days %lu, pob %f", inCard.kanji, pass, (unsigned long)daysSinceShown, percentageOfBatch);
                showCard = YES;
            }
            break;
        case 6:
            if (daysSinceShown > 30*4 || pass == 5) {
                NSLog(@"show %@ card from leitner 6, pass %d, days %lu, pob %f", inCard.kanji, pass, (unsigned long)daysSinceShown, percentageOfBatch);
                showCard = YES;
            }
            break;
        case 7:
            if (daysSinceShown > 365 || pass == 6) {
                NSLog(@"show %@ card from leitner 7, pass %d, days %lu, pob %f", inCard.kanji, pass, (unsigned long)daysSinceShown, percentageOfBatch);
                showCard = YES;
            }
            break;
        default:
            break;
    }
    return showCard;
}
*/

-(NSMutableArray*)getPlayCardsFromKanjiPlayList:(NSArray*)inKanjiPlayList andKanjiCombinedList:(NSArray*)inKanjiCombinedList andWordPlayList:(NSArray*)inWordPlayList andWordCombinedList:(NSArray*)inWordCombinedList {
    
    // does it contain 'se'?!
    bool found = false;
    for (int i=0; i < inWordPlayList.count; i++) {
        FlashCard *thisCard = [inWordPlayList objectAtIndex:i];
        if ([thisCard.kanji isEqualToString:@"背"])
        {
            found = true;
        }
    }
    
    // get all cards
    if (allCards == nil) {
        allCards = [[self getAllCardsFromKanjiPlayList:inKanjiPlayList andKanjiCombinedList:inKanjiCombinedList andWordPlayList:inWordPlayList andWordCombinedList:inWordCombinedList] retain];
        NSLog(@"Found %lu matching words\r\n", (unsigned long)allCards.count);
    }
    
    // if we are at 100% recognition, we have to shuffle cards, otherwise we'll keep
    // getting the same 10 over and over
    float scores[7];
    [self getLeitnerStatsIntoIntArray:scores forCards:allCards];
    if (scores[6] == allCards.count)
        [allCards shuffle];

    // build list of shuffled cards
    int pass = 0;
    NSMutableArray *shuffledCards = [[NSMutableArray alloc] initWithCapacity:10];
//    NSMutableArray *shuffledCards = [NSMutableArray arrayWithCapacity:10];
    while (shuffledCards.count < [KrLocalOptions getSingleton].maxBatchSize && pass < 5) {
        // loop through cards
        for (int i=0; i < [allCards count] && shuffledCards.count < [KrLocalOptions getSingleton].maxBatchSize; i++) {
            // get card
            BlasterCard *thisCard = [allCards objectAtIndex:i];
            
            // make sure we don' try and add the same card again!
            if (![shuffledCards containsObject:thisCard]) {
                // get leitner deck and minutes/days since shown
                int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:thisCard.kanji andKana:thisCard.kana andSenseIndex:thisCard.senseIndex withBlasterGameType:gameType];
                NSUInteger daysSinceShown = [[KrAppOptions getSingleton] getDaysSinceLastShownForKanji:thisCard.kanji andKana:thisCard.kana andSenseIndex:thisCard.senseIndex withBlasterGameType:gameType];
                NSUInteger minutesSinceShown = [[KrAppOptions getSingleton] getMinutesSinceLastShownForKanji:thisCard.kanji andKana:thisCard.kana andSenseIndex:thisCard.senseIndex withBlasterGameType:gameType];
                
                // show card?
                BOOL showCard = [self showCardInLeitnerDeck:leitnerDeck withDaysSinceShown:daysSinceShown andMinutesSinceShown:minutesSinceShown forPass:pass];
                
                JapaneseWord *thisWord = [[JapaneseDictionary getSingleton] getWordFromDictionaryId:thisCard.dictionaryId];
                NSLog(@"Show card %@ result is %d for deck %d, pass %d, minutes since shown %d, days since Shown %d", thisWord.kanji, showCard, leitnerDeck, pass, (int)minutesSinceShown, (int)daysSinceShown);

                // add to shuffled cards
                if (showCard) {
                    // we can't add to cards if there is no sample sentence, because it will give
                    // false positive about the number of cards we have available to play
                    JapaneseWord *myWord = [[JapaneseDictionary getSingleton] getWordFromDictionaryId:thisCard.dictionaryId];
                    NSMutableArray *sentences = [self getSentencesWithWord:myWord andSense:thisCard.senseIndex giveMe:1];
                    if (sentences.count != 0) {
                        // add to shuffled cards
                        BlasterCard *card = [allCards objectAtIndex:i];
                        [shuffledCards addObject:card];
                    }
                }
            }
            else {
                // was this an error before!?
                int i=0;
            }
        }
        
        // increment pass count in case we haven't got any cards to practice
        pass++;
    }
    
    NSLog(@"Found %lu words for practice using leitner, taking %d passes\r\n", (unsigned long)shuffledCards.count, pass);
    
    // shuffle the cards so we don't keep getting the same ones first
    [shuffledCards shuffle];
    
    NSLog(@"Shuffle cards finished\r\n");
    
    return [shuffledCards autorelease];
}


-(BOOL)showCardInLeitnerDeck:(int)leitnerDeck withDaysSinceShown:(NSUInteger)daysSinceShown andMinutesSinceShown:(NSUInteger)minutesSinceShown forPass:(NSUInteger)pass {
    // centralized logic to leitnerlogic class
    return [LeitnerLogic showCardInLeitnerDeck:leitnerDeck withDaysSinceShown:daysSinceShown andMinutesSinceShown:minutesSinceShown forPass:pass];
}

-(void)didCompleteSentence:(BlasterSentence *)inSentence {
    // tell game delegate
    [delegate didCompleteSentence:inSentence];
}

-(BOOL)isWordKnownForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex {
    // if it's in learning kanji or known kanji, it's known
    // fix 3/22... only check known if we are in game mode free play
    if (gameType&blGameType_Freeplay) {
        for (int i=0; i < knownWords.count; i++) {
            FlashCard *thisKnown = [knownWords objectAtIndex:i];
            if ([thisKnown.kanji isEqualToString:inKanji] && [thisKnown.kana isEqualToString:inKana] && (inSenseIndex == thisKnown.firstSenseIndex || inSenseIndex == 0))
                return YES;
        }
    }
    for (int i=0; i < learningWords.count; i++) {
        FlashCard *thisKnown = [learningWords objectAtIndex:i];
        if ([thisKnown.kanji isEqualToString:inKanji] && [thisKnown.kana isEqualToString:inKana] && (inSenseIndex == thisKnown.firstSenseIndex || inSenseIndex == 0))
            return YES;
    }
    
    // not known
    return NO;
}

-(BOOL)isKanjiKnown:(NSString*)inKanji {
    // if it's in learning kanji or known kanji, it's known
    for (int i=0; i < knownKanji.count; i++) {
        NSString *thisKnown = [knownKanji objectAtIndex:i];
        if ([thisKnown isEqualToString:inKanji])
            return YES;
    }
    for (int i=0; i < learningKanji.count; i++) {
        NSString *thisKnown = [learningKanji objectAtIndex:i];
        if ([thisKnown isEqualToString:inKanji])
            return YES;
    }
    
    // not known
    return NO;
}

-(BlasterSentence*)getCurrentSentence {
    return currentSentence;
}

-(BlasterCard*)getCurrentCard {
    return [playCards objectAtIndex:playIndex];
}

-(void)checkCardAnswers {
    // if this is a reading game
    BOOL changed = NO;
    if (gameType&blGameType_Reading) {
        // mark existing card words as seen
        for (int i=0; i < currentSentence.words.count; i++) {
            // if this is a word the user was to complete
            BlasterWord *thisWord = [currentSentence.words objectAtIndex:i];
            if (thisWord.toBeCompleted) {
                // options need saving
                changed = YES;
                
                // increment or decrement count depending on if furigana is right or wrong
                // or if user had to look
                if ([thisWord.furigana isEqualToString:thisWord.userFurigana] &&
                    !thisWord.hadToLook)
                    [thisWord incrementLeitnerDeckNumber];
                else
                    [thisWord decrementLeitnerDeckNumber];
            }
        }
    }
    else {
        // mark word and kanji as seen
        for (int i=0; i < currentSentence.words.count; i++) {
            // if this is a word the user was to complete
            BlasterWord *thisWord = [currentSentence.words objectAtIndex:i];
            if (thisWord.toBeCompleted) {
                // options need saving
                changed = YES;
                
                // loop through kanjis
                BOOL allCorrect = YES;
                for (int k=0; k < thisWord.kanjis.count; k++) {
                    BlasterKanji *thisKanji = [thisWord.kanjis objectAtIndex:k];
                    if (thisKanji.toBeCompleted) {
                        // if we got drawing right and didn't have to look
                        if (thisKanji.finalScore >= [KrLocalOptions getSingleton].passAccuracy &&
                            !thisKanji.hadToLook) {
                        }
                        else {
                            // not all correct
                            allCorrect = NO;
                        }
                    }
                }
                
                // increment or decrement count depending on if any drawings were wrong
                if (allCorrect)
                    [thisWord incrementLeitnerDeckNumber];
                else
                    [thisWord decrementLeitnerDeckNumber];
            }
        }
    }
    
    // save options if changed
    if (changed)
        [[KrAppOptions getSingleton] save];
}

-(void)goToNextCard {
    // if we have more cards
    if (playIndex+1 < playCards.count) {
        // increment index
        playIndex++;
        
        // get current sentence for current play card
        JapaneseDictionary *dictionary = [JapaneseDictionary getSingleton];
        NSMutableArray *sentences = [NSMutableArray arrayWithCapacity:10];
        while (sentences.count <= 0 && playIndex < playCards.count) {
            // get next word
            BlasterCard *thisCard = [playCards objectAtIndex:playIndex];
            JapaneseWord *myWord = [dictionary getWordFromDictionaryId:thisCard.dictionaryId];
            sentences = [self getSentencesWithWord:myWord andSense:thisCard.senseIndex giveMe:1];
            if (sentences.count == 0) {
                // go to next word
                playIndex++;
            }
        }
        if (sentences.count > 0)
            currentSentence = [[BlasterSentence alloc] initWithSentenceEntry:[sentences objectAtIndex:0] forGame:self];
        else {
            // no sentences found for any of the words left
            [self startNewGame];
        }
    }
    else {
        // restart game
        [self startNewGame];
    }
}

-(NSMutableArray*)getSentencesWithWord:(JapaneseWord*)inEntry andSense:(int)inSenseIndex giveMe:(int)inNumberRequired {
    NSMutableArray *returnArray = [NSMutableArray arrayWithCapacity:inNumberRequired];
    
    // save db for readbility
    SampleSentences *samples = [SampleSentences getSingleton];
    
    // start finding sentences with our JLPT level down, but then add a JLPT
    // level each time until we find a sentence
    int jlptLevel = [KrLocalOptions getSingleton].showWordsFor;
    NSMutableArray *idList;
    do {
        // get jlpt level list as string
        NSString *jlptStringList = [[JapaneseDictionary getSingleton] getJlptStringListFromLevelNumber:jlptLevel];
        NSString *jlptIdList = [[JapaneseDictionary getSingleton] getCategoryIdListFromStringList:jlptStringList];
        
        // find all sentences that use this word and randomize
        // don't find words with missing stroke data for writing game
        NSString *query1 = [NSString stringWithFormat:@"select s.sentence_id from sentence_words sw join sentence s on s.sentence_id=sw.sentence_id where sw.dictionary_id=%d and (sense_index=%d or sense_index=0) and s.custom_category_id in %@ and uses_kanji=1 and missing_stroke_data=0", inEntry.dictionaryId, inSenseIndex, jlptIdList];
        
//        query1 = @"select sentence_id from sentence where sentence_id=95130";
        
        // try and find sentences WITHIN the users selected JLPT level first
        idList = [JmwDatabase getIdListFromQuery:query1];
        
        // go to next JLPT level
        jlptLevel++;
    } while (idList.count == 0 && jlptLevel <= kcJlptLevel_0);
    
    // if none found, log error
    if (idList.count == 0) {
        NSLog(@"No sentences found at JLPT level for word %@,%@, trying all levels\r\n", inEntry.kanji, inEntry.kana);
    }
    
    // how manu sentences did we find
    NSLog(@"Found %lu sentences for word %@,%@\r\n", (unsigned long)idList.count, inEntry.kanji, inEntry.kana);
    
    // pick random entries until we have enough
    while (idList.count > 0 && returnArray.count < inNumberRequired) {
        // get random sentence
        NSUInteger pickIndex = (random() % idList.count);
        int sentenceId = [[idList objectAtIndex:pickIndex] intValue];
        SampleEntry *thisEntry = [samples getEntryUsingSentenceId:sentenceId];
        
        // remove from id list
        [idList removeObjectAtIndex:pickIndex];
        
        // add to list
        [returnArray addObject:thisEntry];
    }
    
    // what are leitner settings
//    NSString *key = [NSString stringWithFormat:@"%@\r\n%@\r\n%d", inEntry.kanji, inEntry.kana, inSenseIndex];
    int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKanji:inEntry.kanji andKana:inEntry.kana andSenseIndex:inSenseIndex withBlasterGameType:gameType];
//    int leitnerDeck = [[KrAppOptions getSingleton] getLeitnerDeckNumberForKey:key isWritingGame:gameType&blGameType_Writing];
    NSUInteger daysSinceShown = [[KrAppOptions getSingleton] getDaysSinceLastShownForKanji:inEntry.kanji andKana:inEntry.kana andSenseIndex:inSenseIndex withBlasterGameType:gameType];
//    int daysSinceShown = [[KrAppOptions getSingleton] getDaysSinceLastShownForKey:key isWritingGame:gameType&blGameType_Writing];
    NSLog(@"Word is in leitner deck %d with days since shown of %lu\r\n", leitnerDeck, (unsigned long)daysSinceShown);
    
    return returnArray;
}

-(NSMutableArray*)getDictionaryKanaIndexList {
    NSMutableArray *indexList = [NSMutableArray arrayWithCapacity:3000];
    
    // get jlpt id list based on users selected JLPT level
    NSString *jlptStringList = [[JapaneseDictionary getSingleton] getJlptStringListFromLevelNumber:(jlptLevelEnum)[KrLocalOptions getSingleton].showWordsFor];
    NSString *jlptIdList = [[JapaneseDictionary getSingleton] getCategoryIdListFromStringList:jlptStringList];
    
    // get all id's for selected word level
    sqlite3 *dictionaryDb = [JmwDatabase getSingleton];
    NSString *query = [NSString stringWithFormat:@"select kana_index from dictionary d where custom_category_id in %@", jlptIdList];
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(dictionaryDb, [query UTF8String], -1, &statement, nil) != SQLITE_OK) {
//        char *errMessage;
//        errMessage = (char*)sqlite3_errmsg(dictionaryDb);
        sqlite3_finalize(statement);
        return nil;
    }
    
    // create connection for each word, make sure we have 2 connections
    while (sqlite3_step(statement) == SQLITE_ROW) {
        // get this index from the database and add to list
        int kanaIndex = sqlite3_column_int(statement, 0);
        [indexList addObject:[NSNumber numberWithInt:kanaIndex]];
    }
    sqlite3_finalize(statement);
    
    return indexList;
}

-(void)dealloc {
    // release our variables
    [kanjiPlayList release];
    [allCards release];
//    [jouyuuCards release];
    [playCards release];
    
    // super
    [super dealloc];
}

@end
