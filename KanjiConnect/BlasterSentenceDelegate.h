//
//  BlasterSentenceDelegate.h
//  KanjiConnect
//
//  Created by Ray Price on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef KanjiConnect_BlasterSentenceDelegate_h
#define KanjiConnect_BlasterSentenceDelegate_h

@class BlasterSentence;

@protocol BlasterSentenceDelegate

-(void)didCompleteSentence:(BlasterSentence*)inSentence;

@end


#endif
