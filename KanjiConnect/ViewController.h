//
//  ViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlasterGame.h"
#import "SectionBrowserViewControllerDelegate.h"
#import "CloudFileQuery.h"

@interface ViewController : UIViewController <SectionBrowserViewControllerDelegate, UIAlertViewDelegate> {
    IBOutlet UISegmentedControl *readingStudyFreeSegment;
    IBOutlet UISegmentedControl *writingStudyFreeSegment;
    IBOutlet UIActivityIndicatorView *activityView;
    IBOutlet UIButton *readingButton;
    IBOutlet UIButton *writingButton;
    IBOutlet UIButton *listsButton;
    IBOutlet UIButton *optionsButton;
    
    blGameTypeEnum selectedGameType;
    
    CloudFileQuery *cloudQuery;
    
    int optionsCheckCount;
    NSTimer *checkTimer;
}

-(IBAction)readingButtonPressed:(id)sender;
-(IBAction)writingButtonPressed:(id)sender;
-(IBAction)readingStudyFreeSwitchValueChanged:(id)sender;
-(IBAction)writingStudyFreeSwitchValueChanged:(id)sender;
-(IBAction)flashDecksButtonPressed:(id)sender;

-(void)checkOkToPlayForGameType:(blGameTypeEnum)inGameType completion:(void (^)(BOOL success)) block;
-(void)pushControllerForGameType:(blGameTypeEnum)inGameType;
//-(BOOL)waitForUpdatedOptions;
-(void)createGameWithType:(ViewController*)inController;

-(void)checkOptionsTimerFired:(NSTimer*)inTimer;

//-(void)pushControllerForGameType:(NSNumber*)inGameTypeNumber;

@end
