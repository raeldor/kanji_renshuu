//
//  BlasterKanjiSgo.h
//  KanjiConnect
//
//  Created by Ray Price on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SceneGraphObject.h"
#import "KanjiDrawingView.h"
#import "BlasterKanji.h"
#import "ScrollLabel.h"

@interface BlasterKanjiSgo : SceneGraphObject {
//    IBOutlet KanjiDrawingView *drawingView;
//    IBOutlet UIView *colorView;
//    IBOutlet ScrollingLabel *meaningLabel;
//    IBOutlet UILabel *questionLabel;
    
    BlasterKanji *kanji;
}

@property (nonatomic, assign) IBOutlet KanjiDrawingView *drawingView;
@property (nonatomic, assign) IBOutlet UILabel *meaningLabel;
@property (nonatomic, assign) IBOutlet UILabel *questionLabel;
@property (nonatomic, assign) BlasterKanji *kanji;
@property (nonatomic, assign) IBOutlet UIView *colorView;

+(BlasterKanjiSgo*)BlasterKanjiSgoWithKanji:(BlasterKanji*)inKanji;
-(void)setKanji:(BlasterKanji*)inKanji;
-(void)setAsCompleted;

@end
