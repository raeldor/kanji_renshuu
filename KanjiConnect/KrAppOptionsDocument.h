//
//  AppOptionsDocument.h
//  KanjiConnect
//
//  Created by Ray Price on 10/12/12.
//
//

#import <Foundation/Foundation.h>

@interface KrAppOptionsDocument : UIDocument {
	NSMutableDictionary *appOptions;
}

@property (nonatomic, retain) NSMutableDictionary *appOptions;

@end
