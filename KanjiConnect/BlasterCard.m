//
//  BlasterCard.m
//  KanjiConnect
//
//  Created by Ray Price on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BlasterCard.h"

@implementation BlasterCard

@synthesize dictionaryId;
@synthesize kanji;
@synthesize kana;
@synthesize senseIndex;

-(id)initWithDictionaryId:(int)inDictionaryId kanji:(NSString*)inKanji andKana:(NSString*)inKana andSense:(int)inSenseIndex {
    self = [super init];
    if (self) {
        // save vars
        dictionaryId = inDictionaryId;
        kanji = [inKanji retain];
        kana = [inKana retain];
        senseIndex = inSenseIndex;
    }
    return self;
}

-(void)dealloc {
    // release ours retains
    [kanji release];
    [kana release];
    
    // super
    [super dealloc];
}

@end
