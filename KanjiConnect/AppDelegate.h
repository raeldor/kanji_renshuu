//
//  AppDelegate.h
//  KanjiConnect
//
//  Created by Ray Price on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KrLocalOptions.h"
#import "KrAppOptions.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) KrLocalOptions *localOptions;
@property (nonatomic, retain) KrAppOptions *appOptions;
@property (nonatomic, assign) BOOL isOptionsLoaded;
@property (nonatomic, assign) BOOL isLocalOptionsLoaded;

// fonts that can change size depending on device/options
@property (nonatomic, retain) UIFont *dictionaryListDefinitionFont;
@property (nonatomic, retain) UIFont *dictionaryListReadingFont;
@property (nonatomic, retain) UIFont *dictionaryListMeaningFont;
@property (nonatomic, retain) UIFont *dictionaryViewDefinitionFont;
@property (nonatomic, retain) UIFont *dictionaryViewGradeFont;
@property (nonatomic, retain) UIFont *dictionaryViewReadingFont;
@property (nonatomic, retain) UIFont *dictionaryViewSenseNumberFont;
@property (nonatomic, retain) UIFont *dictionaryViewMeaningFont;
@property (nonatomic, retain) UIFont *dictionaryViewNotesFont;
@property (nonatomic, retain) UIFont *dictionaryViewConjugationTypeFont;
@property (nonatomic, retain) UIFont *dictionaryViewConjugationFont;
@property (nonatomic, retain) UIFont *deckEditDefinitionFont;
@property (nonatomic, retain) UIFont *deckEditReadingFontFont;
@property (nonatomic, retain) UIFont *deckEditMeaningFont;

-(void)localOptionsLoaded:(NSNotification *) notification;
-(void)appOptionsLoaded:(NSNotification *) notification;
-(void)copyBundledDocumentsOfType:(NSString*)inType;
-(BOOL)isBundledDocumentWithName:(NSString*)inName andType:(NSString*)inType;

@end
