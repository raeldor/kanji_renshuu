//
//  KanjiPickerViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KanjiPickerViewController.h"
#import "KanjiPickerCell.h"

@interface KanjiPickerViewController ()

@end

@implementation KanjiPickerViewController

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // get dictionary and store as shortcut
    kanjiDictionary = [KanjiDictionary getSingleton];
    
    // selected kanji list
    searchResults = [[NSMutableArray alloc] initWithCapacity:10];
    selectedKanjis = [[NSMutableArray alloc] initWithCapacity:10];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction)handleLongPressGesture:(UIGestureRecognizer *)gestureRecognizer {
    // if we're now recognized
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        // convert search result to a list of kanji for speed
        NSMutableArray *searchKanjiList = [[KanjiDictionary getSingleton] getKanjiListFromIndexList:searchResults];
        
        // select all current search results
        for (int s=0; s < searchKanjiList.count; s++) {
            // already in selected list?
            NSString *kanji = [searchKanjiList objectAtIndex:s];
            int selectedIndex = -1;
            for (int i=0; i < selectedKanjis.count; i++)
                if ([[selectedKanjis objectAtIndex:i] isEqualToString:kanji]) {
                    selectedIndex = i;
                    break;
                }
            
            // select or deselect
            if (selectedIndex != -1) {
                // remove from selected list
                [selectedKanjis removeObjectAtIndex:selectedIndex];
            }
            else {
                // add to selected list
                [selectedKanjis addObject:kanji];
            }
        }
        
        // refresh table to display check marks
        [searchController.searchResultsTableView reloadData];
    }
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"KanjiPickerCell";
    KanjiPickerCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
	// configure cell
    int section = [indexPath section];
    int row = [indexPath row];

    // if search controller
	KanjiEntry *kanjiEntry;
    if (tableView == self.searchDisplayController.searchResultsTableView)
        kanjiEntry = [self entryAtGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        kanjiEntry = [self entryAtGroupIndex:section entryIndex:row];
	cell.kanjiLabel.text = [kanjiEntry kanji];
	cell.meaningLabel.text = [kanjiEntry getAllMeaningsUsingSeperator:@", "];
	cell.readingLabel.text = [kanjiEntry getAllReadingsUsingSeperator:@", "];
    
    // already in selected list?
    int selectedIndex = -1;
    for (int i=0; i < selectedKanjis.count; i++)
        if ([[selectedKanjis objectAtIndex:i] isEqualToString:[kanjiEntry kanji]]) {
            selectedIndex = i;
            break;
        }
    
    // is it selected?
    if (selectedIndex == -1)
        cell.accessoryType = UITableViewCellAccessoryNone;
    else
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    return cell;
}

-(int)numberOfEntriesInGroup:(int)groupIndex {
    // return group count from dictionary
    int groupCount = [kanjiDictionary getGroupCount];
    return groupCount > 0 ? [kanjiDictionary getEntryCountUsingGroupIndex:groupIndex] : 0;
}

-(KanjiEntry*)entryAtGroupIndex:(int)groupIndex entryIndex:(int)entryIndex {
    // get entry from dictionary
    return [kanjiDictionary getEntryUsingGroupIndex:groupIndex entryIndex:entryIndex];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    // if search controller
    if (tableView == self.searchDisplayController.searchResultsTableView)
        return 1;
    else {
        // get group count from dictionary
        int groupCount = [kanjiDictionary getGroupCount];
        return groupCount > 0 ? groupCount : 1;
    }
}

-(NSString*)tableView:(UITableView*)tableView
titleForHeaderInSection:(NSInteger)section {
    // if search controller
    if (tableView == self.searchDisplayController.searchResultsTableView)
        return @"Search Results";
    else {
        // get group name
        int groupCount = [kanjiDictionary getGroupCount];
        return groupCount > 0 ? [kanjiDictionary getGroupNameUsingIndex:section] : @"No kanjis";
    }
}

-(NSArray*)sectionIndexTitlesForTableView:(UITableView*)tableView {
    // if search controller
    if (tableView == self.searchDisplayController.searchResultsTableView)
        return nil;
    else
        // get full list
        return [kanjiDictionary getCustomCategoryList];
}

-(NSInteger)tableView:(UITableView*)tableView
numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView)
        return searchResults.count;
    else
        return [self numberOfEntriesInGroup:section];
}

-(void)updateSearchResultsUsingSearchController:(UISearchDisplayController*)controller {
	// get search results
	if ([controller.searchBar.text length] != 0) {
        // release existing results
        [searchResults release];
        
        // get search list based on scope
        searchResults = [[kanjiDictionary getIndexListFromSearchString:self.searchDisplayController.searchBar.text usingSearchScope:self.searchDisplayController.searchBar.selectedScopeButtonIndex] retain];

        // add long press to select all
        [searchController.searchResultsTableView removeGestureRecognizer:longPressRecognizer];
        [searchController.searchResultsTableView addGestureRecognizer:longPressRecognizer];
	}
    else
        [searchController.searchResultsTableView removeGestureRecognizer:longPressRecognizer];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // update search results
    [self updateSearchResultsUsingSearchController:controller];
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller 
shouldReloadTableForSearchString:(NSString *)searchString
{
    // update search results
    [self updateSearchResultsUsingSearchController:controller];
    return YES;
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // show scope bar after first time
    self.searchDisplayController.searchBar.showsScopeBar = YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
}

-(IBAction)selectButtonPressed:(id)sender {
    // call delegate with selections
    [delegate didMakeSelection:self selection:selectedKanjis];
    
	// pop controller
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(IBAction)cancelButtonPressed:(id)sender {
	// pop controller
    [self.navigationController popViewControllerAnimated:TRUE];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

-(void)tableView:(UITableView*)tableView
didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// which row are we getting
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
    
	// find kanji entry we are touching
	KanjiEntry *kanjiEntry;
    if (tableView == self.searchDisplayController.searchResultsTableView)
        kanjiEntry = [self entryAtGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        kanjiEntry = [self entryAtGroupIndex:section entryIndex:row];
    
    // already in selected list?
    int selectedIndex = -1;
    for (int i=0; i < selectedKanjis.count; i++)
        if ([[selectedKanjis objectAtIndex:i] isEqualToString:[kanjiEntry kanji]]) {
            selectedIndex = i;
            break;
        }
    
    // select or deselect
    if (selectedIndex != -1) {
        // remove from selected list
        [selectedKanjis removeObjectAtIndex:selectedIndex];
    }
    else {
        // add to selected list
        [selectedKanjis addObject:kanjiEntry.kanji];
    }
    
    // refresh table to display check mark
    [tableView reloadData];
}

-(void)dealloc {
    // release our retains
    [selectedKanjis release];
    [searchResults release];
    
    // super
    [super dealloc];
}

@end
