//
//  BlasterWordSgo.m
//  KanjiConnect
//
//  Created by Ray Price on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BlasterWordSgo.h"
#import "BlasterKanji.h"
#import "BlasterKanjiSgo.h"
#import "BlasterGame.h"
#import "TextFunctions.h"
#import "GraphicsHelpers.h"
#import "KrLocalOptions.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGB2(r,g,b) [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1.0f]

@interface BlasterWordSgo ()

@end

@implementation BlasterWordSgo

@synthesize word;
@synthesize meaningLabel;
@synthesize kanaLabel;

+(BlasterWordSgo*)BlasterWordSgoWithWord:(BlasterWord*)inWord {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"BlasterWordSgo-iPad" owner:self options:nil];
    BlasterWordSgo *newSgWord = [nibViews objectAtIndex:0];
    [newSgWord setWord:inWord];
    
    return newSgWord;
}

-(void)setWord:(BlasterWord*)inWord {
    // save word
    word = [inWord retain];
    
    // set word text
    meaningLabel.text = word.meaning;
    kanaLabel.text = word.kana;
    
    // different nib for iPad because it's a different screen ratio
    NSString *nibName = @"BlasterKanjiSgo-iPad";
    
    // get size of kanji view
    NSArray* nibTest = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:0];
    float kanjiWidth = ((UIView*)[nibTest objectAtIndex:0]).frame.size.width;
    
    // set size of frame based on number of kanji
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, word.kanjis.count*kanjiWidth, self.frame.size.height);
    self.meaningLabel.frame = CGRectMake(self.meaningLabel.frame.origin.x, self.meaningLabel.frame.origin.y, self.frame.size.width, self.meaningLabel.frame.size.height);
    
    // set font size based on trying to fit into frame
    int newFontSize = [GraphicsHelpers largestFontSizeForText:meaningLabel.text withFont:meaningLabel.font withSizeConstraint:meaningLabel.frame.size];
    meaningLabel.font = [UIFont fontWithName:meaningLabel.font.fontName size:newFontSize];
    
    // trim kana not to include kana in kanji word and then trim kanalabel
    if ([TextFunctions containsKanji:word.kanji]) {
        // trim trailing kana
        int trailingKanaCount = 0;
        for (int i=(int)word.kanji.length-1; i >= 0; i--) {
            if ([TextFunctions isKana:[word.kanji substringWithRange:NSMakeRange(i, 1)]]) {
                trailingKanaCount++;
                kanaLabel.text = [kanaLabel.text substringToIndex:kanaLabel.text.length-1];
            }
            else
                break;
        }
        
        // trim leading kana
        int leadingKanaCount = 0;
        for (int i=0; i < word.kanji.length-1; i++) {
            if ([TextFunctions isKana:[word.kanji substringWithRange:NSMakeRange(i, 1)]]) {
                leadingKanaCount++;
                kanaLabel.text = [kanaLabel.text substringFromIndex:1];
            }
            else
                break;
        }
        
        // trim kana label size
        kanaLabel.frame = CGRectMake(kanaLabel.frame.origin.x+leadingKanaCount*kanjiWidth, kanaLabel.frame.origin.y, kanaLabel.frame.size.width-((trailingKanaCount+leadingKanaCount)*kanjiWidth), kanaLabel.frame.size.height);
    }
    
    // add sub view controllers for kanji
    BlasterGame *thisGame = word.containerGame;
    for (int i=0; i < word.kanjis.count; i++) {
        // find the kanji entry
        BlasterKanji *thisKanji = [word.kanjis objectAtIndex:i];
        NSString *thisKanjiString = [word.kanji substringWithRange:NSMakeRange(i, 1)];
        
        // create new kanji view
        BlasterKanjiSgo *kanjiView = [BlasterKanjiSgo BlasterKanjiSgoWithKanji:thisKanji];
        [self addSubview:kanjiView];
        
        // set drawing background color to word color
        kanjiView.drawingView.backgroundColor = self.backgroundColor;
        
        // color words not in the dictionary in gray
        if ([inWord.meaning isEqualToString:@""])
            kanjiView.drawingView.drawColor = [UIColor grayColor];
            
        // color depending on word attributes
        if (word.isInJlptLevel && word.isWellKnown && !word.toBeCompleted) {
            kanjiView.questionLabel.textColor = UIColorFromRGB2(0,200,0); // green
            kanjiView.drawingView.drawColor = UIColorFromRGB2(0,200,0); // green
        }
        else {
            if (!word.isInJlptLevel && !word.toBeCompleted && [TextFunctions containsKanji:word.kanji]) {
                kanjiView.questionLabel.textColor = UIColorFromRGB2(255,128,128); // red
                kanjiView.drawingView.drawColor = UIColorFromRGB2(255,128,128); // red
            }
            else {
                if (word.toBeCompleted) {
                    // support dark mode in ios13
                    if (@available(iOS 13.0, *)) {
                        kanjiView.questionLabel.textColor = [UIColor labelColor];
                    } else {
                        // Fallback on earlier versions
                        kanjiView.questionLabel.textColor = [UIColor blackColor];
                    }
                    kanjiView.drawingView.drawColor = UIColorFromRGB2(128,128,255); // blue
                }
            }
        }
        
        // set position
        kanjiView.frame = CGRectMake(i*kanjiWidth, kanaLabel.frame.size.height, kanjiView.frame.size.width, kanjiView.frame.size.height);
        
        // if this word is completed, show kanji meaning (in gray)
        if (word.completed && ![TextFunctions isKana:thisKanjiString])
            kanjiView.meaningLabel.hidden = NO;
        
        // if it's a word we know well, hide kanji meaning too
        if (word.isInJlptLevel && word.isWellKnown && !word.toBeCompleted)
            kanjiView.meaningLabel.hidden = YES;
        else {
            // if this is a writing game, but the word is well known for reading, still
            // don't display it's kanji meanings
            if (thisGame.gameType&blGameType_Writing && word.isWellKnownForReading)
                kanjiView.meaningLabel.hidden = YES;
        }
        
        // if user has chosen to hide all furigana by default, hide kanji meaning too
        if ([KrLocalOptions getSingleton].hideAllFurigana)
            kanjiView.meaningLabel.hidden = YES;
        
        // add to word as sub-view
        [self addSubview:kanjiView];
    }
    
    // if word is completed
    if (word.completed) {
        // don't show kana if only kana
        if (![TextFunctions containsKanji:word.kanji]) {
            kanaLabel.hidden = YES;
        }
        else {
            // if it's a word we know well, hide description
            if (word.isInJlptLevel && word.isWellKnown)
                kanaLabel.hidden = YES;
            else {
                // if this is a writing game, but the word is well known for reading, still
                // don't display it's furigana
                if (thisGame.gameType&blGameType_Writing && word.isWellKnownForReading)
                    kanaLabel.hidden = YES;
                else
                {
                    // if user has chosen to hide all furigana by default
                    if ([KrLocalOptions getSingleton].hideAllFurigana)
                        kanaLabel.hidden = YES;
                    else
                        kanaLabel.hidden = NO;
                }
            }
        }
        
        // if this is a particle, don't show meaning
        if ([word.wordEntry.wordType isEqualToString:@"Particle"])
            meaningLabel.hidden = YES;
        else {
            // show meaning for words outside our jlpt level
            if (word.isInJlptLevel)
                meaningLabel.hidden = YES;
            else
            {
                // if user has chosen to hide all furigana by default
                if ([KrLocalOptions getSingleton].hideAllFurigana)
                    meaningLabel.hidden = YES;
                else
                    meaningLabel.hidden = NO;
            }
        }
    }
    else {
        // needs to be completed
        
        // don't show meaning
        meaningLabel.hidden = YES;
        
        // if this is reading game
        if (thisGame.gameType&blGameType_Reading) {
            // show label as a '?'
            kanaLabel.text = @"?";
            kanaLabel.backgroundColor = [UIColor lightGrayColor];
        }
    }
}

-(void)setAsCompleted {
    // set as completed
    word.completed = YES;
    
    // if this is reading game, show meaning of all kanji
    BlasterGame *thisGame = word.containerGame;
    if (thisGame.gameType&blGameType_Reading)
        [self setKanjiHiddenStateTo:NO];
}

-(void)helpButtonPressed:(id)sender {
    // user looked
    word.hadToLook = YES;
    
    // which stage are we on?
    if (helpStage < 4) {
        // increment help stage
        helpStage++;
        
        // display extra information depending on stage
        switch (helpStage) {
            case 1:
                // show invividual kanji meanings
                [self setKanjiHiddenStateTo:NO];
                break;
            case 2:
                // show word meaning
                meaningLabel.hidden = NO;
                break;
            case 3:
                // show furigana
                kanaLabel.text = [word furigana];
//                kanaLabel.text = [word.kana substringToIndex:word.kana.length-word.conjugation.length];
            default:
                break;
        }
    }
}

-(BOOL)clearButtonPressed:(id)sender {
    // go back a stage
    if (helpStage > 0) {
        // go back a stage
        helpStage--;
        
        // change visual
        switch (helpStage) {
            case 0:
                // hide kanji meaning again
                [self setKanjiHiddenStateTo:YES];
                break;
            case 1:
                // hide word meaning
                meaningLabel.hidden = YES;
                break;
            case 2:
                // reset furigana
                kanaLabel.text = @"?";
            default:
                break;
        }
        return YES;
    }
    else
        return NO;
}

-(void)setKanjiHiddenStateTo:(BOOL)inState {
    // set state of kanji in word
    for (int i=0; i < self.subviews.count; i++) {
        // if this is a kanji
        UIView *subView = [self.subviews objectAtIndex:i];
        if ([subView isKindOfClass:[BlasterKanjiSgo class]]) {
            BlasterKanjiSgo *kanjiSgo = (BlasterKanjiSgo*)subView;
            // if this is not kana
            if (![TextFunctions isKana:kanjiSgo.kanji.kanji])
                // show meaning of kanji
                kanjiSgo.meaningLabel.hidden = inState;
        }
    }
}

-(void)dealloc
{
    // release variables
    [word release];
    
    // call super
    [super dealloc];
}

@end
