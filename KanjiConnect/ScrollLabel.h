//
//  ScrollingLabel.h
//  KanjiConnect
//
//  Created by Ray Price on 10/25/12.
//
//

#import <UIKit/UIKit.h>

@interface ScrollLabel : UILabel {
    
}

-(void)startScrolling;

@end
