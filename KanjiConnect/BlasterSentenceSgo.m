//
//  BlasterSentenceSgo.m
//  KanjiConnect
//
//  Created by Ray Price on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BlasterSentenceSgo.h"
#import "BlasterWord.h"
#import "BlasterWordSgo.h"
#import "BlasterGame.h"
#import "BlasterSentenceWord.h"
#import "TextFunctions.h"
#import "BlasterKanjiSgo.h"
#import "KanjiDictionary.h"

@interface BlasterSentenceSgo ()

@end

@implementation BlasterSentenceSgo

@synthesize sentence;
@synthesize meaningLabel;
@synthesize maxWidth;
@synthesize sentenceLineHeight;

+(BlasterSentenceSgo*)BlasterSentenceSgoWithSentence:(BlasterSentence*)inSentence usingWidth:(float)inWidth {
    // get from nib
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"BlasterSentenceSgo-iPad" owner:self options:nil];
    BlasterSentenceSgo *newSgSentence = [nibViews objectAtIndex:0];
    newSgSentence.maxWidth = inWidth;
    [newSgSentence setSentence:inSentence];
    
    return newSgSentence;
}

-(void)setSentence:(BlasterSentence*)inSentence {
    // get and save single sentence height
    sentenceLineHeight = self.bounds.size.height;
    
    // find word top margin (height of furigana)
    wordTopMargin = 44.0f;
    kanjiWidth = 88.0f;
    
    // save word
    sentence = [inSentence retain];
    
    // set meaning text
    meaningLabel.text = sentence.meaning;
    
    // track size for setting frame
    float sentenceWidth = 0.0f;
    float sentenceHeight = 0.0f;
    float xOffset = 0.0f;
    float yOffset = 0.0f;
    
    // take the sentence and replace the words with placeholders
    NSMutableString *newSentence = [NSMutableString stringWithString:sentence.kanji];
    for (int i=0; i < sentence.words.count; i++) {
        // find the word entry
        BlasterWord *thisWord = [sentence.words objectAtIndex:i];
        
        // replace with special character (@)
        NSRange range = [newSentence rangeOfString:thisWord.kanji];
        if (range.location == NSNotFound) {
//            [NSException raise:@"Error in BlasterSentenceSgo:setSentence" format:@"Cannot find word %@ in sentence %@", thisWord.kanji, sentence.kanji];
        }
        else
            [newSentence replaceCharactersInRange:range withString:@"@"];
    }
    
    // loop through characters 1-by-one
    int wordIndex = 0;
    for (int c=0; c < newSentence.length; c++) {
        // for placeholders, add word object
        NSString *thisChar = [newSentence substringWithRange:NSMakeRange(c, 1)];
        
        // new sgo
        SceneGraphObject *newSgo;
        
        // for non-placeholders (punctuation, names), add kanji object
        if ([thisChar isEqualToString:@"@"]) {
            // find the word entry
            BlasterWord *thisWord = [sentence.words objectAtIndex:wordIndex++];
            
            // create new word view
            newSgo = [BlasterWordSgo BlasterWordSgoWithWord:thisWord];
        }
        else {
            // try and find a kanji entry if we can
            BlasterKanji *thisKanji;
            KanjiEntry *thisEntry = [[KanjiDictionary getSingleton] getEntryUsingKanji:thisChar];
            if (thisEntry != nil) {
                thisKanji = [[BlasterKanji alloc] initWithKanjiEntry:thisEntry toBeCompleted:NO forGame:inSentence.containerGame];
            }
            else {
                // find the kanji
                thisKanji = [[BlasterKanji alloc] initWithKanjiString:thisChar forGame:inSentence.containerGame];
            }
            
            // create new kanji view
            BlasterKanjiSgo *newKanji = [BlasterKanjiSgo BlasterKanjiSgoWithKanji:thisKanji];
            
            // set kanji for words we can't find to gray
            newKanji.drawingView.drawColor = [UIColor grayColor];
            
            // this becomes sgo
            newSgo = newKanji;
            
            // release this kanji
            [thisKanji release];
        }
        
        // if we've exceeded max width
        NSString *nextChar = @"";
        if (c+1 < newSentence.length)
            nextChar = [newSentence substringWithRange:NSMakeRange(c+1, 1)];
        if (xOffset+newSgo.frame.size.width > maxWidth || (([nextChar isEqualToString:@"、"] ||
            [nextChar isEqualToString:@"。"]) && xOffset+newSgo.frame.size.width+kanjiWidth > maxWidth)) {
            yOffset += sentenceLineHeight; // use actual value because kanji is different height
            xOffset = 0.0f;
        }
        
        // track size
        if (xOffset+newSgo.frame.size.width > sentenceWidth)
            sentenceWidth = xOffset+newSgo.frame.size.width;
        if (yOffset+sentenceLineHeight > sentenceHeight)
            sentenceHeight = yOffset+sentenceLineHeight;
        
        // set position and add to size
        newSgo.frame = CGRectMake(xOffset, yOffset+([thisChar isEqualToString:@"@"]?0.0f:wordTopMargin), newSgo.frame.size.width, newSgo.frame.size.height);
        xOffset += newSgo.frame.size.width;
        
        // add to word as sub-view
        [self addSubview:newSgo];
    }
    
    // set size of frame based on word sizes
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, sentenceWidth, sentenceHeight);
    self.meaningLabel.hidden = YES;
//    self.backgroundColor = [UIColor lightGrayColor];
    
    // if this is reading game
    BlasterGame *thisGame = sentence.containerGame;
    if (thisGame.gameType&blGameType_Reading) {
        // hide word meaning and pronunciation
        meaningLabel.hidden = YES;
        
        // show pronunciation for words we don't know (ie, out of JLPT level or
        // entirely containing kanji we don't know)
        for (int i=0; i < self.subviews.count; i++) {
            UIView *thisView = [self.subviews objectAtIndex:i];
            if ([thisView isKindOfClass:[BlasterWordSgo class]]) {
                // show this word's meaning
                BlasterWordSgo *thisSgo = (BlasterWordSgo*)thisView;
                if (thisSgo.word.completed) {
                    // show meaning if it's not a particle
                    if (thisSgo.word.wordEntry == nil)
                        thisSgo.meaningLabel.hidden = NO;
                }
            }
        }
    }
}

-(void)setAsCompleted {
    // if this is reading game
    BlasterGame *thisGame = sentence.containerGame;
    if (thisGame.gameType&blGameType_Reading) {
        // show description and kana now
        meaningLabel.hidden = NO;
    }
}

-(void)dealloc
{
    // release variables
    [sentence release];
    
    // call super
    [super dealloc];
}

@end
