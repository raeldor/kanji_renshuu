//
//  KanjiList.h
//  KanjiConnect
//
//  Created by Ray Price on 7/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataFile.h"
#import "DataFileDelegate.h"

@interface KanjiList : DataFile <DataFileDelegate, NSCopying> {
    NSString *listName;
    NSMutableArray *kanjiList;
}

@property (nonatomic, retain) NSString *listName;
@property (nonatomic, retain) NSMutableArray *kanjiList;

-(id)initFromArchive:(NSString*)inName;
+(void)deleteKanjiList:(NSString*)inName;

@end
