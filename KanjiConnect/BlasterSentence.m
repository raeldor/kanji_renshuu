//
//  BlasterSentence.m
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BlasterSentence.h"
#import "BlasterSentenceWord.h"
#import "JapaneseDictionary.h"
#import "TextFunctions.h"
#import "KrAppOptions.h"

@implementation BlasterSentence

@synthesize words;
@synthesize meaning;
@synthesize completed;
@synthesize containerGame;
@synthesize delegate;
@synthesize kanji;

-(id)initWithSentenceEntry:(SampleEntry*)inEntry forGame:(BlasterGame*)inGame {
    self = [super init];
    if (self) {
        // save entry and game
        sentenceEntry = [inEntry retain];
        containerGame = [inGame retain];
        
        // save meaning
        meaning = [sentenceEntry.meaning retain];
        kanji = [sentenceEntry.kanji retain];
        
        // create sentence words
        // loop through words in sentence
        words = [[NSMutableArray alloc] initWithCapacity:10];
        for (int i=0; i < sentenceEntry.words.count; i++) {
            // create game sentence word from this
            SampleEntryWord *thisEntry = [sentenceEntry.words objectAtIndex:i];
                        
            // create word and set ourselves as the delegate
            BlasterWord *newWord = [[BlasterWord alloc] initWithSampleEntryWord:thisEntry forGame:containerGame];
            newWord.delegate = self;
            
            // add word to list
            [words addObject:newWord];
            [newWord release];
        }
    }
    return self;
}

-(void)didCompleteWord:(id)inWord {
    // word completed for this sentence, check to see if ALL words are now completed
    BOOL allCompleted = YES;
    for (int i=0; i < words.count; i++) {
        BlasterWord *thisWord = [words objectAtIndex:i];
        if (!thisWord.completed) {
            allCompleted = NO;
            break;
        }
    }
    
    // if all words are completed, alert OUR delegate
    if (allCompleted)
        [delegate didCompleteSentence:self];
}

-(void)dealloc {
    // release our retains
    [sentenceEntry release];
    [containerGame release];
    [meaning release];
    [kanji release];
    [words release];
    
    // super
    [super dealloc];
}

@end
