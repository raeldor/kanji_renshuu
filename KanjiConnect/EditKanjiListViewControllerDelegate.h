//
//  EditKanjiListViewControllerDelegate.h
//  KanjiConnect
//
//  Created by Ray Price on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef KanjiConnect_EditKanjiListViewControllerDelegate_h
#define KanjiConnect_EditKanjiListViewControllerDelegate_h

#import "KanjiList.h"

@protocol EditKanjiListViewControllerDelegate

-(void)didUpdateKanjiList:(KanjiList*)inKanjiList;
-(void)didAddKanjiList:(KanjiList*)inKanjiList;

@end


#endif
