//
//  EditKcListViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EditKanjiListViewController.h"
#import "KcEditNameCell.h"
#import "KcKanjiListEntryCell.h"
#import "KanjiPickerViewController.h"

@interface EditKanjiListViewController ()

@end

@implementation EditKanjiListViewController

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

-(void)setEditableObject:(KanjiList*)inEditableObject isNew:(BOOL)inIsNew {
    // save object and isnew state
    editList = [inEditableObject copy];
    originalList = [inEditableObject retain];
    isNew = inIsNew;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // set to edit mode to use add/delete
    [self.tableView setEditing:TRUE animated:NO];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(BOOL)isOkToSave {
    return YES;
}

-(void)saveButtonClick:(id)sender {
	// check first for errors
	if ([self isOkToSave])
	{
        // update original object
        originalList.listName = editList.listName;
        originalList.kanjiList = editList.kanjiList;
        
		// notify parent of change
		if (isNew)
            [delegate didAddKanjiList:originalList];
		else
            [delegate didUpdateKanjiList:originalList];
		
		// pop controller
        [self.navigationController popViewControllerAnimated:TRUE];
	}
}

-(void)cancelButtonClick:(id)sender {
	// pop controller
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(IBAction)didChangeName:(id)sender {
    // sender is text field
    UITextField *nameField = (UITextField*)sender;
    editList.listName = nameField.text;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    // return title
    switch (section) {
        case 0:
            return @"General";
            break;
        case 1:
            return @"Kanji";
            break;
        default:
            break;
    }
    return @"Untitled";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
        return 1;
    else
        return editList.kanjiList.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Cell";
    NSString *nameCellIdentifier = @"KcEditNameCell";
    NSString *listCellIdentifier = @"KcKanjiListEntryCell";
    
    // which section
    if ([indexPath section] == 0) {
        KcEditNameCell *cell = [tableView dequeueReusableCellWithIdentifier:nameCellIdentifier];
        cell.nameField.text = editList.listName;
        return cell;
    }
    else {
        // set description
        if ([indexPath row] < editList.kanjiList.count) {
            KcKanjiListEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:listCellIdentifier];
            KanjiEntry *thisEntry = [[KanjiDictionary getSingleton] getEntryUsingKanji:[editList.kanjiList objectAtIndex:[indexPath row]]];
            cell.kanjiLabel.text = [editList.kanjiList objectAtIndex:[indexPath row]];
            cell.meaningLabel.text = [thisEntry getAllMeaningsUsingSeperator:@", "];
            return cell;
        }
        else {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            cell.textLabel.text = @"Add Kanji";
            cell.textLabel.textColor = [UIColor lightGrayColor];
            return cell;
        }
    }
    
    // no cell
    return nil;
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// which row are we getting
	NSUInteger row = [indexPath row];
	
    // if we are looking at kanji
    if ([indexPath section] == 1) {
		// show delete style and insert for 'add new'
		if (row != editList.kanjiList.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
    }
    
	return UITableViewCellEditingStyleNone;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [editList.kanjiList removeObjectAtIndex:[indexPath row]];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        // fire kanjipicker segue to show list of kanji
        [self performSegueWithIdentifier:@"KanjiPickerSegue" sender:self];
    }   
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // call super first
    [super prepareForSegue:segue sender:sender];
    
    // only one segue
    KanjiPickerViewController *pickerController = [segue destinationViewController];
    
    // set delegate
    pickerController.delegate = self;
}

-(void)didMakeSelection:(id)sender selection:(NSMutableArray*)inSelection {
    // add selected kanjis to our list
    for (int i=0; i < inSelection.count; i++) {
        // see if already exists
        BOOL found = NO;
        for (int j=0; j < editList.kanjiList.count; j++)
            if ([[editList.kanjiList objectAtIndex:j] isEqualToString:[inSelection objectAtIndex:i]]) {
                found = YES;
                break;
            }
        
        // if not found, add
        if (!found)
            [editList.kanjiList addObject:[inSelection objectAtIndex:i]];
    }
    
    // refresh table view
    [self.tableView reloadData];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

-(void)dealloc {
    // release our retains
    [editList release];
    [originalList release];
    
    // super
    [super dealloc];
}

@end
