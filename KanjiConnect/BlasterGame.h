//
//  BlasterGame.h
//  KanjiConnect
//
//  Created by Ray Price on 7/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JapaneseWord.h"
#import "BlasterKanji.h"
#import "BlasterGameDelegate.h"
#import "BlasterSentenceDelegate.h"
#import "BlasterCard.h"
//#import "KrAppOptions.h" // removed for circular reference
//#import "KrLocalOptions.h"
#import "CloudFileQuery.h"

typedef enum {
    blGameType_Study = 1,
    blGameType_Freeplay = 2,
    blGameType_Writing = 4,
    blGameType_Reading = 8
} blGameTypeEnum;


@interface BlasterGame : NSObject <BlasterSentenceDelegate> {
    blGameTypeEnum gameType;
    
    CloudFileQuery *cloudQuery;
    
    NSMutableArray *knownKanji; // of type string
    NSMutableArray *learningKanji; // of type string
    NSMutableArray *knownWords; // of type flashCard
    NSMutableArray *learningWords; // of type flashCard
    
    int playIndex;
    NSMutableArray *kanjiPlayList; // of type string
    NSMutableArray *allCards; // of type blastercard
    NSMutableArray *playCards; // of type blastercard
    BlasterSentence *currentSentence;
    
    // cache word lists form database, because it's quicker when dealing with random stuff
    NSMutableArray *dictionaryKanaIndexList; // of type int
}

-(id)init;
-(void)startGameWithGameType:(blGameTypeEnum)inGameType completion:(void (^)(BOOL success)) block;
-(NSMutableArray*)getDictionaryKanaIndexList;
/*-(NSMutableArray*)getAllCardsFromPlayIdList:(NSString*)inIdList andCombinedIdList:(NSString*)inCombinedIdList;
-(NSMutableArray*)getPlaylistFromPlayIdList:(NSString*)inIdList andCombinedIdList:(NSString*)inCombinedIdList;*/
-(NSMutableArray*)getPlayCardsFromKanjiPlayList:(NSArray*)inKanjiPlayList andKanjiCombinedList:(NSArray*)inKanjiCombinedList andWordPlayList:(NSArray*)inWordPlayList andWordCombinedList:(NSArray*)inWordCombinedList;
-(NSMutableArray*)getAllCardsFromKanjiPlayList:(NSArray*)inKanjiPlayList andKanjiCombinedList:(NSArray*)inKanjiCombinedList andWordPlayList:(NSArray*)inWordPlayList andWordCombinedList:(NSArray*)inWordCombinedList;

//-(NSString*)getJlptStringListFromLevel:(kcJlptLevel)inLevel;
-(NSMutableArray*)getSentencesWithWord:(JapaneseWord*)inEntry andSense:(int)inSenseIndex giveMe:(int)inNumberRequired;
-(BlasterSentence*)getCurrentSentence;
-(BlasterCard*)getCurrentCard;
-(void)checkCardAnswers;
-(void)goToNextCard;
-(BOOL)isKanjiKnown:(NSString*)inKanji;
-(BOOL)isWordKnownForKanji:(NSString*)inKanji andKana:(NSString*)inKana andSenseIndex:(int)inSenseIndex;
-(NSUInteger)getWordCount;
-(NSUInteger)getKanjiCount;
-(void)getLeitnerStatsIntoIntArray:(float[])inIntArray forCards:(NSMutableArray*)inCards;
-(void)getPlayStatsIntoIntArray:(float[])inIntArray;
//-(BOOL)isShowCard:(BlasterCard*)inCard forPass:(int)pass percentageOfBatch:(float)percentageOfBatch;
-(BOOL)showCardInLeitnerDeck:(int)leitnerDeck withDaysSinceShown:(NSUInteger)daysSinceShown andMinutesSinceShown:(NSUInteger)minutesSinceShown forPass:(NSUInteger)pass;
-(NSString*)getKanjiIdStringPlayList:(NSArray*)inList;
-(void)removeDuplicatesFromStringArray:(NSMutableArray*)inArray;
/*
-(void)markAnswerRightForWord:(BlasterWord*)inWord;
-(void)markAnswerWrongForWord:(BlasterWord*)inWord;*/

@property (nonatomic, assign) blGameTypeEnum gameType;
@property (nonatomic, assign) id<BlasterGameDelegate> delegate;

@end
