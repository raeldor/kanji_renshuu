//
//  RadialMenuViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#import "RadialMenuViewControllerDelegate.h"

@interface RadialMenuViewController : UIViewController <AVAudioPlayerDelegate> {
    IBOutlet UIView *menuOption1;
    IBOutlet UIView *menuOption2;
    IBOutlet UIView *menuOption3;
    IBOutlet UIView *menuOption4;
    IBOutlet UILabel *menuLabel1;
    IBOutlet UILabel *menuLabel2;
    IBOutlet UILabel *menuLabel3;
    IBOutlet UILabel *menuLabel4;
    IBOutlet UIImageView *menuImage1;
    IBOutlet UIImageView *menuImage2;
    IBOutlet UIImageView *menuImage3;
    IBOutlet UIImageView *menuImage4;
    
    UILongPressGestureRecognizer *longRecognizer;
    
    int currentSelection;
    int previousSelection;
    
    NSArray *menuStrings;
    
    id<RadialMenuViewControllerDelegate> delegate;
}

@property (nonatomic, assign) id<RadialMenuViewControllerDelegate> delegate;

-(void)processLongPress:(id)sender;
-(BOOL)checkTouchOnView:(UIView*)inView withPoint:(CGPoint)inPoint;
-(void)setMenuItemStrings:(NSArray*)inMenuStrings;
-(void)playSoundEffectNamed:(NSString*)inEffectName withVolume:(float)inVolume;
-(NSString*)getMenuItemStringForIndex:(int)inIndex;

@end
