//
//  BlasterGameDelegate.h
//  KanjiConnect
//
//  Created by Ray Price on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef KanjiConnect_BlasterGameDelegate_h
#define KanjiConnect_BlasterGameDelegate_h

#import "BlasterKanji.h"
#import "BlasterWord.h"
#import "BlasterSentence.h"

@protocol BlasterGameDelegate

-(void)didCompleteKanji:(BlasterKanji*)inKanji;
-(void)didCompleteWord:(BlasterWord*)inWord;
-(void)didCompleteSentence:(BlasterSentence*)inSentence;

@end

#endif
