//
//  ScrollingLabel.m
//  KanjiConnect
//
//  Created by Ray Price on 10/25/12.
//
//

#import "ScrollLabel.h"

@implementation ScrollLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)startScrolling {
    return;
    
    /* not sure why this was removed
    [UILabel animateWithDuration:1.0f delay:0.1f options:UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat animations:^{
        CGAffineTransform transform = CGAffineTransformMakeTranslation(0, -100);
        self.transform = transform;
    } completion:^(BOOL finished) {
    }]; */
}

- (void)drawTextInRect:(CGRect)rect {
    // make font smaller to support multi-line
    
    return [super drawTextInRect:rect];
}

-(void)dealloc {
    // call super
	[super dealloc];
}

@end
