//
//  ProgressViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 9/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlasterGame.h"
#import "TapToCloseDelegate.h"

@interface ProgressViewController : UIViewController {
    IBOutlet UILabel *foundLabel;
    IBOutlet UILabel *percentCompleteLabel;
    IBOutlet UIView *statsView;
    
    // nice pastel colors
    NSMutableArray *connectColors;
    
    // save game
    BlasterGame *game;
}

-(IBAction)viewTapped:(id)sender;
-(void)setGame:(BlasterGame*)inGame;
-(void)updateStats;
-(void)updateGraphView:(UIView*)inGraphView usingStatsArray:(float[])inStatsArray;

@property (nonatomic, assign) id<TapToCloseDelegate> delegate;

@end
