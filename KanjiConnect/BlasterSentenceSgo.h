//
//  BlasterSentenceSgo.h
//  KanjiConnect
//
//  Created by Ray Price on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SceneGraphObject.h"
#import "BlasterSentence.h"

@interface BlasterSentenceSgo : SceneGraphObject {
    IBOutlet UILabel *meaningLabel;
    float sentenceLineHeight;
    float wordTopMargin;
    float kanjiWidth;

    BlasterSentence *sentence;
    float maxWidth;
}
+(BlasterSentenceSgo*)BlasterSentenceSgoWithSentence:(BlasterSentence*)inSentence usingWidth:(float)inWidth;
-(void)setSentence:(BlasterSentence*)inSentence;
-(void)setAsCompleted;

@property (nonatomic, retain) BlasterSentence *sentence;
@property (nonatomic, retain) UILabel *meaningLabel;
@property (nonatomic, assign) float maxWidth;
@property (nonatomic, assign) float sentenceLineHeight;

@end