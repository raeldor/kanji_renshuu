//
//  BlasterKanjiSgo.m
//  KanjiConnect
//
//  Created by Ray Price on 7/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BlasterKanjiSgo.h"
#import "TextFunctions.h"
#import "BlasterGame.h"

@interface BlasterKanjiSgo ()

@end

@implementation BlasterKanjiSgo

@synthesize drawingView;
@synthesize meaningLabel;
@synthesize questionLabel;
@synthesize kanji;
@synthesize colorView;

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

+(BlasterKanjiSgo*)BlasterKanjiSgoWithKanji:(BlasterKanji*)inKanji {
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"BlasterKanjiSgo-iPad" owner:self options:nil];
    BlasterKanjiSgo *newSgKanji = [nibViews objectAtIndex:0];
    [newSgKanji setKanji:inKanji];
    
    return newSgKanji;
}

-(void)setKanji:(BlasterKanji*)inKanji {
    // save kanji
    kanji = [inKanji retain];
    
    // setup drawing
    drawingView.isStatic = YES;
//    drawingView.drawing.strokes = [inKanji.kanjiEntry.drawing.strokes copy];
    if (inKanji.kanjiEntry.meanings.count > 0)
        meaningLabel.text = (NSString*)[inKanji.kanjiEntry.meanings objectAtIndex:0];
    else
        meaningLabel.text = @"Unknown";
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        drawingView.strokeWidth = 3.0f;
    else
        drawingView.strokeWidth = 7.0f;
    
    // make drawing black
    // support dark mode in ios13
    if (@available(iOS 13.0, *)) {
        drawingView.drawColor = [UIColor labelColor];
    } else {
        // Fallback on earlier versions
        drawingView.drawColor = [UIColor blackColor];
    }
    drawingView.shadowColor = [UIColor clearColor];
    
    // background is black
//    drawingView.backgroundColor = [UIColor blackColor];
    meaningLabel.textColor = [UIColor grayColor];
    
    // get game
    BlasterGame *thisGame = kanji.containerGame;
    
    // what type of game?
    if (thisGame.gameType&blGameType_Reading) {
        // kanji are always completed for reading game, so show kanji (in question label)
        meaningLabel.hidden = YES;
        questionLabel.hidden = NO;
        questionLabel.text = kanji.kanji;
//        questionLabel.backgroundColor = [UIColor blackColor];
        
        // if we have drawing, show instead of font
        if (inKanji.kanjiEntry != nil) {
            // show drawing
            drawingView.drawing.strokes = [[inKanji.kanjiEntry.drawing.strokes copy] autorelease];
            drawingView.hidden = NO;
            questionLabel.hidden = YES;
            
            // shrink drawing if this is a chiisaii kana (or kagetsu ka)
            NSString *chiisai = @"ゃゅょっぁぃぅぇぉャュョッィァァィゥェォヶ";
            if ([chiisai rangeOfString:inKanji.kanjiEntry.kanji].location != NSNotFound) {
                // apply transform
                float scale = 0.75f;
                drawingView.transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(scale, scale), 0.0f, drawingView.frame.size.height*(1.0f-scale));
                drawingView.strokeWidth *= 1.0f/scale;
            }
//            else
//                drawingView.transform = CGAffineTransformIdentity;
        }
    }
    else {
        // if not one that the user needs to fill in
        if (!kanji.toBeCompleted) {
            // show as font
            meaningLabel.hidden = YES;
            questionLabel.hidden = NO;
            questionLabel.text = kanji.kanji;
            
            // if we have drawing, show instead of font
            if (inKanji.kanjiEntry != nil) {
                drawingView.drawing.strokes = [[inKanji.kanjiEntry.drawing.strokes copy] autorelease];
                drawingView.hidden = NO;
                questionLabel.hidden = YES;
                
                // shrink drawing if this is a chiisaii kana
                NSString *chiisai = @"ゃゅょっぃぁャュョッィァ";
                if ([chiisai rangeOfString:inKanji.kanjiEntry.kanji].location != NSNotFound) {
                    // apply transform
                    float scale = 0.75f;
                    drawingView.transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(scale, scale), 0.0f, drawingView.frame.size.height*(1.0f-scale));
                    drawingView.strokeWidth *= 1.0f/scale;
                    /*
                    // apply transform
                    drawingView.transform = CGAffineTransformTranslate(CGAffineTransformMakeScale(0.5f, 0.5f), 0.0f, drawingView.frame.size.height/2.0f);
                    drawingView.strokeWidth *= 2.0f;*/
                }
//                else
//                    drawingView.transform = CGAffineTransformIdentity;
            }
        }
        else {
            // if user has not completed
            if (!kanji.completed) {
                // show question mark
                meaningLabel.hidden = YES;
                questionLabel.hidden = NO;
                questionLabel.backgroundColor = [UIColor lightGrayColor];
            }
            else {
                // show users drawing
                meaningLabel.hidden = YES;
                questionLabel.hidden = YES;
                drawingView.hidden = NO;
            }
        }
    }
}

// show real kanji drawing instead of question mark
-(void)setAsCompleted {
    return;
    /* not sure why this is not used anymore yet
    // set kanji a completed
    kanji.completed = YES;
    
    // show drawing AND reading
    meaningLabel.hidden = NO;
    questionLabel.hidden = YES;
    drawingView.hidden = NO;*/
}

-(void)dealloc {
    // release variables
    [kanji release];
    
    // call super
    [super dealloc];
}

@end
