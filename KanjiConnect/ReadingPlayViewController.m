//
//  PlayController.m
//  KanjiConnect
//
//  Created by Ray Price on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ReadingPlayViewController.h"
#import "JapaneseDictionary.h"
#import "KanjiDrawingView.h"
#import "RadialMenuViewController.h"
#import "NSMutableArrayShuffle.h"
#import "ResourceManager.h"
#import "KrAppOptions.h"
#import "TextFunctions.h"
#import "ProgressViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "KrLocalOptions.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGB2(r,g,b) [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1.0f]

@interface ReadingPlayViewController ()

@end

@implementation ReadingPlayViewController

@synthesize game;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
    // call super
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self.navigationController setToolbarHidden:YES animated:animated];
    
    // hide writing interface
    [self hideDrawingInterface:NO];
    
    // hide the animation slider by default
    animationSliderHeightConstraint.constant = 0.0f;
    
    // set priority on auto-layout to make sentence fill
    meaningHeightConstraint.active = NO;
    meaningFixedHeightConstraint.active = YES;
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

-(void)viewDidAppear:(BOOL)animated {
    /* this gets executed after help screen has shown and ends up resetting the already
    answered question
     
     // hide meaning to maximize space
    [self hideSentenceMeaning];
    
    // do here, as may be resized after viewdidload
    // show sentence and set transform
    [self showCurrentSentence];
    newFocusSentence.transform = [self getZoomOutTransform];
     */
    
    // do here, as may be resized after viewdidload
    // only do for first time though, otherwise it wipes answers
    if (newFocusSentence == nil) {
        [self showCurrentSentence];
        newFocusSentence.transform = [self getZoomOutTransform];
    }
}

- (void)viewDidLoad
{
    // call super first
    [super viewDidLoad];
    
    /*
    // create cache for system sounds
    systemSounds = [[NSMutableDictionary alloc] init];
*/
    
    // default zoom and stroke width
    defaultZoom = 0.4f;
    defaultStrokeWidth = 7.0f;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        defaultZoom = 0.5f;
        defaultStrokeWidth = 3.0f;
    }
    
    // set hint and undo colors to lighter gray
    clearButton.backgroundColor = UIColorFromRGB2(220, 220, 220);
    hintButton.backgroundColor = UIColorFromRGB2(220, 220, 220);
    
    // set border on drawing view
    // support dark mode in ios13
    if (@available(iOS 13.0, *)) {
        [writingPopupView.layer setBorderColor: [[UIColor labelColor] CGColor]];
    } else {
        // Fallback on earlier versions
        [writingPopupView.layer setBorderColor: [[UIColor blackColor] CGColor]];
    }
    [writingPopupView.layer setBorderWidth: 1.0];
    
    // event for when keyboard is shown, so we can move the clear and hint buttons
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    // if this is a writing game, move buttons to bottom
    if (game.gameType&blGameType_Writing) {
        // move clear and help buttons
//        clearButton.frame = CGRectOffset(clearButton.frame, 0, 200);
//        hintButton.frame = CGRectOffset(hintButton.frame, 0, 200);

        
        // show/hide correct help button
        readingHelpButton.hidden = YES;
        writingHelpButton.hidden = NO;
    }
    else {
        // show/hide correct help button
        readingHelpButton.hidden = NO;
        writingHelpButton.hidden = YES;
    }
    
    // set delegate for writing view
    writeKanjiController.delegate = self;
    
    [self.navigationController setToolbarHidden:NO];
    
    // setup our drawing stroke width etc
    finishedView.isStatic = YES;
    finishedView.strokeWidth = 12;
    finishedView.shadowColor = [UIColor lightGrayColor];
    drawingView.shadowColor = [UIColor lightGrayColor];
    animatedView.shadowColor = [UIColor lightGrayColor];
    outlineView.shadowColor = [UIColor lightGrayColor];
    // support dark mode in ios13
    if (@available(iOS 13.0, *)) {
        finishedView.drawColor = [UIColor labelColor];
        drawingView.drawColor = [UIColor labelColor];
        animatedView.drawColor = [UIColor labelColor];
        outlineView.drawColor = [UIColor labelColor];
    } else {
        // Fallback on earlier versions
        finishedView.drawColor = [UIColor blackColor];
        drawingView.drawColor = [UIColor blackColor];
        animatedView.drawColor = [UIColor blackColor];
        outlineView.drawColor = [UIColor blackColor];
    }

    // use write kanji controller to isolate kanji writing and hint system
    // for re-use in other apps
    writeKanjiController = [[WriteKanjiViewController alloc] init];
    writeKanjiController.delegate = self;
    [writeKanjiController setControllerArray:[NSArray arrayWithObjects:strokesLabel, clearButton, hintButton, drawingView, buildView, outlineView, animatedView, drawingAnimationSlider, nil]];
    
    // set game delegate
    game.delegate = self;
    
    // create gesture recognizers
    // create single tap recognizer for zoom in
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapRecognizer.delegate = self;
    tapRecognizer.enabled = YES;
    [playView addGestureRecognizer:tapRecognizer];
    
    // create double-tap recognizer for zoom out
    doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.delegate = self;
    doubleTapRecognizer.enabled = YES;
    [playView addGestureRecognizer:doubleTapRecognizer];
    
    // create long press recognizer for clear button
    clearButtonLongRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(clearButtonLongPressed:)];
    longRecognizer.delegate = self;
    [clearButton addGestureRecognizer:clearButtonLongRecognizer];
    
    // create long press recognizer for radial menu
    longRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(viewLongPressed:)];
    longRecognizer.delegate = self;
    [self.view addGestureRecognizer:longRecognizer];

    /*
    pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(viewPinched:)];
    pinchRecognizer.delegate = self;
    pinchRecognizer.enabled = NO;
    [self.view addGestureRecognizer:pinchRecognizer];
    
    // create swipe left and right recognizer for finding matching kanji
    swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewSwipedLeft:)];
    swipeLeftRecognizer.delegate = self;
    swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeLeftRecognizer.enabled = NO;
    [self.view addGestureRecognizer:swipeLeftRecognizer];
    
    // swipe right
    swipeRightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(viewSwipedRight:)];
    swipeRightRecognizer.delegate = self;
    swipeRightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    swipeRightRecognizer.enabled = NO;
    [self.view addGestureRecognizer:swipeRightRecognizer];
    */
    
    // save dictionaries
    dictionary = [JapaneseDictionary getSingleton];
    kanjiDictionary = [KanjiDictionary getSingleton];
    
    /* don't do here, as may resize after, do in view will appear instead
    // show sentence and set transform
    [self showCurrentSentence];
    newFocusSentence.transform = [self getZoomOutTransform];
    
    // hide meaning to maximize space
    [self hideSentenceMeaning];
     */
    
    // create and hide magnifying image view
    magView = [[UIImageView alloc] initWithFrame:CGRectMake(200, 200, 250, 250)];
    magView.contentMode = UIViewContentModeScaleAspectFit;
    magView.hidden = YES;
    [self.view addSubview:magView];
    [self.view bringSubviewToFront:magView];
    
    // create mag overlay view
    magOverlayView = [[UIImageView alloc] initWithFrame:CGRectMake(200, 200, 250, 250)];
    magOverlayView.contentMode = UIViewContentModeScaleAspectFit;
    magOverlayView.hidden = YES;
    magOverlayView.image = [UIImage imageNamed:@"magnifier_500.png"];
    [self.view addSubview:magOverlayView];
    [self.view bringSubviewToFront:magOverlayView];
    
    // create mask
    CGImageRef maskRef = [[UIImage imageNamed:@"magnifier_mask_500.png"] CGImage];
    magMask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), nil, YES);
    
    /*
    // start music
    musicPlayer = [[AVAudioPlayer alloc] initWithData:[ResourceManager getBundleDataNamed:@"theowlnamedorion.mp3"] error:nil];
    musicPlayer.volume = 0.25f;
    musicPlayer.numberOfLoops = 99999;
    [musicPlayer play];
    */

    return;
}

-(void)viewLongPressed:(id)sender {
    return;
    // only magnify if not zoomed in
    if (!zoomedIn) {
        UILongPressGestureRecognizer *recognizer = (UILongPressGestureRecognizer*)sender;
        CGPoint tapPoint = [recognizer locationInView:playView];
        CGPoint tapPoint2 = [recognizer locationInView:self.view];
        
        if (recognizer.state != UIGestureRecognizerStateEnded) {
            // create image
            float zoom = 2.0f;
            float magSize = 75.0f;
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(magSize, magSize), playView.opaque, zoom);
            CGContextTranslateCTM(UIGraphicsGetCurrentContext(), -tapPoint.x+(magSize/2.0f), -tapPoint.y+(magSize/1.0f));
            [playView.layer renderInContext:UIGraphicsGetCurrentContext()];
            CGImageRef img = CGBitmapContextCreateImage(UIGraphicsGetCurrentContext());
//            UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            // mask image
            CGImageRef newCGImage = CGImageCreateWithMask(img, magMask);
            UIImage *newImage = [UIImage imageWithCGImage:newCGImage];
            CGImageRelease(newCGImage);
            
            // show mag view and set image
            CGPoint startPoint = CGPointMake(tapPoint2.x-(magView.frame.size.width/2.0f), tapPoint2.y-(magView.frame.size.height));
            if (startPoint.y < -(magView.frame.size.height*0.25f))
                startPoint.y = -(magView.frame.size.height*0.25f);
            if (startPoint.y+magView.frame.size.height > self.view.frame.size.height)
                startPoint.y = self.view.frame.size.height-magView.frame.size.height;
            magView.frame = CGRectOffset(magView.frame, startPoint.x-magView.frame.origin.x, startPoint.y-magView.frame.origin.y);
            magOverlayView.frame = magView.frame;
            magView.image = newImage;
            magView.hidden = NO;
            magOverlayView.hidden = NO;
            
            // release images
            CGImageRelease(img);
            
            // set in magnifying mode and remove this gesture recognizer
            isMagnifying = YES;
        }
        else {
            // hide magnifier
            magView.hidden = YES;
            magOverlayView.hidden = YES;
            isMagnifying = NO;
        }
    }
}

-(void)onKeyboardShow:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];

    // get keyboard frame
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    // move clear and hint buttons up
//    clearButton.frame = CGRectOffset(clearButton.frame, 0, -keyboardEndFrame.size.height+bottomToolbar.frame.size.height);
//    hintButton.frame = CGRectOffset(hintButton.frame, 0, -keyboardEndFrame.size.height+bottomToolbar.frame.size.height);
}

-(void)onKeyboardHide:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    
    // get keyboard frame
    CGRect keyboardEndFrame;
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    // move clear and hint buttons down
//    clearButton.frame = CGRectOffset(clearButton.frame, 0, keyboardEndFrame.size.height-bottomToolbar.frame.size.height);
//    hintButton.frame = CGRectOffset(hintButton.frame, 0, keyboardEndFrame.size.height-bottomToolbar.frame.size.height);
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    // don't responsd to tap on menu subview
    if ([touch.view isKindOfClass:[UIButton class]] || [touch.view.superview isKindOfClass:[UIToolbar class]]) {
        return NO;
    }
    return YES;
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    // release player
    [player release];
}

-(void)viewPinched:(id)sender {
    
}

-(BlasterWordSgo*)getWordTappedForRecognizer:(UIGestureRecognizer*)inRecognizer {
    // see if a word was tapped
    for (int i=0; i < newFocusSentence.subviews.count; i++) {
        UIView *thisView = [newFocusSentence.subviews objectAtIndex:i];
        if ([thisView isKindOfClass:[BlasterWordSgo class]]) {
            BlasterWordSgo *thisWord = (BlasterWordSgo*)thisView;
            CGPoint tapPoint = [inRecognizer locationInView:thisWord];
            if (tapPoint.x >= 0 && tapPoint.y >= 0 &&
                tapPoint.x < thisWord.frame.size.width &&
                tapPoint.y < thisWord.frame.size.height)
                return thisWord;
        }
    }
    
    // not tapped
    return nil;
}

-(BlasterKanjiSgo*)getKanjiTappedForRecognizer:(UIGestureRecognizer*)inRecognizer {
    // see if a kanji was tapped
    for (int i=0; i < newFocusSentence.subviews.count; i++) {
        UIView *thisView = [newFocusSentence.subviews objectAtIndex:i];
        if ([thisView isKindOfClass:[BlasterWordSgo class]]) {
            BlasterWordSgo *thisWord = (BlasterWordSgo*)thisView;
            CGPoint tapPoint = [inRecognizer locationInView:thisWord];
            if (tapPoint.x >= 0 && tapPoint.y >= 0 &&
                tapPoint.x < thisWord.frame.size.width &&
                tapPoint.y < thisWord.frame.size.height) {
                // if word was tapped, find out which kanji
                for (int k=0; k < thisWord.subviews.count; k++) {
                    UIView *thisSubView = [thisWord.subviews objectAtIndex:k];
                    if ([thisSubView isKindOfClass:[BlasterKanjiSgo class]]) {
                        BlasterKanjiSgo *thisKanji = (BlasterKanjiSgo*)thisSubView;
                        tapPoint = [inRecognizer locationInView:thisKanji];
                        if (tapPoint.x >= 0 && tapPoint.y >= 0 &&
                            tapPoint.x < thisKanji.frame.size.width &&
                            tapPoint.y < thisKanji.frame.size.height)
                            return thisKanji;
                    }
                }
            }
        }
    }
    
    // not tapped
    return nil;
}

-(void)viewDoubleTapped:(id)sender {
    // if this is a writing game
    if (game.gameType&blGameType_Writing && !zoomedIn && nextButton.enabled == YES) {
        BlasterKanjiSgo *thisKanji = [self getKanjiTappedForRecognizer:doubleTapRecognizer];
        if (thisKanji != nil) {
            // if this was one of the ones the user needed to complete
            if (thisKanji.kanji.toBeCompleted) {
                // set as focus kanji
                newFocusKanji = thisKanji;
                
                // enabledDrawing
                [writeKanjiController resetWithKanjiEntry:thisKanji.kanji.kanjiEntry andUserDrawing:thisKanji.drawingView.drawing];
                [writeKanjiController updateStrokesLeftLabel];
                [writeKanjiController changeStrokeWidthTo:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad?8:12)];
                [drawingView refreshDrawing];
                
                // show drawing interface and comparison without zooming
                [self showDrawingInterface:YES];
                
                /*
                // can't do anything during zooming
                isZooming = YES;
                
                // hide next button
                nextButton.enabled = NO;
                
                // hide help buttons
                readingHelpButton.hidden = YES;
                writingHelpButton.hidden = YES;
                
                // hide meaning for more room
                [self hideSentenceMeaning];
                
                // zoom in on this focus kanji
                [UIView animateWithDuration:0.5f animations:^{
                    // zoom in
                    newFocusSentence.transform = [self getZoomTransformForKanji:thisKanji];
                    
                    // while zooming in, fade out question label for kanji
                    thisKanji.questionLabel.alpha = 0.0f;
                } completion:^(BOOL finished) {
                    // hide existing views
                    thisKanji.questionLabel.hidden = YES;
                    thisKanji.drawingView.hidden = YES;
                    
                    // set to correct kanji entry and user drawing
                    [writeKanjiController resetWithKanjiEntry:thisKanji.kanji.kanjiEntry andUserDrawing:thisKanji.drawingView.drawing];
                    
                    // show comparison
                    [writeKanjiController showComparison];
                    
                    // now zoomed in
                    zoomedIn = YES;
                    isZooming = NO;
                    
                    // set focus to this kanji
                    newFocusKanji = thisKanji;
                }];
                 */
            }
        }
    }
    
    // show meaning of word and kanji's if user asks for help
    // see if a word was tapped
    BlasterWordSgo *thisWord = [self getWordTappedForRecognizer:doubleTapRecognizer];
    if (thisWord != nil) {
        // if word is marked completed
        if (thisWord.word.completed) {
            // this word is completed, so user must be asking for help
            // show kanji meanings and word meaning
            thisWord.meaningLabel.hidden = NO;
            [thisWord setKanjiHiddenStateTo:NO];
            
            // if this is a well known kanji (green) knock back a level
            // and show furigana too
            if ([TextFunctions containsKanji:thisWord.word.kanji])
                thisWord.kanaLabel.hidden = NO;
            if (thisWord.word.isWellKnown) {
                [thisWord.word decrementLeitnerDeckNumber];
            }
        }
    }
}

-(void)comparisonDrawingTouched:(id)sender {
    // zoom out and remove drawing stuff
    
    // show question label again if user didn't draw anything
    if (drawingView.drawing.strokes.count == 0)
        newFocusKanji.questionLabel.hidden = NO;
    else {
        //
        // hide comparison
        [writeKanjiController hideComparison];
        
        // show drawing again
        newFocusKanji.drawingView.hidden = NO;
        
        // hide extra controls
        drawingAnimationSlider.hidden = YES;
    }
    
    // hide drawing controls
    [self hideDrawingInterface:YES];
    
    // can't do anything during zooming state
    isZooming = YES;
    
    // play zoom out effect
    [self playSoundEffectNamed:@"zoom_out1.mp3" withVolume:0.75f];
    
    // zoom out
    [UIView animateWithDuration:0.5f animations:^{
        // zoom out
        newFocusSentence.transform = [self getZoomOutTransform];
        
        // if this is a reading game
        if (game.gameType&blGameType_Reading) {
            // fade other words back in
            for (int o=0; o < newFocusSentence.subviews.count; o++) {
                UIView *otherView = [newFocusSentence.subviews objectAtIndex:o];
                otherView.alpha = 1.0f;
            }
        }
        else {
            // while zooming out, fade back in question label if user didn't draw anything
            if (newFocusKanji.drawingView.drawing.strokes.count == 0)
                newFocusKanji.questionLabel.alpha = 1.0f;
        }
    } completion:^(BOOL finished) {
        if (game.gameType&blGameType_Reading) {
            // delete invisible text field and remove keyboard
            [hiddenTextField resignFirstResponder];
            [hiddenTextField removeFromSuperview];
            [hiddenTextField release];
            hiddenTextField = nil;
        }
        else {
            // redraw drawing because stroke width looks odd after zoomed out
            newFocusKanji.drawingView.strokeWidth = defaultStrokeWidth;
            [newFocusKanji.drawingView refreshDrawing];
            
            // show meaning again
            if (![KrLocalOptions getSingleton].hideTranslation)
                [self showSentenceMeaning];
            else
                // play show meaning
                [self playSoundEffectNamed:@"buttonchime02up.wav" withVolume:0.75f];
        }
        
        // show help buttons again
        if (game.gameType&blGameType_Writing)
            writingHelpButton.hidden = NO;
        else
            readingHelpButton.hidden = NO;
        
        // show check button again
        nextButton.enabled = YES;
        
        // now not zoomed in
        zoomedIn = NO;
        isZooming = NO;
    }];
}

-(void)viewTapped:(id)sender {
    // if we are in check mode
    if (nextButton.enabled == YES && game.gameType&blGameType_Writing) {
        // if we are showing the stroke checker, hide it
        if (!writingPopupView.hidden)
            [self hideDrawingInterface:YES];
    }
    else {
        // if we are drawing stroke, we can't process tap
        if (drawingView.isDrawing)
            return;
        
        // can't do anything if already zooming
        if (isZooming)
            return;
        
        // don't do anything for taps above the end of the sentence scroll view
        float endPos = sentenceScrollView.frame.origin.y + sentenceScrollView.frame.size.height;
        CGPoint tapPoint = [tapRecognizer locationInView:self.view];
        if (tapPoint.y > endPos)
            return;
        
        // if we're in reading mode and we tapped on a word, or writing and tapped on a kanji
        BOOL processed = NO;
        if (game.gameType&blGameType_Reading)
            processed = [self viewTappedDuringReadingGame:sender];
        else
            processed = [self viewTappedDuringWritingGame:sender];
        
        // if not processed and zoomed in, zoom out
        if (!processed && zoomedIn) {
            // if this is a writing game
            if (game.gameType&blGameType_Writing) {
                /*
                // push DOWN writing view
                [UIView animateWithDuration:0.5f animations:^{
                    writingPopupView.frame = CGRectMake(0, self.view.frame.size.height, writingPopupView.frame.size.width, writingPopupView.frame.size.height);
                } completion:^(BOOL finished) {
                    writingPopupView.hidden = YES;
                }];
                */
                
                // show question label again if user didn't draw anything
                if (drawingView.drawing.strokes.count == 0)
                    newFocusKanji.questionLabel.hidden = NO;
                else {
                    // hide question mark label now something is shown
                    newFocusKanji.questionLabel.hidden = YES;
                    
                    // set kanji drawing to drawing view drawing
                    KanjiDrawing *newDrawing = [[KanjiDrawing alloc] init];
                    newFocusKanji.drawingView.drawing = newDrawing;
                    [newDrawing release];
                    [newFocusKanji.drawingView clearStrokes];
                    [newFocusKanji.drawingView.drawing.strokes addObjectsFromArray:[[drawingView.drawing.strokes copy] autorelease]];
                    [newFocusKanji.drawingView refreshDrawing];
                    newFocusKanji.drawingView.hidden = NO;
    //                drawingView.hidden = YES;
                }
                
                // hide drawing controls
                [self hideDrawingInterface:YES];
            }
            /* don't need to do this now it's a popup
            else {
                // hide help and clear button
                hintButton.hidden = YES;
                clearButton.hidden = YES;
            }*/
            
            // can't do anything during zooming state
            isZooming = YES;
            
            // play zoom out effect
            [self playSoundEffectNamed:@"zoom_out1.mp3" withVolume:0.75f];
            
            if (game.gameType&blGameType_Reading) {
                // delete invisible text field and remove keyboard
                [hiddenTextField resignFirstResponder];
                [hiddenTextField removeFromSuperview];
                [hiddenTextField release];
                hiddenTextField = nil;
            }
            
            // zoom out
            [UIView animateWithDuration:0.5f animations:^{
                // zoom out
                newFocusSentence.transform = [self getZoomOutTransform];
                
                // if this is a reading game
                if (game.gameType&blGameType_Reading) {
                    // fade other words back in
                    for (int o=0; o < newFocusSentence.subviews.count; o++) {
                        UIView *otherView = [newFocusSentence.subviews objectAtIndex:o];
                        otherView.alpha = 1.0f;
                    }
                }
                else {
                    // fade other words back in
                    for (int o=0; o < newFocusSentence.subviews.count; o++) {
                        UIView *otherView = [newFocusSentence.subviews objectAtIndex:o];
                        otherView.alpha = 1.0f;
                        
                        // fade kanji back in
                        for (int k=0; k < otherView.subviews.count; k++) {
                            UIView *otherSubView = [otherView.subviews objectAtIndex:k];
                            if ([otherSubView isKindOfClass:[BlasterKanjiSgo class]])
                                otherSubView.alpha = 1.0f;
                        }
                    }
                    
                    // while zooming out, fade back in question label if user didn't draw anything
                    if (newFocusKanji.drawingView.drawing.strokes.count == 0)
                        newFocusKanji.questionLabel.alpha = 1.0f;
                }
            } completion:^(BOOL finished) {
                if (game.gameType&blGameType_Reading) {
                    /*
                    // delete invisible text field and remove keyboard
                    [hiddenTextField resignFirstResponder];
                    [hiddenTextField removeFromSuperview];
                    [hiddenTextField release];
                    hiddenTextField = nil;*/
                    
                    // no longer focus
                    newFocusWord = nil;
                }
                else {
                    // redraw drawing because stroke width looks odd after zoomed out
                    newFocusKanji.drawingView.strokeWidth = defaultStrokeWidth;
                    [newFocusKanji.drawingView refreshDrawing];
                    
                    // no longer focus
                    newFocusKanji = nil;
                }
                
                // show check button again
                checkButton.enabled = YES;
                
                // show help buttons again
                if (game.gameType&blGameType_Writing)
                    writingHelpButton.hidden = NO;
                else
                    readingHelpButton.hidden = NO;
                
                // now not zoomed in
                zoomedIn = NO;
                isZooming = NO;
            }];
        }
        /*
        else {
            // if this is a reading game
            if (game.gameType&blGameType_Reading) {
                // check for zoom on word
                [self viewTappedDuringReadingGame:sender];
            }
            else {
                // check for zoom on kanji
                [self viewTappedDuringWritingGame:sender];
            }
        }*/
    }
    
    return;
}

-(BOOL)viewTappedDuringReadingGame:(id)sender {
    // see if a word was tapped
    BlasterWordSgo *thisWord = [self getWordTappedForRecognizer:tapRecognizer];
    if (thisWord != nil && thisWord != newFocusWord) {
        // if word is not yet completed
        if (!thisWord.word.completed) {
            // zoom
            [self zoomToWord:thisWord];
           
            // processed
            return YES;
        }
    }
    
    return NO;
}

-(BOOL)viewTappedDuringWritingGame:(id)sender {
    // see if a kanji was tapped
    BlasterKanjiSgo *thisKanji = [self getKanjiTappedForRecognizer:tapRecognizer];
    if (thisKanji != nil && thisKanji != newFocusKanji) {
        // kanji was tapped, zoom if not completed
        if (!thisKanji.kanji.completed) {
            // zoom
            [self zoomToKanji:thisKanji];
            
            // processed
            return YES;
        }
    }

    // not processed
    return NO;
}

-(IBAction)nextKanjiButtonPressed:(id)sender {
    // if we're showing meaning then we must be trying to close the playback view
//    if (sentencePartScreen.priority > 500.0f) {
    // this doesn't work anymore.  use next button to see if we're in check mode
    if (nextButton.enabled == YES) {
        if (game.gameType&blGameType_Writing) {
            // actually try and SHOW the animation of the next kanji instead of just hiding
            BlasterKanjiSgo *thisKanji = [self getNextFocusKanji];
            if (thisKanji != nil) {
                // set as focus kanji
                newFocusKanji = thisKanji;
                
                // enabledDrawing
                [writeKanjiController resetWithKanjiEntry:thisKanji.kanji.kanjiEntry andUserDrawing:thisKanji.drawingView.drawing];
                [writeKanjiController updateStrokesLeftLabel];
                [writeKanjiController changeStrokeWidthTo:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad?8:12)];
                [drawingView refreshDrawing];
                
                // show drawing interface and comparison without zooming
                [self showDrawingInterface:YES];
            }
            else {
                [self hideDrawingInterface:YES];
            }
        }
    }
    else
        // move to kanji
        [self moveToNextKanji];
}

-(IBAction)previousKanjiButtonPressed:(id)sender {
    // if we're showing meaning then we must be trying to close the playback view
//    if (sentencePartScreen.priority > 500.0f) {
    // this doesn't work anymore.  use next button to see if we're in check mode
    if (nextButton.enabled == YES) {
        if (game.gameType&blGameType_Writing) {
            // actually try and SHOW the animation of the next kanji instead of just hiding
            BlasterKanjiSgo *thisKanji = [self getPreviousFocusKanji];
            if (thisKanji != nil) {
                // set as focus kanji
                newFocusKanji = thisKanji;
                
                // enabledDrawing
                [writeKanjiController resetWithKanjiEntry:thisKanji.kanji.kanjiEntry andUserDrawing:thisKanji.drawingView.drawing];
                [writeKanjiController updateStrokesLeftLabel];
                [writeKanjiController changeStrokeWidthTo:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad?8:12)];
                [drawingView refreshDrawing];
                
                // show drawing interface and comparison without zooming
                [self showDrawingInterface:YES];
            }
            else {
                [self hideDrawingInterface:YES];
            }
        }
    }
    else
        // move to kanji
        [self moveToPreviousKanji];
}
        
-(BlasterKanjiSgo*)getNextFocusKanji {
    // must already be zoomed in, so just move to next kanji
    BOOL foundFocusKanji = NO;
    for (int i=0; i < newFocusSentence.subviews.count; i++) {
        // if this is a word
        if ([[newFocusSentence.subviews objectAtIndex:i] isKindOfClass:[BlasterWordSgo class]]) {
            // if needs completing
            BlasterWordSgo *thisWord = [newFocusSentence.subviews objectAtIndex:i];
            if (thisWord.word.toBeCompleted) {
                // loop through kanjis
                for (int k=0; k < thisWord.subviews.count; k++) {
                    UIView *thisSubView = [thisWord.subviews objectAtIndex:k];
                    if ([thisSubView isKindOfClass:[BlasterKanjiSgo class]]) {
                        BlasterKanjiSgo *thisKanji = (BlasterKanjiSgo*)thisSubView;
                        // if this is the focus kanji
                        if (!foundFocusKanji) {
                            if (thisKanji == newFocusKanji)
                                foundFocusKanji = YES;
                        }
                        else {
                            // does this kanji need completing?
                            if (!thisKanji.kanji.completed) {
                                return thisKanji;
                            }
                        }
                    }
                }
            }
        }
    }
    
    return nil;
}
        
-(BlasterKanjiSgo*)getPreviousFocusKanji {
    // must already be zoomed in, so just move to next kanji
    BOOL foundFocusKanji = NO;
    for (int i=(int)newFocusSentence.subviews.count-1; i >= 0; i--) {
        // if this is a word
        if ([[newFocusSentence.subviews objectAtIndex:i] isKindOfClass:[BlasterWordSgo class]]) {
            // if needs completing
            BlasterWordSgo *thisWord = [newFocusSentence.subviews objectAtIndex:i];
            if (thisWord.word.toBeCompleted) {
                // loop through kanjis
                for (int k=(int)thisWord.subviews.count-1; k >= 0; k--) {
                    UIView *thisSubView = [thisWord.subviews objectAtIndex:k];
                    if ([thisSubView isKindOfClass:[BlasterKanjiSgo class]]) {
                        BlasterKanjiSgo *thisKanji = (BlasterKanjiSgo*)thisSubView;
                        // if this is the focus kanji
                        if (!foundFocusKanji) {
                            if (thisKanji == newFocusKanji)
                                foundFocusKanji = YES;
                        }
                        else {
                            // does this kanji need completing?
                            if (!thisKanji.kanji.completed) {
                                // zoom in on kanji
                                return thisKanji;
                            }
                        }
                    }
                }
            }
        }
    }
    
    return nil;
}
        
-(BOOL)moveToNextKanji {
    BlasterKanjiSgo *nextKanji = [self getNextFocusKanji];
    if (nextKanji != nil) {
        [self zoomToKanji:nextKanji];
        return YES;
    }
    else {
        // if we haven't found anything, zoom out
        [self viewTapped:self];
        return NO;
    }
    
    /*
    // must already be zoomed in, so just move to next kanji
    BOOL foundFocusKanji = NO;
    for (int i=0; i < newFocusSentence.subviews.count; i++) {
        // if this is a word
        if ([[newFocusSentence.subviews objectAtIndex:i] isKindOfClass:[BlasterWordSgo class]]) {
            // if needs completing
            BlasterWordSgo *thisWord = [newFocusSentence.subviews objectAtIndex:i];
            if (thisWord.word.toBeCompleted) {
                // loop through kanjis
                for (int k=0; k < thisWord.subviews.count; k++) {
                    UIView *thisSubView = [thisWord.subviews objectAtIndex:k];
                    if ([thisSubView isKindOfClass:[BlasterKanjiSgo class]]) {
                        BlasterKanjiSgo *thisKanji = (BlasterKanjiSgo*)thisSubView;
                        // if this is the focus kanji
                        if (!foundFocusKanji) {
                            if (thisKanji == newFocusKanji)
                                foundFocusKanji = YES;
                        }
                        else {
                            // does this kanji need completing?
                            if (!thisKanji.kanji.completed) {
                                // zoom in on kanji
                                [self zoomToKanji:thisKanji];
                                
                                return YES;
                            }
                        }
                    }
                }
            }
        }
    
        // if this is the last word, and we still haven't found it, zoom out
//        if (i == newFocusSentence.subviews.count-1)
//            [self viewTapped:self];
    }
    */
    
    return NO;
}

-(BOOL)moveToPreviousKanji {
    BlasterKanjiSgo *previousKanji = [self getPreviousFocusKanji];
    if (previousKanji != nil) {
        [self zoomToKanji:previousKanji];
        return YES;
    }
    else {
        // if we haven't found anything, zoom out
        [self viewTapped:self];
        return NO;
    }
    
    /*
    // must already be zoomed in, so just move to next kanji
    BOOL foundFocusKanji = NO;
    for (int i=(int)newFocusSentence.subviews.count-1; i >= 0; i--) {
        // if this is a word
        if ([[newFocusSentence.subviews objectAtIndex:i] isKindOfClass:[BlasterWordSgo class]]) {
            // if needs completing
            BlasterWordSgo *thisWord = [newFocusSentence.subviews objectAtIndex:i];
            if (thisWord.word.toBeCompleted) {
                // loop through kanjis
                for (int k=(int)thisWord.subviews.count-1; k >= 0; k--) {
                    UIView *thisSubView = [thisWord.subviews objectAtIndex:k];
                    if ([thisSubView isKindOfClass:[BlasterKanjiSgo class]]) {
                        BlasterKanjiSgo *thisKanji = (BlasterKanjiSgo*)thisSubView;
                        // if this is the focus kanji
                        if (!foundFocusKanji) {
                            if (thisKanji == newFocusKanji)
                                foundFocusKanji = YES;
                        }
                        else {
                            // does this kanji need completing?
                            if (!thisKanji.kanji.completed) {
                                // zoom in on kanji
                                [self zoomToKanji:thisKanji];
                                
                                return YES;
                            }
                        }
                    }
                }
            }
        }
        
        // if this is the last word, and we still haven't found it, zoom out
//        if (i == newFocusSentence.subviews.count-1)
//            [self viewTapped:self];
    }
    
    // if we didn't find anything, zoom out
    [self viewTapped:self];
    */
    
    return NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    // override next button to go to next word
    
    // must already be zoomed in, so just move to next word
    BOOL foundFocusWord = NO;
    for (int i=0; i < newFocusSentence.subviews.count; i++) {
        // find focus word first
        if (!foundFocusWord) {
            if ([newFocusSentence.subviews objectAtIndex:i] == newFocusWord)
                foundFocusWord = YES;
        }
        else {
            // if this is a word that needs completing, this must be the next word
            if ([[newFocusSentence.subviews objectAtIndex:i] isKindOfClass:[BlasterWordSgo class]]) {
                // if needs completing
                BlasterWordSgo *thisWord = [newFocusSentence.subviews objectAtIndex:i];
                if (thisWord.word.toBeCompleted) {
                    // zoom
                    [self zoomToWord:thisWord];
                    
                    /*
                    // play zoom in effect
                    [self playSoundEffectNamed:@"zoom_in1.mp3" withVolume:0.75f];
                    
                    // zoom in on this word
                    [UIView animateWithDuration:0.5f animations:^{
                        // change transform
                        newFocusSentence.transform = [self getZoomTransformForWord:thisWord];
                        
                        // fade out other words and kanji in sentence
                        for (int o=0; o < newFocusSentence.subviews.count; o++) {
                            UIView *otherView = [newFocusSentence.subviews objectAtIndex:o];
                            if (otherView != thisWord)
                                otherView.alpha = 0.3f;
                            else
                                otherView.alpha = 1.0f;
                        }
                    }];
                    
                    // set as focus word now
                    newFocusWord = thisWord;
                    
                    // set hidden text field to same value as new word furigana
                    hiddenTextField.text = [TextFunctions hiraganaToRomaji:newFocusWord.word.userFurigana];
                    */
                    
                    return YES;
                }
            }
        }
        
        // if this is the last word, and we still haven't found it, zoom out
        if (i == newFocusSentence.subviews.count-1)
            [self viewTapped:self];
    }
    
    return NO;
}

-(void)zoomToKanji:(BlasterKanjiSgo*)thisKanji {
    // can't do anything during zooming
    isZooming = YES;
    
    // if we're already drawing a kanji, update kanji's drawing
    if (!writingPopupView.hidden) {
        // show question label again if user didn't draw anything
        if (drawingView.drawing.strokes.count == 0)
            newFocusKanji.questionLabel.hidden = NO;
        else {
            // hide question mark label now something is shown
            newFocusKanji.questionLabel.hidden = YES;
            
            // set kanji drawing to drawing view drawing
            KanjiDrawing *newDrawing = [[KanjiDrawing alloc] init];
            newFocusKanji.drawingView.drawing = newDrawing;
            [newDrawing release];
            [newFocusKanji.drawingView clearStrokes];
            [newFocusKanji.drawingView.drawing.strokes addObjectsFromArray:[[drawingView.drawing.strokes copy] autorelease]];
            [newFocusKanji.drawingView refreshDrawing];
            newFocusKanji.drawingView.hidden = NO;
        }
    }
    
    /*
    // popup writing view if not already shown
    if (writingPopupView.hidden) {
        writingPopupView.frame = CGRectMake(0, self.view.frame.size.height, writingPopupView.frame.size.width, writingPopupView.frame.size.height);
        writingPopupView.hidden = NO;
        [UIView animateWithDuration:0.5f animations:^{
            writingPopupView.frame = CGRectMake(0, self.view.frame.size.height-writingPopupView.frame.size.height, writingPopupView.frame.size.width, writingPopupView.frame.size.height);
        } completion:^(BOOL finished) {
        }];
    }*/
    
    // hide help buttons
//    writingHelpButton.hidden = YES;
//    readingHelpButton.hidden = YES;
    
    // play zoom in effect
    [self playSoundEffectNamed:@"zoom_in1.mp3" withVolume:0.75f];
    
    // update drawing popup with this kanji's drawing
    [writeKanjiController resetWithKanjiEntry:thisKanji.kanji.kanjiEntry andUserDrawing:thisKanji.drawingView.drawing];
    [writeKanjiController updateStrokesLeftLabel];
    [writeKanjiController changeStrokeWidthTo:(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad?8:12)];
    [drawingView enableDrawing];
    [drawingView refreshDrawing];
    
    // show drawing interface
    [self showDrawingInterface:NO];
    
    // zoom in on this focus kanji
    [UIView animateWithDuration:0.5f animations:^{
        // zoom in
        NSLog(@"transform=%f,%f,%f,%f,%f,%f",
              newFocusSentence.transform.a,
              newFocusSentence.transform.b,
              newFocusSentence.transform.c,
              newFocusSentence.transform.d,
              newFocusSentence.transform.tx,
              newFocusSentence.transform.ty
              );
        newFocusSentence.transform = [self getZoomTransformForKanji:thisKanji];
        NSLog(@"after transform=%f,%f,%f,%f,%f,%f",
              newFocusSentence.transform.a,
              newFocusSentence.transform.b,
              newFocusSentence.transform.c,
              newFocusSentence.transform.d,
              newFocusSentence.transform.tx,
              newFocusSentence.transform.ty
              );
        
        // fade out other words and kanji in sentence
        for (int o=0; o < newFocusSentence.subviews.count; o++) {
            UIView *otherView = [newFocusSentence.subviews objectAtIndex:o];
            if (otherView != thisKanji.superview) {
                otherView.alpha = 0.3f;
                
                // set kanji alpha back to 1.0, so we don't get fade within fade
                for (int k=0; k < otherView.subviews.count; k++) {
                    UIView *otherSubView = [otherView.subviews objectAtIndex:k];
                    if ([otherSubView isKindOfClass:[BlasterKanjiSgo class]])
                        otherSubView.alpha = 1.0f;
                }
            }
            else {
                otherView.alpha = 1.0f;
                
                // fade out kanji other than the one we're looking at
                for (int k=0; k < otherView.subviews.count; k++) {
                    UIView *otherSubView = [otherView.subviews objectAtIndex:k];
                    if ([otherSubView isKindOfClass:[BlasterKanjiSgo class]]) {
                        if (otherSubView != thisKanji)
                            otherSubView.alpha = 0.3f;
                        else
                            otherSubView.alpha = 1.0f;
                    }
                }
            }
        }
        
        // while zooming in, fade out question label for kanji
        //                thisKanji.questionLabel.alpha = 0.0f;
    } completion:^(BOOL finished) {
        // hide question label
        //                thisKanji.questionLabel.hidden = YES;
        //                thisKanji.drawingView.hidden = YES;
        
        // now zoomed in
        zoomedIn = YES;
        isZooming = NO;
    }];
    
    // always hide check sentence button
    checkButton.enabled = NO;
    
    // set this as focus word
    newFocusKanji = thisKanji;
}

-(void)zoomToWord:(BlasterWordSgo*)thisWord {
    // always hide check sentence button
    checkButton.enabled = NO;
    
    // hide help buttons
    writingHelpButton.hidden = YES;
    readingHelpButton.hidden = YES;
    
    // can't do anything during zooming
    isZooming = YES;
    
    // play zoom in effect
    [self playSoundEffectNamed:@"zoom_in1.mp3" withVolume:0.75f];
    
    // create invisible text field to bring up keyboard if not already shown
    if (hiddenTextField == nil) {
        hiddenTextField = [[UITextField alloc] init];
        hiddenTextField.hidden = YES;
        hiddenTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        hiddenTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        hiddenTextField.returnKeyType = UIReturnKeyNext;
        hiddenTextField.delegate = self;
        myInputAccessoryView.hidden = NO;
        
        // create input accessory view here manually
        UIToolbar *keyboardAccessory = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        UIBarButtonItem *undoButton = [[UIBarButtonItem alloc] initWithTitle:@"Undo" style:UIBarButtonItemStylePlain target:self action:@selector(clearButtonPressed:)];
        UIBarButtonItem *spacerButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *hintButton = [[UIBarButtonItem alloc] initWithTitle:@"Hint" style:UIBarButtonItemStylePlain target:self action:@selector(hintButtonPressed:)];
        [keyboardAccessory setItems:[NSArray arrayWithObjects:undoButton, spacerButton, hintButton, nil]];
        [undoButton release];
//        [myInputAccessoryView removeFromSuperview];
        hiddenTextField.inputAccessoryView = keyboardAccessory;
        [keyboardAccessory release];
//        hiddenTextField.inputAccessoryView = myInputAccessoryView;
        [self.view addSubview:hiddenTextField];
        [hiddenTextField becomeFirstResponder];
    }
    
    // zoom in on this focus word
    [UIView animateWithDuration:0.5f animations:^{
        // zoom in
        newFocusSentence.transform = [self getZoomTransformForWord:thisWord];
        
        // fade out other words and kanji in sentence
        for (int o=0; o < newFocusSentence.subviews.count; o++) {
            UIView *otherView = [newFocusSentence.subviews objectAtIndex:o];
            if (otherView != thisWord)
                otherView.alpha = 0.3f;
            else
                otherView.alpha = 1.0f;
        }
    } completion:^(BOOL finished) {
        // reset kana label back to '?'
        thisWord.kanaLabel.text = @"?";
        
        // set hidden text field to same value as new word furigana
        hiddenTextField.text = @"";
//        hiddenTextField.text = [TextFunctions hiraganaToRomaji:newFocusWord.word.userFurigana];
        
        // capture hidden field changes
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(hiddenTextFieldChange:)
         name:UITextFieldTextDidChangeNotification
         object:hiddenTextField];
        
        // now zoomed in
        zoomedIn = YES;
        isZooming = NO;
        
        // show help button
        hintButton.hidden = NO;
        clearButton.hidden = NO;
    }];
    
    // set this as focus word
    newFocusWord = thisWord;
}

-(void)hideSentenceMeaning {
    /*
    // save old frames
    oldScrollViewFrame = sentenceScrollView.frame;
    oldCheckButtonFrame = checkButton.frame;
    
    // resize the sentence scrollview to maximize space
    sentenceScrollView.frame = CGRectMake(sentenceScrollView.frame.origin.x, sentenceScrollView.frame.origin.y, sentenceScrollView.frame.size.width, meaningScrollView.frame.origin.y+meaningScrollView.frame.size.height-sentenceScrollView.frame.origin.y);
    
    // reposition the check meaning button
    checkButton.frame = nextButton.frame;
    */

/* don't do this now with auto-layout
    // resize the sentence scrollview to maximize space
    sentenceScrollView.frame = CGRectMake(sentenceScrollView.frame.origin.x, sentenceScrollView.frame.origin.y, sentenceScrollView.frame.size.width, meaningScrollView.frame.origin.y+meaningScrollView.frame.size.height-sentenceScrollView.frame.origin.y);
*/
    
//    sentenceScrollView.backgroundColor = [UIColor yellowColor];
    
    // set priority on auto-layout to make sentence fill
    meaningHeightConstraint.active = NO;
    meaningFixedHeightConstraint.active = YES;

//    sentencePartScreen.priority = 100;
//    sentenceFullScreen.priority = 900;

    // layout to make meaning label change size
    [sentenceScrollView layoutIfNeeded];
    [meaningScrollView layoutIfNeeded];
    
    /*
    for (NSLayoutConstraint *constraint in sentenceScrollView.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            if (constraint.multiplier == 1.0f)
                constraint.priority = 900;
            else
                constraint.priority = 100;
        }
    }
    [sentenceScrollView layoutIfNeeded];
    */
    
//    meaningScrollView.hidden = YES;
}

-(void)showSentenceMeaning {
    /*
    // size the sentence scrollview and check button back again
    sentenceScrollView.frame = oldScrollViewFrame;
    checkButton.frame = oldCheckButtonFrame;
    */
    
    // play show meaning
    [self playSoundEffectNamed:@"buttonchime02up.wav" withVolume:0.75f];
    
    /*
    // find gap height between sentence and meaning
    float gapHeight = (newFocusSentence.sentenceLineHeight*0.25f*defaultZoom);
    
    // pre-calc height to make sure meaning is at least a certain height
    float newSentenceHeight = newFocusSentence.frame.size.height+gapHeight;
    float newMeaningStartPos = sentenceScrollView.frame.origin.y+newSentenceHeight;
    float newMeaningHeight = meaningScrollView.frame.origin.y+meaningScrollView.frame.size.height-newMeaningStartPos;
    CGRect largeFrame = CGRectMake(0, 0, meaningScrollView.frame.size.width, 600.0f);
    float textHeight = [sentenceMeaningLabel.text sizeWithFont:sentenceMeaningLabel.font constrainedToSize:largeFrame.size lineBreakMode:NSLineBreakByWordWrapping].height;
    
    // calculate offset to make sure we see ALL meaning text
    float extraSpaceNeeded = 0.0f;
    if (newMeaningHeight < textHeight)
        extraSpaceNeeded = textHeight-newMeaningHeight;
    
    // resize the sentence scrollview to size of sentence
    sentenceScrollView.frame = CGRectMake(sentenceScrollView.frame.origin.x, sentenceScrollView.frame.origin.y, sentenceScrollView.frame.size.width, newFocusSentence.frame.size.height-extraSpaceNeeded);
    
    // resize the meaning label to use the rest of the space
    float startPos = sentenceScrollView.frame.origin.y+sentenceScrollView.frame.size.height+gapHeight;
    meaningScrollView.frame = CGRectMake(meaningScrollView.frame.origin.x, startPos, meaningScrollView.frame.size.width, meaningScrollView.frame.origin.y+meaningScrollView.frame.size.height-startPos);
    
    // find text height
    NSLog(@"Text is %@, height is %f", sentenceMeaningLabel.text, textHeight);
    sentenceMeaningLabel.frame = CGRectMake(0, 0, sentenceMeaningLabel.frame.size.width, textHeight);
    */
    
    // set priority on auto-layout to make meaning show
    meaningFixedHeightConstraint.active = NO;
    meaningHeightConstraint.active = YES;
//    sentencePartScreen.priority = 900;
//    sentenceFullScreen.priority = 100;
    
    // set content size again to constrain label
//    meaningScrollView.contentSize = CGSizeMake(sentenceScrollView.frame.size.width, 9999.0f);

    // layout to make meaning label change size
    [sentenceScrollView layoutIfNeeded];
    [meaningScrollView layoutIfNeeded];
    
    
    // show the sentence meaning again
//    meaningScrollView.hidden = NO;
}

-(void)hideDrawingInterface:(BOOL)animate {
    // animation looks horrible
    animate = NO;
    // push DOWN writing view
    // do this first, layout animates
    writingControlsHeightConstraint.constant = 0.0f;
    if (animate) {
        [UIView animateWithDuration:0.5f animations:^{
            [writingPopupView layoutIfNeeded];
        } completion:^(BOOL finished) {
            writingPopupView.hidden = YES;
        }];
    }
    else {
        [writingPopupView layoutIfNeeded];
        writingPopupView.hidden = YES;
    }
        
    /*
    // hide drawing stuff
    drawingView.hidden = YES;
    outlineView.hidden = YES;
    buildView.hidden = YES;
    animatedView.hidden = YES;
    clearButton.hidden = YES;
    hintButton.hidden = YES;
    strokesLabel.hidden = YES;*/
}

-(void)showDrawingInterface:(BOOL)showComparison {
    // animation looks horrible
    BOOL animate = NO;
    
    // always hide comparison, otherwise it's shown again when the user moves to the next question
    // and tries to draw.  showdrawing interface turns on comparison where needed anway
    [writeKanjiController hideComparison];

    // popup writing view if not already shown
    if (writingPopupView.hidden) {
        // if showing comparison, hide buttons
        if (showComparison) {
            clearButton.hidden = YES;
            hintButton.hidden = YES;
            strokeCountHeightConstraint.constant = 0.0f;
            animationSliderHeightConstraint.constant = 34.0f;
            writingButtonsHeightConstraint.constant = 0.0f;
//            drawingAnimationSlider.hidden = NO;
            drawingView.hidden = YES;
        }
        else {
            clearButton.hidden = NO;
            hintButton.hidden = NO;
            strokeCountHeightConstraint.constant = 20.0f;
            animationSliderHeightConstraint.constant = 0.0f;
            writingButtonsHeightConstraint.constant = 34.0f;
//            drawingAnimationSlider.hidden = YES;
            drawingView.hidden = NO;
        }
        
        // show comparison if requested
//        if (showComparison)
//            [writeKanjiController showComparison];
        // show popup
    
        // pull up writing view
        // set height first, then show comparison otherwise frame heights are zero!
        writingPopupView.hidden = NO;
        writingControlsHeightConstraint.constant = playView.frame.size.height/2.6f;
        if (animate) {
            [UIView animateWithDuration:0.5f animations:^{
                [writingPopupView layoutIfNeeded];
            } completion:^(BOOL finished) {
                // show comparison if requested
                if (showComparison)
                    [writeKanjiController showComparison];
            }];
        }
        else {
            [writingPopupView layoutIfNeeded];
            // show comparison if requested
            if (showComparison)
                [writeKanjiController showComparison];
        }
        
        /*
        writingPopupView.frame = CGRectMake(0, self.view.frame.size.height, writingPopupView.frame.size.width, writingPopupView.frame.size.height);
        writingPopupView.hidden = NO;
        [UIView animateWithDuration:0.5f animations:^{
            if (@available(iOS 11, *)) {
                writingPopupView.frame = CGRectMake(0, (self.view.safeAreaLayoutGuide.layoutFrame.size.height+self.view.safeAreaLayoutGuide.layoutFrame.origin.y)-writingPopupView.frame.size.height, writingPopupView.frame.size.width, writingPopupView.frame.size.height);
            }
            else {
                writingPopupView.frame = CGRectMake(0, self.view.frame.size.height-writingPopupView.frame.size.height, writingPopupView.frame.size.width, writingPopupView.frame.size.height);
            }
        } completion:^(BOOL finished) {
            // show comparison if requested
            if (showComparison)
                [writeKanjiController showComparison];
        }];
         */
    }
    else {
        if (showComparison)
            [writeKanjiController showComparison];
    }
    
    /*
    // show drwaing stuff
    drawingView.hidden = NO;
    outlineView.hidden = NO;
    buildView.hidden = NO;
    animatedView.hidden = NO;
    clearButton.hidden = NO;
    hintButton.hidden = NO;
    strokesLabel.hidden = YES;*/
}

-(void)hiddenTextFieldChange:(id)sender {
    // set text in current word meaning to the same
    newFocusWord.word.userFurigana = [[JapaneseDictionary getSingleton] convertToHiragana:hiddenTextField.text];
    if ([newFocusWord.word.userFurigana isEqualToString:@""])
        newFocusWord.kanaLabel.text = @"?";
    else
        newFocusWord.kanaLabel.text = [[JapaneseDictionary getSingleton] convertToHiragana:hiddenTextField.text];
}

-(void)playSoundEffectNamed:(NSString*)inEffectName withVolume:(float)inVolume {
    /*
    // see if system sound is loaded, otherwise load
    if ([systemSounds objectForKey:inEffectName] == nil) {
        // load new object and add to cache
        NSString *path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], inEffectName];
        CFURLRef thisUrl = CFURLCreateWithFileSystemPath(nil, (__bridge CFStringRef)path, kCFURLPOSIXPathStyle, false);
        SystemSoundID newSound;
        OSStatus result = AudioServicesCreateSystemSoundID(thisUrl, &newSound);
        CFRelease(thisUrl);
        
        // add sound to cache
        if (result == noErr) {
            // add to cache and play
            [systemSounds setObject:[NSNumber numberWithUnsignedInteger:newSound] forKey:inEffectName];
            AudioServicesPlaySystemSound(newSound);
        }
    }
    else {
        // get from cache and play
        NSNumber *thisNum = [systemSounds objectForKey:inEffectName];
        SystemSoundID thisSound = [thisNum unsignedIntegerValue];
        AudioServicesPlaySystemSound(thisSound);
    
        // add a callback
//        AudioServicesAddSystemSoundCompletion(thisSound, nil, nil, &MyAudioServicesSystemSoundCompletionProc, self);
    }
     */
    
    // load new sound
    AVAudioPlayer *effectsPlayer = [[AVAudioPlayer alloc] initWithData:[ResourceManager getBundleDataNamed:inEffectName] error:nil];
    effectsPlayer.delegate = self;
    [effectsPlayer prepareToPlay];
    effectsPlayer.volume = inVolume;
    [effectsPlayer play];
}

/*
void MyAudioServicesSystemSoundCompletionProc (SystemSoundID ssID, void *clientData) {
    // make callback to object as before
}*/

-(void)didCompleteKanji:(BlasterKanji *)inKanji {
}

-(void)didCompleteSentence:(BlasterSentence *)inSentence {
    // show sentence meaning
    sentenceMeaningLabel.text = inSentence.meaning;
}

-(void)checkNextWord {
    // get sentence sgo
    BlasterSentenceSgo *sgoSentence = nil;
    for (int i=0; i < sentenceScrollView.subviews.count; i++) {
        UIView *thisView = [sentenceScrollView.subviews objectAtIndex:i];
        if ([thisView isKindOfClass:[BlasterSentenceSgo class]]) {
            sgoSentence = (BlasterSentenceSgo*)thisView;
            break;
        }
    }
    
    // if this is a reading game
    BOOL waitFirst = FALSE;
    if (game.gameType&blGameType_Reading) {
        // make sure it's a word
        UIView *thisView = [sgoSentence.subviews objectAtIndex:checkWordIndex];
        if ([thisView isKindOfClass:[BlasterWordSgo class]]) {
            // if this a word that we needed to fill in?
            BlasterWordSgo *sgoWord = (BlasterWordSgo*)thisView;
            if (sgoWord.word.toBeCompleted) {
                // is the kana label the same as the word kana?
//                NSString *fullKana = [NSString stringWithFormat:@"%@%@", sgoWord.kanaLabel.text, sgoWord.word.conjugation];
                if ([sgoWord.kanaLabel.text isEqualToString:sgoWord.word.furigana]) {
//                    if ([fullKana isEqualToString:sgoWord.word.kana]) {
                    // show in green, or orange if you had to look
                    if (sgoWord.word.hadToLook)
                        sgoWord.kanaLabel.backgroundColor = [UIColor orangeColor];
                    else
                        sgoWord.kanaLabel.backgroundColor = [UIColor colorWithRed:0.0f green:200.0f/255.0f blue:0.0f alpha:1.0f]; // darker green
                    sgoWord.meaningLabel.hidden = NO;
                    
                    // set as completed to show kanji meaning too
                    [sgoWord setAsCompleted];
                    
                    // play correct sound
                    [self playSoundEffectNamed:@"multimedia_rollover_061.mp3" withVolume:0.75f];
                }
                else {
                    // show in red
//                    sgoWord.kanaLabel.backgroundColor = [UIColor redColor];
                    sgoWord.kanaLabel.backgroundColor = [UIColor colorWithRed:1.0f green:0.25f blue:0.25f alpha:1.0f]; // not so red
                    sgoWord.kanaLabel.text = sgoWord.word.furigana;
                    sgoWord.meaningLabel.hidden = NO;
                    
                    // set as completed to show kanji meaning too
                    [sgoWord setAsCompleted];
                    
                    // play incorrect sound
                    [self playSoundEffectNamed:@"multimedia_rollover_022.mp3" withVolume:0.75f];
                }
                
                // wait to show user
                waitFirst = YES;
            }
        }
    }
    else {
        // check kanji
        // make sure it's a word
        UIView *thisView = [sgoSentence.subviews objectAtIndex:checkWordIndex];
        if ([thisView isKindOfClass:[BlasterWordSgo class]]) {
            // check next kanji for the word
            BlasterWordSgo *sgoWord = (BlasterWordSgo*)thisView;
            UIView *thisSubView = [sgoWord.subviews objectAtIndex:checkKanjiIndex];
            if ([thisSubView isKindOfClass:[BlasterKanjiSgo class]]) {
                // if this kanji is to be completed
                BlasterKanjiSgo *thisKanji = (BlasterKanjiSgo*)thisSubView;
                if (thisKanji.kanji.toBeCompleted) {
                    // if the user didn't draw any strokes
                    if (thisKanji.drawingView.drawing.strokes.count == 0) {
                        // show real drawing in red
                        thisKanji.drawingView.drawing.strokes = [[thisKanji.kanji.kanjiEntry.drawing.strokes copy] autorelease];
                        [thisKanji.drawingView refreshDrawing];
                        thisKanji.drawingView.backgroundColor = [UIColor redColor];
                        // support dark mode in ios13
                        if (@available(iOS 13.0, *)) {
                            thisKanji.drawingView.drawColor = [UIColor labelColor];
                        } else {
                            // Fallback on earlier versions
                            thisKanji.drawingView.drawColor = [UIColor blackColor];
                        }
                        thisKanji.drawingView.shadowColor = [UIColor clearColor];
                        thisKanji.questionLabel.hidden = YES;
                        thisKanji.drawingView.hidden = NO;
                        
                        // show word and kanji meaning
                        sgoWord.meaningLabel.hidden = NO;
                        [sgoWord setKanjiHiddenStateTo:NO];
                        
                        // set score to zero
                        thisKanji.kanji.finalScore = 0.0f;
                    }
                    else {
                        // get score for drawing
                        float score = [thisKanji.drawingView.drawing compareWithDrawing:thisKanji.kanji.kanjiEntry.drawing];
                        thisKanji.kanji.finalScore = score;
                        if (score >= [[KrLocalOptions getSingleton] passAccuracy]) {
                            // show in green, but if had to look, show in orange
                            if (thisKanji.kanji.hadToLook)
                                thisKanji.drawingView.backgroundColor = [UIColor orangeColor];
                            else
                                thisKanji.drawingView.backgroundColor = [UIColor colorWithRed:0.0f green:200.0f/255.0f blue:0.0f alpha:1.0f]; // darker green
                            // support dark mode in ios13
                            if (@available(iOS 13.0, *)) {
                                thisKanji.drawingView.drawColor = [UIColor labelColor];
                            } else {
                                // Fallback on earlier versions
                                thisKanji.drawingView.drawColor = [UIColor blackColor];
                            }
                            thisKanji.drawingView.shadowColor = [UIColor clearColor];
                            [thisKanji.drawingView refreshDrawing];
                            
                            // show word and kanji meaning
                            sgoWord.meaningLabel.hidden = NO;
                            [sgoWord setKanjiHiddenStateTo:NO];
                            
                            // set as completed to show kanji meaning too
                            [thisKanji setAsCompleted];
                            
                            // play incorrect sound
                            [self playSoundEffectNamed:@"multimedia_rollover_061.mp3" withVolume:0.75f];
                        }
                        else {
                            // show in red
                            thisKanji.drawingView.backgroundColor = [UIColor redColor];
                            // support dark mode in ios13
                            if (@available(iOS 13.0, *)) {
                                thisKanji.drawingView.drawColor = [UIColor labelColor];
                            } else {
                                // Fallback on earlier versions
                                thisKanji.drawingView.drawColor = [UIColor blackColor];
                            }
                            thisKanji.drawingView.shadowColor = [UIColor clearColor];
                            [thisKanji.drawingView refreshDrawing];
                            sgoWord.meaningLabel.hidden = NO;
                            
                            // show kanji meaning too
                            [sgoWord setKanjiHiddenStateTo:NO];
                            
                            // set as completed to show kanji meaning too
                            [thisKanji setAsCompleted];
                            
                            // play incorrect sound
                            [self playSoundEffectNamed:@"multimedia_rollover_022.mp3" withVolume:0.75f];
                            
                            // zoom in and show incorrect drawing animation with users
                        }
                    }
                    
                    // wait to show user
                    waitFirst = YES;
                }
            }
        }
    }
    
    // if this is a kanji game, try and go to the next kanji
    BOOL goToNextWord = NO;
    if (game.gameType&blGameType_Writing) {
        // get current word
        UIView *thisView = [sgoSentence.subviews objectAtIndex:checkWordIndex];
        if (++checkKanjiIndex < thisView.subviews.count) {
            // check word/kanji
            if (waitFirst)
                [self performSelector:@selector(checkNextWord) withObject:self afterDelay:0.8f];
            else
                [self checkNextWord];
        }
        else {
            // go to next word
            checkKanjiIndex = 0;
            goToNextWord = YES;
        }
    }
    else {
        // go to next word
        checkKanjiIndex = 0;
        goToNextWord = YES;
    }
    
    // if we are going to next word
    if (goToNextWord) {
        // check next sub view
        if (++checkWordIndex < sgoSentence.subviews.count) {
            if (waitFirst)
                [self performSelector:@selector(checkNextWord) withObject:self afterDelay:0.8f];
            else
                [self checkNextWord];
        }
        else {
            // show meaning for sentence
            sentenceMeaningLabel.text = sgoSentence.sentence.meaning;
            
            // show meaning again (condense sentence)
            if (![KrLocalOptions getSingleton].hideTranslation)
                [self showSentenceMeaning];
            else
                // play show meaning sound anyway
                [self playSoundEffectNamed:@"buttonchime02up.wav" withVolume:0.75f];

            // re-enable double-tap in case you need clarification of word
            doubleTapRecognizer.enabled = YES;
            
            // hide check button, show next button
            checkButton.enabled = NO;
            nextButton.enabled = YES;
            
            // finished showing answers, so check card in game
            [game checkCardAnswers];
        }
    }
}

-(IBAction)checkAnswersButtonPressed:(id)sender {
    // disable button so we can't use it again
    checkButton.enabled = NO;
    
    // don't allow tapping
// leave tap enabled as a way to zoom out
//    tapRecognizer.enabled = NO;
    doubleTapRecognizer.enabled = NO;
    
    // go through and check furigana or kanji
    checkWordIndex = 0;
    checkKanjiIndex = 0;
    [self checkNextWord];
}

-(IBAction)nextSentenceButtonPressed:(id)sender {
    // hide the drawing area
    [self hideDrawingInterface:YES];
    
    // hide sentence meaning label again (expand sentence)
    [self hideSentenceMeaning];
    
    // go to next card
    [game goToNextCard];
    
    // update statistics
//    [self updateStats];
    
    // show current sentence
    [self showCurrentSentence];
    
    // re-center on this word
    CGAffineTransform newTransform = [self getZoomOutTransform];
    newFocusSentence.transform = newTransform;

    // hide next button, show check button
    nextButton.enabled = NO;
    checkButton.enabled = YES;
    
    // allow tapping again
// this is now always enabled
//    tapRecognizer.enabled = YES;
    doubleTapRecognizer.enabled = YES;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // call super first
    [super prepareForSegue:segue sender:sender];
    
    // is this is the play controller
    if ([[segue identifier] isEqualToString:@"ShowProgress"]) {
        // update stats
        ProgressViewController *progressController = [segue destinationViewController];
        [progressController setGame:game];
    }
}

-(void)didCompleteWord:(BlasterWord *)inWord {
    // update score
    
    // is sentence complete?
    
    
    return;
}

-(void)showCurrentSentence {
    // remove existing sentence
    if (newFocusSentence != nil) {
        [newFocusSentence removeFromSuperview];
    }
    
    // get the current sample sentence for this word
    BlasterSentence *thisSentence = [game getCurrentSentence];
    BlasterSentenceSgo *newSgo = [BlasterSentenceSgo BlasterSentenceSgoWithSentence:thisSentence usingWidth:sentenceScrollView.frame.size.width/defaultZoom];
    newSgo.translatesAutoresizingMaskIntoConstraints = NO;
//    newSgo.backgroundColor = [UIColor redColor];
    
//    BlasterSentenceSgo *newSgo = [BlasterSentenceSgo BlasterSentenceSgoWithSentence:thisSentence usingWidth:sentenceScrollView.frame.size.width/0.75f];
    
    newSgo.layer.anchorPoint = CGPointMake(0.0f, 0.0f);
    newSgo.frame = CGRectMake(0.0f, 0.0f, newSgo.frame.size.width, newSgo.frame.size.height);
    [sentenceScrollView addSubview:newSgo];
    
//    sentenceScrollView.contentSize = CGSizeMake(sentenceScrollView.frame.size.width, newSgo.frame.size.height*defaultZoom);
  
    /*
    // create left constraint
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:newSgo attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:sentenceScrollView attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.0f];
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:newSgo attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:sentenceScrollView attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f];
    [sentenceScrollView addConstraints:[NSArray arrayWithObjects:leftConstraint, topConstraint, nil]];
    [leftConstraint release];
    [topConstraint release];
    
    // create constraint to lock to width of scrollview (NOT contentview)
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:newSgo attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:sentenceScrollView attribute:NSLayoutAttributeWidth multiplier:1.0f/defaultZoom constant:1.0f];
    [sentenceScrollView addConstraint:widthConstraint];
    [widthConstraint release];

    // create constraint fix height
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:newSgo attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f/defaultZoom constant:300.0f];
    [sentenceScrollView addConstraint:heightConstraint];
    [heightConstraint release];
*/
    
    // set as focus
    [newFocusSentence release];
    newFocusSentence = [newSgo retain];
}

-(void)writeKanjiSkipped:(id)sender {
    
}

-(void)hadToLookWhileDrawingKanji:(WriteKanjiViewController*)sender {
    // user had to look
    newFocusKanji.kanji.hadToLook = YES;
}

-(void)writeKanjiDrawn:(id)sender withScore:(float)inScore hadToLook:(BOOL)inHadToLook {
    // allow user to keep drawing so as not to give them too much of a clue
    return;
}

-(void)clearButtonLongPressed:(id)sender {
    // pass onto kanji writing controller
    [writeKanjiController clearButtonLongPressed:sender];
}

-(IBAction)clearButtonPressed:(id)sender {
    // if this is a reading game
    if (game.gameType&blGameType_Reading) {
        // 3-stage help is show kanji meaning, then show word meaning, then show furigana
        // if nothing to clear
        if (![newFocusWord clearButtonPressed:sender]) {
            // clear text
            hiddenTextField.text = @"";
//            thisWord.kanaLabel.text = @"?";
        }
    }
    else
        // pass onto kanji writing controller
        [writeKanjiController clearButtonPressed:sender];
}

-(IBAction)hintButtonPressed:(id)sender {
    // if this is a reading game
    if (game.gameType&blGameType_Reading) {
        // 3-stage help is show kanji meaning, then show word meaning, then show furigana
        [newFocusWord helpButtonPressed:sender];
    }
    else
        // pass onto kanji writing controller
        [writeKanjiController helpButtonPressed:sender];
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return sentenceScrollView;
}

-(IBAction)menuButtonPressed:(id)sender {
    // confirm exit
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Exit to Menu" message:@"This will end your current game, are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [myAlert show];
    [myAlert release];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // if we wanted to exit, dismiss controller
    if (buttonIndex == 1) {
        // go back to menu
        [self.navigationController popViewControllerAnimated:YES];
//        [self dismissViewControllerAnimated:YES completion:^{
//        }];
// deprecated        [self dismissModalViewControllerAnimated:YES];
    }
}

-(CGAffineTransform)getZoomOutTransform {
    // set anchorpoint on view now, so don't have to keep centering
    return CGAffineTransformMakeScale(defaultZoom, defaultZoom);
}

-(CGAffineTransform)getZoomTransformForWord:(BlasterWordSgo*)inWord {
//    return CGAffineTransformMakeScale(defaultZoom, defaultZoom);
    
    // first find bounds of word with relation to our main view
    CGRect bounds = inWord.frame;
    
    // first, find center
    CGPoint center = CGPointMake(bounds.origin.x+bounds.size.width/2.0f, bounds.origin.y+bounds.size.height/2.0f);
    
    // find scale
    float scale1 = sentenceScrollView.frame.size.width/bounds.size.width;
    float scale2 = sentenceScrollView.frame.size.height/bounds.size.height;
    float scale = scale1<scale2?scale1:scale2;
    
    // don't zoom in anymore than 2.0
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (scale > 1.5f)
            scale = 1.5f;
    }
    else {
        if (scale > 1.0f)
            scale = 1.0f;
    }
    
    // get zoom transform
    // how much of screen is covered by keyboard?
    float keyboardPercent = 0.4f;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        keyboardPercent = 0.20f;
    CGAffineTransform myTransform = CGAffineTransformScale(CGAffineTransformMakeTranslation(-center.x*scale+sentenceScrollView.frame.size.width/2.0f, -center.y*scale+(sentenceScrollView.frame.size.height*(1.0f-keyboardPercent))/2.0f), scale, scale);
    
    return myTransform;
}

-(CGAffineTransform)getZoomTransformForKanji:(BlasterKanjiSgo*)inKanji {
//    return CGAffineTransformMakeScale(defaultZoom, defaultZoom);
    
    // first find bounds of word with relation to our main view
    CGRect bounds = [inKanji convertRect:inKanji.drawingView.frame toView:newFocusSentence];
    
    // first, find center
    CGPoint center = CGPointMake(bounds.origin.x+bounds.size.width/2.0f, bounds.origin.y+bounds.size.height/2.0f);
    
    // find scale
    float scale1 = sentenceScrollView.frame.size.width/bounds.size.width;
    float scale2 = sentenceScrollView.frame.size.height/bounds.size.height;
    float scale = scale1<scale2?scale1:scale2;
    
    // don't zoom in anymore than 2.0
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (scale > 1.5f)
            scale = 1.5f;
    }
    else {
        if (scale > 1.0f)
            scale = 1.0f;
    }
    
    // get zoom transform
    // how much of screen is covered by keyboard?
    float keyboardPercent = 0.5f;
//    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//        keyboardPercent = 0.20f;
    CGAffineTransform myTransform = CGAffineTransformScale(CGAffineTransformMakeTranslation(-center.x*scale+sentenceScrollView.frame.size.width/2.0f, -center.y*scale+(sentenceScrollView.frame.size.height*(1.0f-keyboardPercent))/2.0f), scale, scale);
    
    NSLog(@"sentence scrollview frame=%f,%f", sentenceScrollView.frame.size.width, sentenceScrollView.frame.size.height);
    
    return myTransform;
    
    
    
    /*
    // first find bounds of word with relation to our main view
    CGRect bounds = [inKanji convertRect:inKanji.drawingView.frame toView:newFocusSentence];
//    BlasterWordSgo *word = (BlasterWordSgo*)inKanji.superview;
//    bounds.origin.y -= word.kanaLabel.frame.size.height;
//    bounds.size.height += word.kanaLabel.frame.size.height;
    
    // first, find center
    CGPoint center = CGPointMake(bounds.origin.x+bounds.size.width/2, bounds.origin.y+bounds.size.height/2);
    
    // find scale
    float scale1 = sentenceScrollView.frame.size.width/bounds.size.width;
    float scale2 = sentenceScrollView.frame.size.height/bounds.size.height;
    float scale = scale1<scale2?scale1:scale2;
    
    // make it a little smaller
    scale *= 0.9f;
//    scale = 8.0f;
    
    // get zoom transform
    CGAffineTransform myTransform = CGAffineTransformScale(CGAffineTransformMakeTranslation(-center.x*scale+sentenceScrollView.frame.size.width/2.0f, -center.y*scale+sentenceScrollView.frame.size.height/2.0f), scale, scale);
    
    return myTransform;
     */
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)dealloc {
    /*
    // release all system sounds and array
    NSString *nextKey;
    NSEnumerator *enumerator = [systemSounds keyEnumerator];
    while ((nextKey = [enumerator nextObject])) {
        NSNumber *thisNum = [systemSounds objectForKey:nextKey];
        SystemSoundID thisSound = [thisNum unsignedIntegerValue];
        AudioServicesDisposeSystemSoundID(thisSound);
    }
    [systemSounds release];*/
    
    // release recognizers
    [tapRecognizer release];
    [doubleTapRecognizer release];
    [longRecognizer release];
    [clearButtonLongRecognizer release];
    [swipeLeftRecognizer release];
    [swipeRightRecognizer release];
    
    // release magnifier stuff
    [magView release];
    [magOverlayView release];
    CGImageRelease(magMask);
    
    // release other variables
    [game release];
    
    // release music player
    [musicPlayer stop];
    [musicPlayer release];
    
    // always call super
    [super dealloc];
}

@end
