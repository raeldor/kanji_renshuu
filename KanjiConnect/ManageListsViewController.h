//
//  ManageListsViewController.h
//  KanjiConnect
//
//  Created by Ray Price on 7/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditKanjiListViewControllerDelegate.h"

@interface ManageListsViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, EditKanjiListViewControllerDelegate> {
    NSMutableArray *listNameList;
    
    NSString *oldListName;
}

-(void)populateListList;
-(IBAction)readingSegmentChanged:(id)sender;
-(IBAction)writingSegmentChanged:(id)sender;

@end
