//
//  KanjiList.m
//  KanjiConnect
//
//  Created by Ray Price on 7/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KanjiList.h"

@implementation KanjiList

@synthesize listName;
@synthesize kanjiList;

-(id)init {
	if ((self = [super initWithExtension:@".kanjilist" delegate:self])) {
        self.listName = @"";
        NSMutableArray *newList = [[NSMutableArray alloc] init];
        self.kanjiList = newList;
        [newList release];
	}
	return self;
}

-(id)initFromArchive:(NSString*)inName {
	if ((self = [super initFromArchive:inName extension:@".kanjilist" delegate:self])) {
	}
	return self;
}

+(void)deleteKanjiList:(NSString*)inName {
    [KanjiList delete:inName extension:@".kanjilist"];
}

-(void)archiveFieldsUsingArchiver:(NSKeyedArchiver*)inArchiver {
	// archive fields
    [inArchiver encodeObject:listName forKey:@"ListName"];
    [inArchiver encodeObject:kanjiList forKey:@"KanjiList"];
}

-(void)unarchiveFieldsUsingUnarchiver:(NSKeyedUnarchiver*)inUnarchiver {	
	// unarchive fields
    self.listName = [inUnarchiver decodeObjectForKey:@"ListName"];
    self.kanjiList = [inUnarchiver decodeObjectForKey:@"KanjiList"];
}

-(id)copyWithZone:(NSZone *)zone {
    KanjiList *newList = [[KanjiList alloc] init];
    newList.listName = [[listName copy] autorelease];
    for (int i=0; i < kanjiList.count; i++)
        [newList.kanjiList addObject:[[[kanjiList objectAtIndex:i] copy] autorelease]];
    return newList;
}

-(void)dealloc {
    // release our retains
    [listName release];
    [kanjiList release];
    
    // super
	[super dealloc];
}

@end
