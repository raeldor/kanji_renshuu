//
//  ViewController.m
//  KanjiConnect
//
//  Created by Ray Price on 6/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "ReadingPlayViewController.h"
#import "KrAppOptions.h"
#import "KrLocalOptions.h"
#import "BlasterGame.h"
#import "KanjiList.h"
#import "DeckBrowserViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    // call super
    [super viewDidLoad];
    
    // if ios 6, set button fonts smaller
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
        UIFont *font = [UIFont boldSystemFontOfSize:12.0f];
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
//        NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:UITextAttributeFont];
        [readingStudyFreeSegment setTitleTextAttributes:attributes
                                               forState:UIControlStateNormal];
        [writingStudyFreeSegment setTitleTextAttributes:attributes
                                               forState:UIControlStateNormal];
    }
    
    // have to disable play until we have app options
    writingButton.enabled = NO;
    readingButton.enabled = NO;
    readingStudyFreeSegment.enabled = NO;
    writingStudyFreeSegment.enabled = NO;
    optionsButton.enabled = NO;
    listsButton.enabled = NO;
    
//    writingLabel.textColor = [UIColor grayColor];
//    readingLabel.textColor = [UIColor grayColor];
    
    // set timer to check for app options being loaded
    optionsCheckCount = 0;
    checkTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(checkOptionsTimerFired:) userInfo:nil repeats:YES] retain];
    
    // check for app did become active notification
    
    // change tint color
//    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:128.0f/255.0f blue:0.0f alpha:1.0f];
    
}

-(void)checkOptionsTimerFired:(NSTimer*)inTimer {
    // increment counter
    optionsCheckCount++;
    
    // get application delegate
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // have local options been loaded
    if (appDelegate.isLocalOptionsLoaded) {
        // re-enable options
        optionsButton.enabled = YES;
    }
    
    // have options been loaded
    if (appDelegate.isOptionsLoaded) {
        // re-enable buttons
        writingButton.enabled = YES;
        readingButton.enabled = YES;
        readingStudyFreeSegment.enabled = YES;
        writingStudyFreeSegment.enabled = YES;
        optionsButton.enabled = YES;
        listsButton.enabled = YES;
        
        // set switch value to saved
        readingStudyFreeSegment.selectedSegmentIndex = [KrLocalOptions getSingleton].readingStudyFreeSwitchValue;
        writingStudyFreeSegment.selectedSegmentIndex = [KrLocalOptions getSingleton].writingStudyFreeSwitchValue;
        
        // disable timer
        [inTimer invalidate];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // if this is cloud warning
    if ([alertView.title isEqualToString:@"iCloud Issue"]) {
        // assert
        NSAssert(0, @"Progress would not load from cloud");
    }
}


/*

-(BOOL)waitForUpdatedOptions {
    // if we're not already waiting
    if (!waitingForUpdatedOptions) {
        // mark as waiting
        waitingForUpdatedOptions = YES;
        
        // check for cloud access
        NSURL *ubiq = [[NSFileManager defaultManager]
                       URLForUbiquityContainerIdentifier:nil];
        if (ubiq) {
            // code to disable buttons until options have been reloaded
            readingStudyFreeSegment.enabled = NO;
            writingStudyFreeSegment.enabled = NO;
            readingButton.enabled = NO;
            writingButton.enabled = NO;
            listsButton.enabled = NO;
            optionsButton.enabled = NO;
            
            NSLog(@"adding observer for cloud changes in ViewController viewWillAppear");
            
            // register for notification of options loaded
            __block id optionsObserver;
            optionsObserver = [[NSNotificationCenter defaultCenter] addObserverForName:@"App Options Loaded" object:[KrAppOptions getSingleton] queue:nil usingBlock:^(NSNotification *note) {
                NSLog(@"Cloud changes loaded in ViewController viewWillAppear");
                // set switch value to saved
                readingStudyFreeSegment.selectedSegmentIndex = [KrLocalOptions getSingleton].readingStudyFreeSwitchValue;
                writingStudyFreeSegment.selectedSegmentIndex = [KrLocalOptions getSingleton].writingStudyFreeSwitchValue;
                
                // re-enable buttons
                readingStudyFreeSegment.enabled = YES;
                writingStudyFreeSegment.enabled = YES;
                readingButton.enabled = YES;
                writingButton.enabled = YES;
                listsButton.enabled = YES;
                optionsButton.enabled = YES;
                
                // no longer waiting for updated options
                waitingForUpdatedOptions = NO;
                
                // remove observer
                [[NSNotificationCenter defaultCenter] removeObserver:optionsObserver];
            }];
        }
    }
    else {
        NSLog(@"Already waiting for updated options");
        return NO;
    }
    return YES;
}
*/

-(void)viewWillAppear:(BOOL)animated {
    // call super
    [super viewWillAppear:animated];
}

-(void)checkOkToPlayForGameType:(blGameTypeEnum)inGameType completion:(void (^)(BOOL success)) block {
    __block int studyingCount = 0;
    __block int studiedCount = 0;
    
    // loop through all decks and section and find known/learning kanji
    cloudQuery = [[FlashDeck getListOfArchivesWithExtension:@"flashDeck" completion:^(NSArray *archiveList) {
        // wait for all open decks to finish
        dispatch_group_t group = dispatch_group_create();
        
        // loop through decks
        for (int i=0; i < archiveList.count; i++) {
            // loop through sections
            NSString *deckName = [archiveList objectAtIndex:i];
            FlashDeck * thisDeck = [[FlashDeck alloc] initWithName:deckName];
            dispatch_group_enter(group);
            [thisDeck openWithCompletionHandler:^(BOOL success) {
                for (int s=0; s < thisDeck.deckSections.count; s++) {
                    DeckSection *thisSection = [thisDeck.deckSections objectAtIndex:s];
//                    if ([thisSection.sectionType isEqualToString:@"Kanji"]) {
                        int studyStatus;
                        if (inGameType&blGameType_Writing)
                            studyStatus = [[KrLocalOptions getSingleton] getWritingStudyStatusForSectionNamed:thisSection.sectionName inDeckNamed:thisDeck.deckName];
                        else
                            studyStatus = [[KrLocalOptions getSingleton] getReadingStudyStatusForSectionNamed:thisSection.sectionName inDeckNamed:thisDeck.deckName];
                        if (studyStatus == 0) {
                            studiedCount++;
                        }
                        else {
                            // if learning
                            if (studyStatus == 1)
                                studyingCount++;
                        }
//                    }
                }
                dispatch_group_leave(group);
                
                // close the deck again, don't keep it open
                [thisDeck closeWithCompletionHandler:^(BOOL success) {
                    // do nothing
                }];
            }];
            [thisDeck release];
        }
        
        // wait for completion
        while (dispatch_group_wait(group, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.2f]];
        }
        dispatch_release(group);
        
        // if we're in study play mode
        if ((inGameType&blGameType_Reading  && readingStudyFreeSegment.selectedSegmentIndex == 0) ||
            (inGameType&blGameType_Writing  && writingStudyFreeSegment.selectedSegmentIndex == 0)
            ) {
            // must have learning selected
            if (studyingCount == 0) {
                // print error message
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Study Play" message:@"You must have one or more sections marked as 'Studying' to play in 'Study Play' mode." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
                [alert show];
                [alert release];
                
                // call completion with failure
                block(NO);
                return;
            }
        }
        else {
            // must have either learning or known selected
            if (studyingCount+studiedCount == 0) {
                // print error message
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Free Play" message:@"You must have one or more sections marked as either 'Studying' or 'Studied' to play in 'Free Play' mode." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
                [alert show];
                [alert release];
                
                // call completion with failure
                block(NO);
                return;
            }
        }
        
        // call completion with success
        block(YES);
        
        // release query
        [cloudQuery release];
    }] retain];
}

-(IBAction)readingButtonPressed:(id)sender {
    // determine game type from options and menu
    blGameTypeEnum playGameType = blGameType_Reading;
    if (readingStudyFreeSegment.selectedSegmentIndex == 0)
        playGameType += blGameType_Study;
    else
        playGameType += blGameType_Freeplay;
    
    // start spinner here because check takes a while now on cloud
    activityView.hidden = NO;
    [activityView startAnimating];
    
    // disable button
    readingButton.enabled = NO;
    
    // show reading controller
    [self checkOkToPlayForGameType:playGameType completion:^(BOOL success) {
        // push controller
        if (success)
            [self pushControllerForGameType:playGameType];
        else {
            // stop spinner and re-enable button
            readingButton.enabled = YES;
            [activityView stopAnimating];
        }
    }];
}

-(IBAction)writingButtonPressed:(id)sender {
    // determine game type from options and menu
    blGameTypeEnum playGameType = blGameType_Writing;
    if (writingStudyFreeSegment.selectedSegmentIndex == 0)
        playGameType += blGameType_Study;
    else
        playGameType += blGameType_Freeplay;
    
    // start spinner here because check takes a while now on cloud
    activityView.hidden = NO;
    [activityView startAnimating];
    
    // disable button
    writingButton.enabled = NO;
    
    // show writing controller
    [self checkOkToPlayForGameType:playGameType completion:^(BOOL success) {
        // push controller
        if (success)
            [self pushControllerForGameType:playGameType];
        else {
            // stop spinner and re-enable button
            writingButton.enabled = YES;
            [activityView stopAnimating];
        }
    }];
}

-(IBAction)readingStudyFreeSwitchValueChanged:(id)sender {
    // update app options and save
    [KrLocalOptions getSingleton].readingStudyFreeSwitchValue = readingStudyFreeSegment.selectedSegmentIndex;
    [[KrLocalOptions getSingleton] save];
}

-(IBAction)writingStudyFreeSwitchValueChanged:(id)sender {
    // update app options and save
    [KrLocalOptions getSingleton].writingStudyFreeSwitchValue = writingStudyFreeSegment.selectedSegmentIndex;
    [[KrLocalOptions getSingleton] save];
}

-(void)pushControllerForGameType:(blGameTypeEnum)inGameType {
    // create new game on a separate thread

    // perform on another thread so we can see activity indicator
    selectedGameType = inGameType;
    [self performSelector:@selector(createGameWithType:) withObject:self afterDelay:0.1];
    
    [self.view bringSubviewToFront:activityView];
}

-(void)createGameWithType:(ViewController*)inController {
    // create new game, this could take a while, so show activity indicator
    BlasterGame *newGame2 = [[BlasterGame alloc] init];
    [newGame2 startGameWithGameType:selectedGameType completion:^(BOOL success) {
        // stop spinner
        [activityView stopAnimating];
        
        // create play view controller
        ReadingPlayViewController *playController = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadingPlayViewController"];
        playController.game = newGame2;
        [newGame2 release];
        
        // push play controller
        [self.navigationController pushViewController:playController animated:YES];
        /* ios 13 doesn't allow page curl on progress view, so make everything pushes to simplify
        [self.navigationController presentViewController:playController animated:YES completion:^{
        }];
         */
        
// deprecated        [self.navigationController presentModalViewController:playController animated:YES];
        
        // enabled button again
        if (selectedGameType&blGameType_Writing)
            writingButton.enabled = YES;
        else
            readingButton.enabled = YES;
    }];
}

-(void)flashDecksButtonPressed:(id)sender {
    // launch flash decks
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    DeckBrowserViewController *browser=[storyboard instantiateViewControllerWithIdentifier:@"DeckBrowserViewController"];
    browser.sectionBrowserDelegate = self;
    [self.navigationController pushViewController:browser animated:YES];
}

-(void)didUpdateReadingStudySwitchForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName toValue:(NSUInteger)inValue {
    // set and save options
    [[KrLocalOptions getSingleton] setReadingStudyStatusForSectionNamed:inSectionName inDeckNamed:inDeckName toValue:inValue];
    [[KrLocalOptions getSingleton] save];
}

-(void)didUpdateWritingStudySwitchForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName toValue:(NSUInteger)inValue {
    // set and save options
    [[KrLocalOptions getSingleton] setWritingStudyStatusForSectionNamed:inSectionName inDeckNamed:inDeckName toValue:inValue];
    [[KrLocalOptions getSingleton] save];
}

-(int)getReadingStudySwitchValueForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName {
    return [[KrLocalOptions getSingleton] getReadingStudyStatusForSectionNamed:inSectionName inDeckNamed:inDeckName];
}

-(int)getWritingStudySwitchValueForSectionNamed:(NSString*)inSectionName inDeckNamed:(NSString*)inDeckName {
    return [[KrLocalOptions getSingleton] getWritingStudyStatusForSectionNamed:inSectionName inDeckNamed:inDeckName];
}

-(void)didFinishInitializing:(BlasterGame*)inGame {
}

/*
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // call super first
    [super prepareForSegue:segue sender:sender];
        
    // is this is the play controller
    if ([[segue identifier] isEqualToString:@"ReadingPlay"] ||
        [[segue identifier] isEqualToString:@"WritingPlay"]) {
        // determine game type from options and menu
        blGameTypeEnum playGameType = 0;
        if (studyFreeSegment.selectedSegmentIndex == 0)
            playGameType += blGameType_Study;
        else
            playGameType += blGameType_Freeplay;
        if ([[segue identifier] isEqualToString:@"ReadingPlay"])
            playGameType += blGameType_Reading;
        else
            playGameType += blGameType_Writing;
        
        // create new game, this could take a while, so show activity indicator
        activityView.hidden = NO;
        [activityView startAnimating];
        BlasterGame *newGame2 = [[BlasterGame alloc] initWithGameType:playGameType];
        [activityView stopAnimating];
        
        // show play view controller
        ReadingPlayViewController *playController = [segue destinationViewController];
        playController.game = newGame2;
        [newGame2 release];
    }
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)dealloc {
    // release our retains
    [checkTimer release];
    
    // call super
    [super dealloc];
}

@end
